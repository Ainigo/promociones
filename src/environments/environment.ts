// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  version: '1.0',
  serverUrl: 'http://localhost:3001',
  defaultLanguage: 'es-ES',
  supportedLanguages: ['es-ES']
};

export const authParameters: any = {
  tenant : '1eac135e-ccdf-4d83-8b4f-efc9038309f0',
  authorityHostUrl : 'https://login.windows.net/',
  clientId : '9124e0f1-1552-49d2-9230-ae9558fa304d',
  redirectUri: 'https://gotham.aedashomes.com/login',
  // redirectUri: 'http://localhost:4201/login',
  resource: '00000003-0000-0000-c000-000000000000',
  postLogoutUrl: 'https://gotham.aedashomes.com'
  // postLogoutUrl: 'http://localhost:4201'
};

export const api_version: any = {
  // version : 'http://localhost:3001'
  version : ''
};

export const url_prinex: any = {
  // url : 'http://89.202.161.237:8001/query/execute'
  url : 'https://apiprinex.aedashomes.com/query/execute'
};




/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

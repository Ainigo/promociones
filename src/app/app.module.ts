import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import {NgxPaginationModule} from 'ngx-pagination';



// sockets
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

const config: SocketIoConfig = {
  url: environment.serverUrl , options: {}
};



import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { FooterComponent } from './components/footer/footer.component';
import { ChatComponent } from './components/chat/chat.component';
import { ListaUsuariosComponent } from './components/lista-usuarios/lista-usuarios.component';
import { LoginComponent } from './pages/login/login.component';
import { PromocionesComponent } from './pages/promociones/promociones.component';
import { EncuestasComponent } from './pages/encuestas/encuestas.component';
import { ClientesNuevosComponent } from './pages/clientesnuevos/clientesnuevos.component';
import { ClientesNuevosSalesForceComponent } from './pages/clientesnuevossalesforce/clientesnuevossalesforce.component';
import { AreaPrivadaComponent } from './pages/areaprivada/areaprivada.component';
import { VincularInmuebleComponent } from './pages/vincularinmueble/vincularinmueble.component';
import { TalkdeskComponent } from './pages/talkdesk/talkdesk.component';
//import { SendEmailComponent } from './pages/sendemail/sendemail.component';
import { ValidacionSpamComponent } from './pages/validacionspam/validacionspam.component';
import { DisponibilidadComponent } from './pages/disponibilidad/disponibilidad.component';
import { SalesforceComponent } from './pages/salesforce/salesforce.component';
import { UsersComponent } from './pages/users/users.component';
import { FirmanotariaComponent } from './pages/firmanotaria/firmanotaria.component';
import { MktcloudComponent } from './pages/mktcloud/mktcloud.component';
import { TokenComponent } from './pages/token/token.component';
import { HeaderComponent } from './components/header/header.component';





@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    ChatComponent,
    ListaUsuariosComponent,
    LoginComponent,
    PromocionesComponent,
    EncuestasComponent,
    ClientesNuevosComponent,
    ClientesNuevosSalesForceComponent,
    AreaPrivadaComponent,
    VincularInmuebleComponent,
    TalkdeskComponent,
    //SendEmailComponent,
    ValidacionSpamComponent,
    DisponibilidadComponent,
    SalesforceComponent,
    UsersComponent,
    FirmanotariaComponent,
    MktcloudComponent,
    TokenComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    NgxPaginationModule,
    FormsModule,
    HttpClientModule,
    SocketIoModule.forRoot(config),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

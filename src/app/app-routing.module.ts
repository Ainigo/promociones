import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './pages/login/login.component';
import { PromocionesComponent } from './pages/promociones/promociones.component';
import { EncuestasComponent } from './pages/encuestas/encuestas.component';
import { ClientesNuevosComponent } from './pages/clientesnuevos/clientesnuevos.component';
import { ClientesNuevosSalesForceComponent } from './pages/clientesnuevossalesforce/clientesnuevossalesforce.component';
import { AreaPrivadaComponent } from './pages/areaprivada/areaprivada.component';
import { DisponibilidadComponent } from './pages/disponibilidad/disponibilidad.component';
import { SalesforceComponent } from './pages/salesforce/salesforce.component';
import { UsersComponent } from './pages/users/users.component';
import { VincularInmuebleComponent } from './pages/vincularinmueble/vincularinmueble.component';
import { TalkdeskComponent } from './pages/talkdesk/talkdesk.component';
// import { SendEmailComponent } from './pages/sendemail/sendemail.component';
import { ValidacionSpamComponent } from './pages/validacionspam/validacionspam.component';
import { FirmanotariaComponent } from './pages/firmanotaria/firmanotaria.component';
import { MktcloudComponent } from './pages/mktcloud/mktcloud.component';
import { TokenComponent } from './pages/token/token.component';
import { UsuarioGuard } from './guards/usuario-guard.service';
import { AuthGuardService as AuthGuard } from './core/authentication/auth-guard.service';


const appRoutes: Routes = [
  { path: '', component: LoginComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {
    path: 'promociones',
    component: PromocionesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'encuestas',
    component: EncuestasComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'clientesnuevos',
    component: ClientesNuevosComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'clientesnuevossalesforce',
    component: ClientesNuevosSalesForceComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'areaprivada',
    component: AreaPrivadaComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'vincularinmueble',
    component: VincularInmuebleComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'talkdesk',
    component: TalkdeskComponent,
    canActivate: [AuthGuard]
  },
  // {
  // path: 'sendemail',
  // component: SendEmailComponent,
  // canActivate: [AuthGuard]
  // },
  {
    path: 'validacionspam',
    component: ValidacionSpamComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'salesforce',
    component: SalesforceComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'users',
    component: UsersComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'firmanotaria',
    component: FirmanotariaComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'mktcloud',
    component: MktcloudComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'token',
    component: TokenComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'disponibilidad',
    component: DisponibilidadComponent,
    canActivate: [AuthGuard]
  },
  // { path: '**', component: LoginComponent, canActivate: [ UsuarioGuard ] },
  { path: '**', component: LoginComponent, pathMatch: 'full'},
];


@NgModule({
  imports: [ RouterModule.forRoot( appRoutes ) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }

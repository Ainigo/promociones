import { Component, OnInit } from '@angular/core';
import { WebsocketService } from 'src/app/services/websocket.service';
import { finalize } from 'rxjs/operators';
import { PARAMETERS } from '@angular/core/src/util/decorators';
import { AuthenticationService } from '../../core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    public wsService: WebsocketService,
    public authenticationService: AuthenticationService,
  ) { }
  public usuarioAdmin = false;
  public usuarioUser = false;
  public usuarioNotario = false;
  public usuarioMktCloud = false;
  public usuarioPortales = false;
  public usuarioEncuestas = false;
  public usuarioUserApp;
  ngOnInit() {

    const credentials = this.authenticationService.credentials;

    const nombreUsuario = credentials.username;
    // const nombreUsuario = 'azuniga.ext@aedashomes.com';
    this.wsService.getpintarTrazasLogs(nombreUsuario, 'YA vamos a mirar el perfil');

    this.wsService.getUserApp(nombreUsuario)
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      data => {
        console.log('TODO OK3. obtención del perfil' );
        this.usuarioUserApp = data;
        console.log(this.usuarioAdmin);
    },
      error => {
        console.log('Error AAAAA');
      }
    );

    this.wsService.getUserApp(nombreUsuario)
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      result => {
        const valor = JSON.stringify(result);
        const valor2 = JSON.parse(valor);
        this.wsService.getpintarTrazasLogs(nombreUsuario, 'Vamos a la obtención del perfil nuevo' + valor2['result']);
        this.usuarioUserApp = valor2['result'];
        this.wsService.getpintarTrazasLogs(nombreUsuario, 'El resultado es: ' + this.usuarioUserApp);
        if (this.usuarioUserApp.toLowerCase() === 'admin') {
          this.usuarioAdmin = true;
        }
        if (this.usuarioUserApp.toLowerCase() === 'user') {
          this.usuarioUser = true;
        }
        if (this.usuarioUserApp.toLowerCase() === 'notario') {
          this.usuarioNotario = true;
        }
        if (this.usuarioUserApp.toLowerCase() === 'marketing') {
          this.usuarioMktCloud = true;
        }
        if (this.usuarioUserApp.toLowerCase() === 'portales') {
          this.usuarioPortales = true;
        }
        if (this.usuarioUserApp.toLowerCase() === 'encuestas') {
          this.usuarioEncuestas = true;
        }
      },
      error => {
        this.wsService.getpintarTrazasLogs(nombreUsuario, 'Error');
      }
    );
  }

  salir() {
    this.wsService.logoutWS();
    this.authenticationService.logout();
  }

  get username(): string | null {

    const credentials = this.authenticationService.credentials;

    return credentials ? credentials.username : null;
  }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { AppSettings } from '../shared/app-settings';
import { Socket } from 'ngx-socket-io';
import { ProductosCat } from '../classes/productocat';
import { Router } from '@angular/router';



@Injectable({
  providedIn: 'root'
})

export class ProductosService {

  public socketStatus = false;
  public producto: ProductosCat = null;
  public numbercategory: number;

  constructor(
    //private socket: Socket,
    private router: Router,
    public httpClient: HttpClient
  ) {
    this.numbercategory = this.cargarStorage();
  }

  preparePost(url: string, params: any): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Content-Type', 'application/json').set('Cache-Control', 'no-cache');
    console.log('la url es: ' + url);
    console.log(this.httpClient.post<any>(url, params, { headers: httpHeaders }));
    return this.httpClient.post<any>(url, params, { headers: httpHeaders });
  }

    getProductos(idCategorias: number) {
      const json = JSON.stringify({ idCategorias});
      console.log('funcion get Productos');
      return this.preparePost(`${AppSettings.API_PRODUCTOS}`, json);
    }

    getNameCategory(idCategorias: number) {
      const json = JSON.stringify({idCategorias});
      console.log(json);
      return this.preparePost(`${AppSettings.API_CATEGORIAS}`, json);
    }

    cargarStorage(): number {
      let categorianumber = 1;
      if (localStorage.getItem('categoria') ) {
        categorianumber = JSON.parse( localStorage.getItem('categoria') );
      }
      return categorianumber;
  }
}

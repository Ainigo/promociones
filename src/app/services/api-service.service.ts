import { Injectable, Version } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { AppSettings } from '../shared/app-settings';
import { IUser } from '../shared/interfaces/users';
import { IStudy } from '../shared/interfaces/studies';
import { IStudyFullModule, Secondhandrent, Nextpromotion } from '../shared/interfaces/studyFull';
import { IBasicType } from '../shared/interfaces/basictype';
import { IPromotionFullModule, Promotion } from '../shared/interfaces/promotionFull';
import { IListBuilding } from '../shared/interfaces/buildings';
import { IBuildingFull } from '../shared/interfaces/buildings';
import { ISecondHandRentModule } from '../shared/interfaces/seconHandRentFull';
import { INextPromotionModule } from '../shared/interfaces/next-promotion';
import { IDocumentationFullModule } from '../shared/interfaces/documenation';
import { Masters } from '../shared/interfaces/masters';
import { FirstLevelQualities } from '../shared/interfaces/firstlevelqualities';
import { CntValueFull } from '../shared/interfaces/cntValueFull';
import { IUserStudies } from '../shared/interfaces/userStudies';
import { RequestOptions, ResponseContentType, Http } from '@angular/http';
// import { saveAs } from 'file-saver';


@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  currentStudy: IStudyFullModule.IStudyFull;

  constructor(public httpClient: HttpClient) { }

  preparePost(url: string, params: any): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Content-Type', 'application/json').set('Cache-Control', 'no-cache');
    return this.httpClient.post<any>(url, params, { headers: httpHeaders });
  }

  preparePostDocument(url: string, params: any): Observable<any> {
    const httpHeaders = new HttpHeaders()// .set('Content-Type', 'multipart/form-data')
      .set('Cache-Control', 'no-cache');
    return this.httpClient.post<any>(url, params, { headers: httpHeaders });
  }

  public getAuthReq(code:string): Observable<any> {
    // return this.preparePost(`${AppSettings.API_LOGIN}`, json);
    return this.httpClient.get<any>(`${AppSettings.GET_ATOKEN}` + '?code=' + code);
  }

  public getLogin(email: string, password: string): Observable<any> {
    const json = JSON.stringify({ email, password });
    // return this.preparePost(`${AppSettings.API_LOGIN}`, json);
    return this.httpClient.get<any>(`${AppSettings.API_LOGIN}`);
  }


  public getLogout(): Observable<any> {
    return this.httpClient.get<any>(`${AppSettings.API_LOGOUT}`);
  }

  //#region User
  public createUser(obj: any): Observable<any> {
    return this.preparePost(`${AppSettings.API_USERS}`, obj);
  }

  public updateUser(obj: any): Observable<any> {
    return this.httpClient.put(`${AppSettings.API_USER}` + obj.user_id, obj);
  }

  public deleteUser(id: any): Observable<any> {
    return this.httpClient.delete(`${AppSettings.API_USER}` + id);
  }

  public getUsers(): Observable<IUser[]> {
    return this.httpClient.get<IUser[]>(`${AppSettings.API_USERS}`);
  }

  public getUser(name: string): Observable<IUser> {
    return this.httpClient.get<IUser>(`${AppSettings.API_USER}` + name);
  }

  //#endregion

  //#region "Study"
  public getStudies(): Observable<IStudy[]> {
    return this.httpClient.get<IStudy[]>(`${AppSettings.API_STUDIES}`);
  }

  public getStudiesByUser(name: string): Observable<IUserStudies> {
    return this.httpClient.get<IUserStudies>(`${AppSettings.API_USER}` + name + `${AppSettings.API_USER_STUDIES}`);
  }

  public AssignStudiesByUser(id: number, obj: any): Observable<any> {
    return this.preparePost(`${AppSettings.API_USER}` + id + `${AppSettings.API_USER_STUDIES}`, obj);
  }

  public UnassignStudiesByUser(user_id: number, study_id: any): Observable<any> {
    return this.httpClient.delete(`${AppSettings.API_USER}` + user_id + `${AppSettings.API_USER_STUDY}` + study_id);
  }

  public getStudy(id: number, version_number: string): Observable<IStudyFullModule.IStudyFull> {
    const httpParams = new HttpParams().set('version_number', version_number);

    return this.httpClient.get<IStudyFullModule.IStudyFull>(`${AppSettings.API_STUDY}` + id, {
      params: httpParams
    });
  }

  public getStudyExport(study_id: number){
    return this.httpClient.get(`${AppSettings.API_EXPORT}` + study_id, {responseType: 'blob' as 'json' });
  }

  public createStudy(study: any): Observable<any> {
    return this.preparePost(`${AppSettings.API_STUDIES}`, study);
  }

  public updateStudy(study: any): Observable<any> {
    return this.httpClient.put(`${AppSettings.API_STUDY}` + study.study_id, study);
  }

  public deleteStudy(id: number): Observable<any> {
    return this.httpClient.delete(`${AppSettings.API_STUDY}` + id);
  }

  public changeStateStudy(study_id: number, obj: any): Observable<any> {
    return this.httpClient.put(`${AppSettings.API_STUDY_STATUS}` + study_id, obj);
  }

  public getVersionsByStudy(study_id: number): Observable<any> {
    return this.httpClient.get<Version[]>(`${AppSettings.API_STUDY}` + study_id + `${AppSettings.API_VERSIONS}`);
  }
  //#endregion

  public getBasicData(type: string): Observable<IBasicType[]> {
    return this.httpClient.get<IBasicType[]>(encodeURI(`${AppSettings.API_BASIC_TYPE}` + type));
  }

  public async getBasicDataAsync(type: string) {
    return this.httpClient.get<IBasicType[]>(`${AppSettings.API_BASIC_TYPE}` + type);
  }

  public deleteNext(id: number): Observable<any> {
    return this.httpClient.delete(`${AppSettings.API_NEXT_PROMOTION}` + id);
  }

  //#region "Promotion"

  public getPromotion(id: number, version_number: string): Observable<IPromotionFullModule.IPromotionFull> {
    let httpParams = new HttpParams().set('version_number', version_number);

    return this.httpClient.get<IPromotionFullModule.IPromotionFull>(`${AppSettings.API_PROMOTION}` + id, {
      params: httpParams
    });
  }

  public createPromotion(promotion: any): Observable<any> {
    return this.preparePost(`${AppSettings.API_PROMOTIONS}`, promotion);
  }

  public updatePromotion(promotion: any): Observable<any> {
    return this.httpClient.put(`${AppSettings.API_PROMOTION}` + promotion.promotion_id, promotion);
  }

  public deletePromotion(id: number): Observable<any> {
    return this.httpClient.delete(`${AppSettings.API_PROMOTION}` + id);
  }

  public getPromotionByStudy(study_id: number, version_number: number): Observable<Promotion[]> {
    const httpParams = new HttpParams()
      .set('study_id', study_id.toString())
      .set('version_number', version_number.toString());
    // tslint:disable-next-line: max-line-length
    return this.httpClient.get<Promotion[]>(`${AppSettings.API_PROMOTIONS}`, {
      params: httpParams
    });
  }
  //#endregion

  //#region "Second Hand Rent"

  public createSecondHandRent(obj: any): Observable<any> {
    return this.preparePost(`${AppSettings.API_SECOND_HAND_RENTS}`, obj);
  }

  public updateSecondHandRent(obj: any): Observable<any> {
    return this.httpClient.put(`${AppSettings.API_SECOND_HAND_RENT}` + obj.shr_id, obj);
  }

  public deleteSecondHandRent(id: number): Observable<any> {
    return this.httpClient.delete(`${AppSettings.API_SECOND_HAND_RENT}` + id);
  }

  public getSecondHandRent(shr_id: number): Observable<ISecondHandRentModule.ISecondHandRentFull> {
    return this.httpClient.get<ISecondHandRentModule.ISecondHandRentFull>(
      `${AppSettings.API_SECOND_HAND_RENT}` + shr_id
    );
  }

  public getSecondHandRentByStudy(study_id: number): Observable<Secondhandrent[]> {
    let httpParams = new HttpParams().set('study_id', study_id.toString());
    // tslint:disable-next-line: max-line-length
    return this.httpClient.get<Secondhandrent[]>(`${AppSettings.API_SECOND_HAND_RENT}`, {
      params: httpParams
    });
  }
  //#endregion

  //#region "Building"
  public getBuildingsByPromotion(promotion_id: number, version_number: number): Observable<IListBuilding> {
    let httpParams = new HttpParams().set('version_number', version_number.toString());
    // tslint:disable-next-line: max-line-length
    return this.httpClient.get<IListBuilding>(
      `${AppSettings.API_PROMOTION}` + promotion_id + `${AppSettings.API_BUILDINGS}`,
      {
        params: httpParams
      }
    );
  }

  public getBuilding(promotion_id: number, building_id: number, version_number: number): Observable<IBuildingFull> {
    let httpParams = new HttpParams().set('version_number', version_number.toString());
    // tslint:disable-next-line: max-line-length
    return this.httpClient.get<IBuildingFull>(
      `${AppSettings.API_PROMOTION}` + promotion_id + `${AppSettings.API_POST_BUILDING}` + building_id,
      {
        params: httpParams
      }
    );
  }

  public createBuilding(obj: any): Observable<any> {
    return this.preparePost(`${AppSettings.API_PROMOTION + obj.promotion_id + AppSettings.API_POST_BUILDING}`, obj);
  }

  public updateBuilding(obj: any): Observable<any> {
    // tslint:disable-next-line: max-line-length
    return this.httpClient.put(
      `${AppSettings.API_PROMOTION}` + obj.promotion_id + `${AppSettings.API_POST_BUILDING}` + obj.building_id,
      obj
    );
  }
  //#endregion

  // #region Quality
  public updateQualities(obj: any, promotion_id: number): Observable<any> {
    return this.preparePost(`${AppSettings.API_PROMOTION + promotion_id + AppSettings.API_POST_QUALITIES}`, obj);
  }

  public getQualities(promotion_id: number): Observable<IPromotionFullModule.Quality[]> {
    return this.httpClient.get<IPromotionFullModule.Quality[]>(
      `${AppSettings.API_PROMOTION + promotion_id + AppSettings.API_POST_QUALITIES}`
    );
  }
  //#endregion

  // #region Program
  public updateProgram(obj: any, promotion_id: number): Observable<any> {
    return this.httpClient.put(`${AppSettings.API_PROMOTION + promotion_id + AppSettings.API_POST_PROGRAM}`, obj);
  }

  public getProgram(promotion_id: number, version_number: number): Observable<IPromotionFullModule.IPromotionFull> {
    let httpParams = new HttpParams().set('version_number', version_number.toString());
    // tslint:disable-next-line: max-line-length
    return this.httpClient.get<IPromotionFullModule.IPromotionFull>(
      `${AppSettings.API_PROMOTION + promotion_id + AppSettings.API_POST_PROGRAM}`,
      {
        params: httpParams
      }
    );
  }
  //#endregion

  //#region Financing
  public createFinancing(obj: any): Observable<any> {
    return this.preparePost(`${AppSettings.API_PROMOTION + obj.promotion_id + AppSettings.API_POST_FINANCING}`, obj);
  }

  public updateFinancing(obj: any): Observable<any> {
    // tslint:disable-next-line: max-line-length
    return this.httpClient.put(
      `${AppSettings.API_PROMOTION}` + obj.promotion_id + `${AppSettings.API_POST_FINANCING}`,
      obj
    );
  }

  public getFinancing(promotion_id: number): Observable<any> {
    return this.httpClient.get<IPromotionFullModule.IPromotionFull>(
      `${AppSettings.API_PROMOTION + promotion_id + AppSettings.API_POST_FINANCING}`
    );
  }
  //#endregion

  //#region "Next Promotion"

  public createNextPromotion(obj: any): Observable<any> {
    return this.preparePost(`${AppSettings.API_NEXT_PROMOTION}`, obj);
  }

  public updateNextPromotion(obj: any): Observable<any> {
    return this.httpClient.put(`${AppSettings.API_NEXT_PROMOTION}` + obj.next_promotion_id, obj);
  }

  public deleteNextPromotion(next_promotion_id: number): Observable<any> {
    return this.httpClient.delete(`${AppSettings.API_NEXT_PROMOTION}` + next_promotion_id);
  }

  public getNextPromotion(next_promotion_id: number): Observable<INextPromotionModule.INextPromotionFull> {
    return this.httpClient.get<INextPromotionModule.INextPromotionFull>(
      `${AppSettings.API_NEXT_PROMOTION}` + next_promotion_id
    );
  }

  public getNextPromotionByStudy(study_id: number): Observable<Nextpromotion[]> {
    let httpParams = new HttpParams().set('study_id', study_id.toString());
    // tslint:disable-next-line: max-line-length
    return this.httpClient.get<Nextpromotion[]>(`${AppSettings.API_NEXT_PROMOTION}`, {
      params: httpParams
    });
  }
  //#endregion

  //#region Data Type
  public createDataType(obj: any): Observable<any> {
    return this.preparePost(`${AppSettings.API_DATA_TYPE}`, obj);
  }

  public updateDataType(obj: any): Observable<any> {
    return this.httpClient.put(`${AppSettings.API_DATA_TYPE}`, obj);
  }

  public getConfigMasters(): Observable<Masters[]> {
    return this.httpClient.get<Masters[]>(`${AppSettings.API_DATA_TYPE}`);
  }
  //#endregion

  //#region Data Value
  public createDataValue(obj: any): Observable<any> {
    return this.preparePost(`${AppSettings.API_DATA_VALUE}`, obj);
  }

  public updateDataValue(obj: any): Observable<any> {
    return this.httpClient.put(`${AppSettings.API_DATA_VALUES}` + obj.value_id, obj);
  }

  public getDataValuesByType(id: number) {
    return this.httpClient.get<CntValueFull[]>(`${AppSettings.API_DATA_VALUES}` + id);
  }
  //#endregion

  //#region Qualities first level config
  public getConfigQualitiesFirstLevel(): Observable<FirstLevelQualities[]> {
    return this.httpClient.get<FirstLevelQualities[]>(`${AppSettings.API_CONFIG_QUALITIES_FIRST_LEVEL}`);
  }
  //#endregion

  //#region
  public getDocumentsNextPromotionByStudy(study_id: number): Observable<IDocumentationFullModule.IDocumentationFull[]> {
    // tslint:disable-next-line: max-line-length
    return this.httpClient.get<IDocumentationFullModule.IDocumentationFull[]>(
      `${AppSettings.API_DOCUMENTS}` + study_id + `${AppSettings.NEXT_PROMOTION}`
    );
  }

  public createDocumentNextPromotionByStudy(study_id: number, obj: any): Observable<any> {
    return this.preparePostDocument(`${AppSettings.API_DOCUMENTS}` + study_id + `${AppSettings.NEXT_PROMOTIONS}`, obj);
  }

  public deleteDocumentNextPromotionByStudy(study_id: number, doc_id: number): Observable<any> {
    return this.httpClient.delete(`${AppSettings.API_DOCUMENTS}` + study_id + `${AppSettings.NEXT_PROMOTION}` + doc_id);
  }

  public getDocumentsPromotion(
    study_id: number,
    promotion_id: number
  ): Observable<IDocumentationFullModule.IDocumentationFull[]> {
    // tslint:disable-next-line: max-line-length
    return this.httpClient.get<IDocumentationFullModule.IDocumentationFull[]>(
      `${AppSettings.API_DOCUMENTS}` + study_id + `${AppSettings.PROMOTION}` + promotion_id
    );
  }

  public createDocumentPromotionByStudy(study_id: number, promotion_id: number, obj: any): Observable<any> {
    return this.preparePostDocument(`${AppSettings.API_DOCUMENTS}` + study_id + `${AppSettings.PROMOTION}` + promotion_id, obj);
  }

  public deleteDocumentPromotion(study_id: number, doc_id: number): Observable<any> {
    return this.httpClient.delete(
      `${AppSettings.API_DOCUMENTS}` + study_id + `${AppSettings.NEXT_PROMOTIONS}` + doc_id
    );
  }

  // public DownloadFileByUrl(name: string, url: string) {
  //   this.httpClient
  //     .get(
  //       url, { responseType: 'blob' }
  //     )
  //     .subscribe(blob => {
  //       saveAs(blob, name);
  //     });
  // }
  //#endregion

}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { AppSettings } from '../shared/app-settings';
import { Socket } from 'ngx-socket-io';
import { Usuario } from '../classes/usuario';
import { Categoria } from '../classes/categoria';
import { Router } from '@angular/router';
import { url_prinex } from '../../environments/environment';



@Injectable({
  providedIn: 'root'
})

export class WebsocketService {

  public socketStatus = false;
  public usuario: Usuario = null;
  public categoria: Categoria = null;
  public url: string = url_prinex.url; // 'http://10.92.92.201:8001/query/execute';//'http://89.202.161.237:8001/query/execute';
  public urlcysnet = 'https://aedassfdc.cysnet.net/ws/createUserPrivateArea';

  constructor(
    private router: Router,
    public httpClient: HttpClient
  ) {
    this.cargarStorage();
  }

  preparePost(url: string, params: any): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Content-Type', 'application/json').set('Cache-Control', 'no-cache');
    // console.log('la url es: ' + url);
    // console.log(this.httpClient.post<any>(url, params, { headers: httpHeaders }));
    return this.httpClient.post<any>(url, params, { headers: httpHeaders });
  }

  preparePostCysnet(urlcysnet: string, params: any): Observable<any> {
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/raw')
      .set('Cache-Control', 'no-cache')
      .set('Access-Control-Allow-Origin', 'https://aedassfdc.cysnet.net/ws/createUserPrivateArea')
      .set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
      // tslint:disable-next-line:max-line-length
      .set('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization')
      .set('Access-Control-Allow-Credentials', 'Content-Type');
    // console.log('la url es: ' + urlcysnet);
    // console.log(this.httpClient.post<any>(urlcysnet, params, { headers: httpHeaders }));
    return this.httpClient.post<any>(urlcysnet, params);
  }


  preparePostPromo(url: string): Observable<any> {
    // console.log('la url es: ' + url);
    // tslint:disable-next-line:max-line-length
    return this.httpClient.post<any>(url, {user: 'aedasgrantuser', pass: 'nkuebrg2556oinsdg9wert', query: AppSettings.SELECT_DISTINCT_PROMOS()});
  }

  preparePostPromoName(url: string): Observable<any> {
    // console.log('la url es: ' + url);
    // tslint:disable-next-line:max-line-length
    return this.httpClient.post<any>(url, {user: 'aedasgrantuser', pass: 'nkuebrg2556oinsdg9wert', query: AppSettings.SELECT_CODANDNOMBREPROMOS()});
  }

  preparePostPromoNameUser(url: string, userEmail: string): Observable<any> {
    // tslint:disable-next-line:max-line-length
    return this.httpClient.post<any>(url, {user: 'aedasgrantuser', pass: 'nkuebrg2556oinsdg9wert', query: AppSettings.SELECT_CODANDNOMBREPROMOSUSER(userEmail)});
  }

  preparePostVivienda(url: string, idPromo: string): Observable<any> {
    // console.log('la url es: ' + url);
    // tslint:disable-next-line:max-line-length
    return this.httpClient.post<any>(url, {user: 'aedasgrantuser', pass: 'nkuebrg2556oinsdg9wert', query: AppSettings.SELECT_VIVIENDAS_V_C(idPromo)});
  }

  preparePostAnejo(url: string, select: string): Observable<any> {
    // console.log('la url es: ' + url);
    return this.httpClient.post<any>(url, {user: 'aedasgrantuser', pass: 'nkuebrg2556oinsdg9wert', query: select});
  }

  preparePostDatosVivienda(url: string, select: string): Observable<any> {
    // console.log('la url es: ' +url);
    return this.httpClient.post<any>(url, {user: 'aedasgrantuser', pass: 'nkuebrg2556oinsdg9wert', query: select});
  }

  preparePostCompradores(url: string, select: string): Observable<any> {
    console.log('ATENTO la url es: ' + select);
    return this.httpClient.post<any>(url, {user: 'aedasgrantuser', pass: 'nkuebrg2556oinsdg9wert', query: select});
  }

  preparePostViviendasLibres(url: string, select: string): Observable<any> {
    // console.log('la url es: ' + url);
    return this.httpClient.post<any>(url, {user: 'aedasgrantuser', pass: 'nkuebrg2556oinsdg9wert', query: select});
  }



  preparePostDNI(url: string, select: string): Observable<any> {
    // console.log('la url es: ' + url);
    return this.httpClient.post<any>(url, {user: 'aedasgrantuser', pass: 'nkuebrg2556oinsdg9wert', query: select});
  }

  preparePostInmuebles(url: string, select: string): Observable<any> {
    // console.log('la url es: ' + url);
    return this.httpClient.post<any>(url, {user: 'aedasgrantuser', pass: 'nkuebrg2556oinsdg9wert', query: select});
  }

  preparePostDocument(url: string, params: any): Observable<any> {
    const httpHeaders = new HttpHeaders()
      .set('Cache-Control', 'no-cache');
    return this.httpClient.post<any>(url, params, { headers: httpHeaders });
  }

     listen( evento: string ) {
     }

    loginWS( nombre: string, pass?: string ) {
      this.usuario = new Usuario( nombre );
      this.guardarStorage();
    }

    getpintarTrazas(emailUsuario: string) {
      console.log('URL TRAZAS: ' + `${AppSettings.API_PINTARTRAZAS}` + emailUsuario);
      return this.httpClient.get(`${AppSettings.API_PINTARTRAZAS}` + emailUsuario);
    }

    getpintarTrazasLogs(emailUsuario: string, message: string) {
      this.getpintarTrazas(emailUsuario).subscribe(
        data => {

          const dato = JSON.stringify(data);
          const dato2 = JSON.parse(dato);
          if (dato2['result'] === 1) {
            return console.log(message);
          } else {
            return '';
          }
        },
        error => {
          console.log(error, { depth: null});
        }
      );
    }

    getLogin(email: string, password: string): Observable<any> {
      const json = JSON.stringify({ email, password });
      // console.log('Pasando por la función getLogin');
      return this.preparePost(`${AppSettings.API_LOGIN}`, json);
    }

    getUserAdmin(email: string) {
      const json = JSON.stringify({email});
      return this.preparePost(`${AppSettings.API_PERFIL}`, json);
    }

    getUserApp(email: string) {
      const json = JSON.stringify({email});
      // return this.preparePost(`${AppSettings.API_PERFILUSERAPP}`, json);
      return this.httpClient.get(`${AppSettings.API_PERFILUSERAPP}` + email);
    }

    getNameCategory(idCategorias: number) {
      const json = JSON.stringify({idCategorias});
      // console.log('El json es:' + json);
      // console.log('La categoría será');
      return this.httpClient.get(`${AppSettings.API_CATEGORIAS}` + idCategorias);
    }

    getCategories() {
      // console.log('El nombre de todas las categorias es: ');
      return this.httpClient.get(`${AppSettings.API_ALLCATEGORIAS}`);
    }

    getUsuariosBBDD() {
      // console.log('El nombre de los usuarios es: ');
      return this.httpClient.get(`${AppSettings.API_ALLUSERS}`);
    }

    getUsuariosAltaPortales(idusuario: number) {
      return this.httpClient.get(`${AppSettings.API_USERSALTAPORTALES}` + idusuario);
    }


    getTalkdeskFile(idTalkdesk: string) {
      return this.httpClient.get(`${AppSettings.API_FILETALKDESK}` + idTalkdesk);
    }

    downloadTalkdeskFile(rutaFichero: string) {
      const regex = new RegExp('/', 'g');
      const json = rutaFichero.replace(regex, '_');

      return this.httpClient.get(`${AppSettings.API_DOWNLOADTALKDESK}` + json);
    }


    getMensajesSpam(fecha: string) {
      return this.httpClient.get(`${AppSettings.API_MENSAJESSPAM}` + fecha);
    }

    getUsuariosPortalesLeads(idportal: string) {
      return this.httpClient.get(`${AppSettings.API_USERSALTALEADS}` + idportal);
    }

    getProductos(idCategorias: number) {
      const json = JSON.stringify({idCategorias});
      // console.log('El json es:' + json);
      // console.log('Los productos serán');
      return this.httpClient.get(`${AppSettings.API_PRODUCTOS}` + idCategorias);
    }

    getPromocion() {
      // console.log('Las promociones serán');
     return this.preparePostPromo(this.url);
    }

    getPromocionName() {
      // console.log('Las promociones con nombres serán');
      return this.preparePostPromoName(this.url);
    }

    getPromocionNameUser(userEmail: string) {
      // console.log('Las promociones con nombres serán');
      return this.preparePostPromoNameUser(this.url, userEmail);
    }

    getViviendas(promotionId: string) {
      // console.log('Las promociones serán');
      return this.preparePostVivienda(this.url, promotionId);
    }

    getAnejos(selectAnejos: string) {
     // console.log('Los anejos serán');
      return this.preparePostAnejo(this.url, selectAnejos);
    }

    getDatosViviendas(selectVivienda: string) {
      // console.log('Los datos de la vivienda son');
      return this.preparePostDatosVivienda(this.url, selectVivienda);
    }

    getCompradores(selectCompradores: string) {
      // console.log('Los propietarios serán');
      return this.preparePostCompradores(this.url, selectCompradores);
    }

    getViviendasCompradores(selectViviendaCompradores: string) {
      // console.log('Los propietarios serán');
      return this.preparePostCompradores(this.url, selectViviendaCompradores);
    }

    getPromotionsUsers(selectPromocionesUsers: string) {
      // console.log('Las promociones serán');
      return this.preparePostCompradores(this.url, selectPromocionesUsers);
    }

    getViviendasLibres(selectViviendaLibre: string) {
      // console.log('Las viviendas libres son')
      return this.preparePostViviendasLibres(this.url, selectViviendaLibre);
    }

    getDNIPrinex(selectdni: string) {
      // console.log('Comprobación si DNI existe en Prinex');
      return this.preparePostDNI(this.url, selectdni);
    }

    getPropiedadesPrinex(selectdni: string) {
      // console.log('Comprobación Propiedades Prinex');
      return this.preparePostInmuebles(this.url, selectdni);
    }

    getAllProductos() {
      // console.log('Todos los productos serán');
      return this.httpClient.get(`${AppSettings.API_ALLPRODUCTOS}`);
    }

    addUserBBDD(emailUser: string, estadoUser: string, nameUser: string, perfilUser: string) {
      const json = JSON.stringify({ emailUser, estadoUser, nameUser, perfilUser });
      // console.log('Pasando por la función addUserBBDD');
      return this.preparePost(`${AppSettings.API_ADDUSER}`, json);
    }

    // tslint:disable-next-line:max-line-length
    addPortalesPromociones(idPortal: string, portal: string, idSalesForce: string, nombrePromocion: string, user: string, navigator: string, currentIp: string, url_portal: string) {
      const json = JSON.stringify({ idPortal, portal, idSalesForce, nombrePromocion, user, navigator, currentIp, url_portal});
      // console.log('Pasando por la función addUserBBDD');
      return this.preparePost(`${AppSettings.API_ADDUSERPORTALES}`, json);
    }

    // tslint:disable-next-line:max-line-length
    addProductsBBDD( idcategoria: number, nombreproducto: string, rutafichero: string, nombrepdf: string, nameproduct: string, ordenproduct: number) {
      const json = JSON.stringify({  idcategoria, nombreproducto, rutafichero, nombrepdf, nameproduct, ordenproduct});
      // console.log('Pasando por la función addProductsBBDD');
      return this.preparePost(`${AppSettings.API_ADDPRODUCTS}`, json);
    }

    // tslint:disable-next-line:max-line-length
    addMailingBBDD(navigator: string, id_correo: string, origen: string, currentIp: string, abierto: string, today: Date, ciudad: string) {
      const json = JSON.stringify({navigator, id_correo, origen, currentIp, abierto, today, ciudad});
      // console.log('Pasando por la función addProductsBBDD');
      return this.preparePost(`${AppSettings.API_ADDMAILING}`, json);
    }


    addCambioBBDD(campoPrinex: string, idPrinex: string, valornuevo: string, valorantiguo: string, user: string, aplicacion: string) {
      const json = JSON.stringify({campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion});
      // console.log('Pasando por la función addCambioBBDD');
      return this.preparePost(`${AppSettings.API_ADDCHANGES}`, json);
    }


    updateUserBBDD(idUser: number, emailUser: string, estadoUser: string, nameUser: string, perfilUser: string) {
      const json = JSON.stringify({idUser, emailUser, estadoUser, nameUser, perfilUser });
      // console.log('Pasando por la función updateUserBBDD');
      return this.preparePost(`${AppSettings.API_UPDATEUSER}`, json);
    }

    updateSpamBBDD(idSpam: number, valueSpam: string,  currentIp: string,  user: string, navigator: string) {
      const json = JSON.stringify({idSpam, valueSpam, currentIp,  user, navigator});
      // console.log('Pasando por la función updateUserBBDD');
      return this.preparePost(`${AppSettings.API_UPDATESPAM}`, json);
    }

    uploadDocument(obj: any): Observable<any> {
      // console.log("El object que envio es" +obj);
      return this.preparePostDocument(`${AppSettings.API_DOCUMENTS}`, obj);
    }

    deleteDocument(urlRemove: string) {
      const json = JSON.stringify({urlRemove});
      // console.log("Le pasamos la url: "+json);
      return this.preparePost(`${AppSettings.API_REMOVEDOCUMENTS}`, json);
    }

    deleteProductsBBDD(urlRemove: string) {
      const json = JSON.stringify({urlRemove});
      // console.log("Le pasamos la url para borrar: "+json);
      return this.preparePost(`${AppSettings.API_REMOVEDOCUMENTSBBDD}`, json);
    }



    logoutWS() {
      this.usuario = null;
      localStorage.removeItem('usuario');

      const payload = {
        nombre: 'sin-nombre'
      };
      this.router.navigateByUrl('');

    }



    getUsuario() {
      return this.usuario;
    }

    guardarStorage() {
      localStorage.setItem( 'usuario', JSON.stringify( this.usuario ) );
    }



    cargarStorage() {

        if (localStorage.getItem('usuario') ) {
          this.usuario = JSON.parse( localStorage.getItem('usuario') );
          this.loginWS( this.usuario.nombre );
        }
    }

    cargarStorageIdCategoria() {
      if (localStorage.getItem('categoria') ) {
        return JSON.parse( localStorage.getItem('categoria') );
      }
  }

  api() {
    // console.log('LA URL API ES:')
    return this.httpClient.get(`${AppSettings.API_APIREMOTE}`);
  }

  connectSaleForce(codigoprinex: string) {
    // console.log('LA CONEXION CON SALESFORCE:')
    const json = JSON.stringify({codigoprinex});
    // console.log("Le pasamos el codigo prinex: "+json);
    return this.httpClient.get(`${AppSettings.API_SALESFORCE}` + codigoprinex);
  }

  updateuserSaleForce(id: string, name: string, fechaactual: string) {
    // console.log('ACTUALIZANDO SALESFORCE:')
    const json = JSON.stringify({id, name, fechaactual});
    // console.log("Le pasamos el email y el name para salesforce: "+json);
    return this.preparePost(`${AppSettings.API_UPDATESALESFORCE}` , json);
  }

  updateoportunitySaleForce(id: string) {
    // console.log('ACTUALIZANDO SALESFORCE:')
    const json = JSON.stringify({id});
    // console.log("Le pasamos el email y el name para salesforce: "+json);
    return this.preparePost(`${AppSettings.API_UPDATEOPORTUNITYSALESFORCE}` , json);
  }


  // tslint:disable-next-line:max-line-length
  updateAltasPortales(idSalesForce: string, nombrePromocion: string, user: string, id: string, currentIp: string, navigator: string, url_site: string) {
    // console.log('ACTUALIZANDO SALESFORCE:')
    const json = JSON.stringify({idSalesForce, nombrePromocion, user, id, currentIp, navigator, url_site});
    // console.log("Le pasamos el email y el name para salesforce: "+json);
    return this.preparePost(`${AppSettings.API_UPDATEALTASPORTALES}` , json);
  }

  updateuserSaleForceAccount(id: string) {
    // console.log('ACTUALIZANDO SALESFORCE ACCOUNT:')
    const json = JSON.stringify({id});
    // console.log("Le pasamos el email y el name para salesforce: "+json);
    return this.preparePost(`${AppSettings.API_UPDATESALESFORCEACCOUNT}` , json);
  }

  getdatosUserSalesForce(id: string) {
    // console.log('SELECT CODIGO USER SALESFORCE:')
    const json = JSON.stringify({id});
    // console.log("Le pasamos el id: "+json);
    return this.httpClient.get(`${AppSettings.API_DATOSSALESFORCE}` + id);
  }

  getIdUserSalesForce(dni: string) {
    // console.log('SELECT CODIGO USER SALESFORCE:')
    const json = JSON.stringify({dni});
    // console.log("Le pasamos el dni: "+json);
    return this.httpClient.get(`${AppSettings.API_DNISALESFORCE}` + dni);
  }

  getIdInmuebleSalesForce(idUser: string) {
    // console.log('SELECT CODIGO VIVIENDA SALESFORCE:')
    const json = JSON.stringify({idUser});
    // console.log("Le pasamos el dni: "+json);
    return this.httpClient.get(`${AppSettings.API_IDINMUEBLESALESFORCE}` + idUser);
  }

  getNamePromotion(idPromotion: string) {
    // console.log('SELECT CODIGO VIVIENDA SALESFORCE:')
    const json = JSON.stringify({idPromotion});
    // console.log("Le pasamos el dni: "+json);
    return this.httpClient.get(`${AppSettings.API_NAMEPROMOTION}` + idPromotion);
  }

  getIdSecundarioInmuebleSalesForce(idUser: string) {
    // console.log('SELECT CODIGO VIVIENDA SALESFORCE:')
    const json = JSON.stringify({idUser});
    // console.log("Le pasamos el dni: "+json);
    return this.httpClient.get(`${AppSettings.API_IDSECUNDARIOINMUEBLESALESFORCE}` + idUser);
  }



  getDatosInmuebleSalesForce(idinmueble: string) {
    // console.log('SELECT CODIGO VIVIENDA SALESFORCE:')
    const json = JSON.stringify({idinmueble});
    // console.log("Le pasamos el dni: "+json);
    return this.httpClient.get(`${AppSettings.API_DATOSINMUEBLESALESFORCE}` + idinmueble);
  }



  getListaPromociones() {
    // console.log(`${AppSettings.API_PROMOCIONESALESFORCE}`);
    return this.httpClient.get(`${AppSettings.API_PROMOCIONESALESFORCE}`);
  }

  getListaOportunidadesGanadas(idPromocion: string) {
    const json = JSON.stringify({idPromocion});
    // console.log("Le pasamos el id: "+json);
    // console.log(`${AppSettings.API_PROMOCIONESALESFORCE}`);
    return this.httpClient.get(`${AppSettings.API_DATOSOPORTUNIDADGANADA}` + idPromocion);
  }

  getListaOportunidadesByUser(accountId: string) {
    const json = JSON.stringify({accountId});
    // console.log("Le pasamos el id: "+json);
    // console.log(`${AppSettings.API_PROMOCIONESALESFORCE}`);
    return this.httpClient.get(`${AppSettings.API_DATOSOPORTUNIDADBYUSER}` + accountId);
  }

  getListaEncuestasOportunidad(accountId: string, opportunityId: string) {
    return this.httpClient.get(`${AppSettings.API_DATOSENCUESTASOPORTUNIDAD}` + accountId + '/' + opportunityId);
  }

  getListaAllOportunidadesGanadas() {
    return this.httpClient.get(`${AppSettings.API_ALLPROMOCIONESALESFORCE}`);
  }


  getEmailUsuarioOportunidades(accountId: string) {
    const json = JSON.stringify({accountId});
    // console.log("Le pasamos el idAccount: "+json);
    return this.httpClient.get(`${AppSettings.API_EMAILSSALESFORCE}` + accountId);
  }

  getSendEmail(sendEmail: JSON) {
    const json = sendEmail;
    console.log(json);
    return this.preparePost(`${AppSettings.API_SENDEMAIL}` , json);
  }

  getIp() {
    return this.httpClient.get(`${AppSettings.API_GETAPI}`);
  }


  getEmailUsuarioAltaOportunidades(accountId: string) {
    const json = JSON.stringify({accountId});
    // console.log("Le pasamos el idAccount: "+json);
    return this.httpClient.get(`${AppSettings.API_EMAILSALTASALESFORCE}` + accountId);
  }



  getIDUserOportunidades(id: string) {
    const json = JSON.stringify({id});
    // console.log("Le pasamos el id: "+json);
    return this.httpClient.get(`${AppSettings.API_IDSALESFORCE}` + id);
  }

  getUsersSalesForce(fechainicial: string, fechafinal: string): Observable<any> {
    const json = JSON.stringify({ fechainicial, fechafinal });
    return this.httpClient.get(`${AppSettings.APIUSERSSALESFORCE}` + json);
  }

  getUsuariosOportunidad(oportunidad: string) {
    // console.log('SELECT CODIGO USUARIOS OPORTUNIDAD:')
    const json = JSON.stringify({oportunidad});
    // console.log("Le pasamos el ID de la oportunidad: "+json);
    return this.httpClient.get(`${AppSettings.API_DATOSUSUARIOSOPORTUNIDAD}` + oportunidad);
  }

  getNombreUsersSalesForce(codUsersOportunidad: string) {
    // console.log('SELECT CODIGO USUARIOS OPORTUNIDAD:')
    const json = JSON.stringify({codUsersOportunidad});
    // console.log("Le pasamos el ID de los usuarios en salesForce: "+json);
    return this.httpClient.get(`${AppSettings.API_NAMEUSUARIOSOPORTUNIDAD}` + codUsersOportunidad);
  }

  getIdPromocionProductCode(productcode: string) {
    // console.log('SELECT CON PRODUCTCODE SALESFORCE:')
    // console.log("Le pasamos el productCode: "+productcode);
    return this.httpClient.get(`${AppSettings.API_PRODUCTCODE}` + productcode);
  }


  getUserAdminAreaPrivada(email: string) {
    const json = JSON.stringify({email});
    // console.log(json);
    // return this.preparePostCysnet(this.urlcysnet);
    return this.httpClient.get(`${AppSettings.API_ADMINAREAPRIVADA}` + email);
  }


  // tslint:disable-next-line:max-line-length
  getCreateLead(email: string, name: string, idioma: string, comentario: string, ip: string, navegador: string, url: string, idportal: string): Observable<any> {
    comentario = 'ESTE TEXTO ES UNA AUTENTICA PRUEBA DE VALOR';
    const json = JSON.stringify({ email, name});
    alert(json);
    return this.httpClient.get(`${AppSettings.API_CREATELEAD}` + json);
  }

  getUserToken(token: string) {
    const json = JSON.stringify({token});
    // console.log(json);
    // return this.preparePostCysnet(this.urlcysnet);
    return this.httpClient.get(`${AppSettings.API_USERTOKEN}` + token);
  }

  getcheckEmail(email: string) {
    const json = JSON.stringify({email});
    // console.log(json);
    return this.httpClient.get(`${AppSettings.API_CHECKEMAIL}` + email);
  }






}

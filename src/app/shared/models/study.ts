export class Study {
  study_id: number;
  code: string;
  city: string;
  zone: string;
  created_at: Date;
  status_id: number;
  region_id: number;

  createObject(data: any, update: boolean) {}
}

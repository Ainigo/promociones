
import { api_version } from '../../environments/environment';
import { url_prinex } from '../../environments/environment';

export class AppSettings {
  public static get API_ENDPOINT(): string {
    return `${AppSettings.API_ENVIRONMENT}` + `${AppSettings.API_VERSION}`;
  }

  public static prodVersion() {
    return true;
  }

  public static get API_ENVIRONMENT(): string {
    if (this.prodVersion()) {
      // return '';
      // return 'http://localhost:3001';
      return api_version.version;
    }
  }

  public static get GET_ATOKEN(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getAToken';
  }

  public static get API_LOGOUT(): string {
    return `${AppSettings.API_ENDPOINT}` + '/logout';
  }

  public static get API_VERSION(): string {
    return '/api/v1';
  }

  public static get API_LOGIN(): string {
    return `${AppSettings.API_ENDPOINT}` + '/auth';
  }

  public static get API_PERFIL(): string {
    return `${AppSettings.API_ENDPOINT}` + '/perfil';
  }

  public static get API_PERFILUSERAPP(): string {
    return `${AppSettings.API_ENDPOINT}` + '/perfilUser/';
  }

  public static get API_PINTARTRAZAS(): string {
    return `${AppSettings.API_ENDPOINT}` + '/pintarTrazas/';
  }


  public static get API_PRODUCTOS(): string {
    return `${AppSettings.API_ENDPOINT}` + '/productos/';
  }

  public static get API_PROMOCIONES(): string {
    return `${AppSettings.API_ENDPOINT}` + '/productos/';
  }

  public static get API_CATEGORIAS(): string {
    return `${AppSettings.API_ENDPOINT}` + '/categorias/';
  }

  public static get API_ALLCATEGORIAS(): string {
    return `${AppSettings.API_ENDPOINT}` + '/allcategorias/';
  }

  public static get API_ALLPRODUCTOS(): string {
    return `${AppSettings.API_ENDPOINT}` + '/allproductos/';
  }

  public static get API_ALLUSERS(): string {
    return `${AppSettings.API_ENDPOINT}` + '/allusers/';
  }

  public static get API_USERSALTAPORTALES(): string {
    return `${AppSettings.API_ENDPOINT}` + '/altaportales/';
  }

  public static get API_FILETALKDESK(): string {
    return `${AppSettings.API_ENDPOINT}` + '/rutafiletalkdesk/';
  }

  public static get API_DOWNLOADTALKDESK(): string {
    return `${AppSettings.API_ENDPOINT}` + '/downloadfiletalkdesk/';
  }

  public static get API_MENSAJESSPAM(): string {
    return `${AppSettings.API_ENDPOINT}` + '/mensajesspam/';
  }

  public static get API_USERSALTALEADS(): string {
    return `${AppSettings.API_ENDPOINT}` + '/altaLeads/';
  }

  public static get API_ADDUSER(): string {
    return `${AppSettings.API_ENDPOINT}` + '/addusers';
  }

  public static get API_ADDUSERPORTALES(): string {
    return `${AppSettings.API_ENDPOINT}` + '/addusersPortales';
  }

  public static get API_UPDATEUSER(): string {
    return `${AppSettings.API_ENDPOINT}` + '/updateusers';
  }

  public static get API_UPDATESPAM(): string {
    return `${AppSettings.API_ENDPOINT}` + '/updatespam';
  }

  public static get API_ADDPRODUCTS(): string {
    return `${AppSettings.API_ENDPOINT}` + '/addproducts';
  }

  public static get API_ADDMAILING(): string {
    return `${AppSettings.API_ENDPOINT}` + '/addmailing';
  }


  public static get API_ADDCHANGES(): string {
    return `${AppSettings.API_ENDPOINT}` + '/addchanges';
  }

  public static get API_USERS(): string {
    return `${AppSettings.API_ENDPOINT}` + '/users';
  }

  public static get API_USER(): string {
    return `${AppSettings.API_ENDPOINT}` + '/users/';
  }

  public static get API_STUDIES(): string {
    return `${AppSettings.API_ENDPOINT}` + '/studies';
  }

  public static get API_EXPORT(): string {
    return `${AppSettings.API_ENDPOINT}` + '/export/';
  }

  public static get API_USER_STUDIES(): string {
    return '/studies';
  }

  public static get API_USER_STUDY(): string {
    return '/studies/';
  }

  public static get API_STUDY(): string {
    return `${AppSettings.API_ENDPOINT}` + '/studies/';
  }

  public static get API_VERSIONS(): string {
    return '/versions';
  }

  public static get API_STUDY_STATUS(): string {
    return `${AppSettings.API_ENDPOINT}` + '/studies/changeStudyStatus/';
  }

  public static get API_BASIC_TYPE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/basic_data/';
  }

  public static get API_PROMOTIONS(): string {
    return `${AppSettings.API_ENDPOINT}` + '/promotions';
  }

  public static get API_PROMOTION(): string {
    return `${AppSettings.API_ENDPOINT}` + '/promotions/';
  }

  public static get API_SECOND_HAND_RENTS(): string {
    return `${AppSettings.API_ENDPOINT}` + '/second_hand_rent';
  }

  public static get API_SECOND_HAND_RENT(): string {
    return `${AppSettings.API_ENDPOINT}` + '/second_hand_rent/';
  }

  public static get API_NEXT_PROMOTION(): string {
    return `${AppSettings.API_ENDPOINT}` + '/next_promotions/';
  }

  public static get API_BUILDINGS(): string {
    return '/buildings';
  }

  public static get API_BUILDING(): string {
    return `${AppSettings.API_ENDPOINT}` + '/buildings/';
  }

  public static get API_POST_BUILDING(): string {
    return '/buildings/';
  }

  public static get API_POST_QUALITIES(): string {
    return '/qualities';
  }

  public static get API_POST_PROGRAM(): string {
    return '/program';
  }

  public static get API_POST_FINANCING(): string {
    return '/finances';
  }

  public static get API_DATA_TYPE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/config/data_types';
  }

  public static get API_DATA_TYPES(): string {
    return `${AppSettings.API_ENDPOINT}` + '/config/data_types/';
  }

  public static get API_DATA_VALUE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/config/data_values';
  }

  public static get API_DATA_VALUES(): string {
    return `${AppSettings.API_ENDPOINT}` + '/config/data_values/';
  }

  public static get API_CONFIG_QUALITIES_FIRST_LEVEL(): string {
    return `${AppSettings.API_ENDPOINT}` + '/config/first_levels';
  }

  public static get API_DOCUMENTS(): string {
    return `${AppSettings.API_ENDPOINT}` + '/documentsUpload/';
  }

  public static get API_REMOVEDOCUMENTS(): string {
    return `${AppSettings.API_ENDPOINT}` + '/documentsRemove';
  }

  public static get API_REMOVEDOCUMENTSBBDD() : string {
    return `${AppSettings.API_ENDPOINT}` + '/documentsRemoveBBDD';
  }

  public static get PROMOTIONS(): string {
    return '/promotions';
  }

  public static get PROMOTION(): string {
    return '/promotions/';
  }

  public static get NEXT_PROMOTIONS(): string {
    return '/next_promotions';
  }

  public static get NEXT_PROMOTION(): string {
    return '/next_promotions/';
  }

  public static get API_APIREMOTE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/ip';
  }

  public static get API_SALESFORCE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/connectsalesforce/';
  }

  public static get API_UPDATESALESFORCE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/updatesalesforce';
  }

  public static get API_UPDATEOPORTUNITYSALESFORCE() : string {
    return `${AppSettings.API_ENDPOINT}` + '/updateoportunitysalesforce';
  }

  public static get API_UPDATEALTASPORTALES(): string {
    return `${AppSettings.API_ENDPOINT}` + '/updatealtaportales';
  }

  public static get API_UPDATESALESFORCEACCOUNT(): string {
    return `${AppSettings.API_ENDPOINT}` + '/updatesalesforceAccount';
  }

  public static get API_IDINMUEBLESALESFORCE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getIdInmueblessalesforce/';
  }

  public static get API_NAMEPROMOTION(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getNamePromotion/';
  }

  public static get API_IDSECUNDARIOINMUEBLESALESFORCE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getsecundariocodigoInmueble/';
  }


  public static get API_DATOSINMUEBLESALESFORCE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getIdDatosInmueblesalesforce/';
  }

  public static get API_PROMOCIONESALESFORCE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getPromocionesSalesForce/';
  }

  public static get API_DATOSOPORTUNIDADGANADA(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getOportunidadesGanada/';
  }

  public static get API_DATOSOPORTUNIDADBYUSER(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getOportunidadesByUser/';
  }

  public static get API_DATOSENCUESTASOPORTUNIDAD(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getencuestasOportunidad/';
  }

  public static get API_ALLPROMOCIONESALESFORCE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getAllOportunidadesGanadas/';
  }

  public static get API_ADMINAREAPRIVADA() : string {
    return `${AppSettings.API_ENDPOINT}` + '/getAdminAreaPrivada/';
  }

  public static get API_CREATELEAD() : string {
    return `${AppSettings.API_ENDPOINT}` + '/getCreateLead/';
  }

  public static get API_USERTOKEN() : string {
    return `${AppSettings.API_ENDPOINT}` + '/getTokenUser/';
  }

  public static get API_CHECKEMAIL() : string {
    return `${AppSettings.API_ENDPOINT}` + '/getcheckEmail/';
  }




  public static get API_EMAILSSALESFORCE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getEmailsSalesForce/';
  }

  public static get API_SENDEMAIL(): string {
    return `${AppSettings.API_ENDPOINT}` + '/sendEmail';
  }

  public static get API_GETAPI(): string {
    return `${AppSettings.API_ENDPOINT}` + '/ip';
  }


  public static get API_EMAILSALTASALESFORCE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getEmailsUsuarios/';
  }

  public static get API_IDSALESFORCE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getIdSalesForce/';
  }

  public static get APIUSERSSALESFORCE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getUsersSalesForce/';
  }




  public static get API_DATOSUSUARIOSOPORTUNIDAD(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getdatosUsuariosOportunidad/';
  }

  public static get API_NAMEUSUARIOSOPORTUNIDAD(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getnamesUsuariosOportunidad/';
  }

  public static get API_DNISALESFORCE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getcodusersalesforce/';
  }

  public static get API_DATOSSALESFORCE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getdatosusersalesforce/';
  }

  public static get API_PRODUCTCODE(): string {
    return `${AppSettings.API_ENDPOINT}` + '/getProductCode/';
  }

  // public static get API_PERFIL(): string {
  //   return `${AppSettings.API_ENDPOINT}` + '/perfil';
  // }


/***********************SELECTS VIVIENDAS************************************/

public static SELECT_PROMOCIONES(promotionid: string): string {
  const select = "SELECT * FROM FUNHIP WHERE UHPROM ='" + promotionid + "'";
  return select;
}

public static SELECT_VIVIENDASLIBRES(promotionid: string): string {
  const select = "SELECT UHEDIT FROM FUNHIP WHERE UHPROM ='" + promotionid + "' AND UHDEES = 'Libre' AND (UHEDIT LIKE 'V%' OR UHEDIT LIKE 'C%')";
  return select;
}

public static SELECT_ESTADOVIVIENDAS(): string {
  const select = "SELECT DISTINCT(UHDEES) FROM FUNHIP";
  return select;
}

public static SELECT_VIVIENDA(promotionid: string, vivienda: string): string {
  const selectVivienda = "SELECT * FROM FUNHIP WHERE UHPROM = '"+promotionid+"' AND UHEDIT = '"+vivienda+"'";
  return selectVivienda;
}

public static SELECT_PROMOCIONESUSERS(): string {
  const selectVivienda = "SELECT NAME, ID, IdentificadorPrinex__c from Promoción__C";
  return selectVivienda;
}

public static SELECT_PROPIETARIOS(promotionid: string, vivienda: string): string {
  const selectPropietarios = "SELECT CL.CLCODI CODIGO_PRINEX, F.UHPROM PROMOCION, F.UHNETO NETO_VIVIENDA, F.UHMTCO METROS_CONS, F.UHMTUT METROS_UTILES, F.UHPTCO EUROS_METRO_CONS, F.UHPTUT EUROS_METRO_UTIL, F.UHACTO  ACTO,F.UHEDIT VIVIENDA, CO.COCODI CODIGO_PRINEX, CL.CLNOMC NOMBRE, CL.CLAPEC APELLIDO, CL.CLCIF  DNI, CL.CLTELF FIJO, CL.CLMOVIL MOVIL, CL.CLTELX EMAIL, CO.COPROF PROFESION, CL.CLPOBL POBLACION, CL.CLPOVI PROVINCIA, CL.CLCODP CODIGO_POSTAL, CL.CLSGVP TIPO_VIA, CL.CLDOMI NOMBRE_VIA, CL.CLNUME NUMERO_VIA, CL.CLESCA ESCALERA, CL.CLPISO PISO, CL.CLPUER PUERTA, CL.CLESTC ESTADO_CIVIL, CL.CLANNA AÑO, CL.CLMENA MES, CL.CLDINA  DIA, FA.ACIBAN IBAN FROM FUNHIP F, FCOMPRA CO , FCLIENT CL, FACTOCO FA WHERE UHPROM = '"+promotionid+"' AND UHEDIT = '"+vivienda+"' AND CO.COPROM = F.UHPROM AND F.UHACTO = CO.COACTO AND CL.CLCODI = CO.COCODI AND FA.ACACTO = f.UHACTO AND FA.ACPROM = f.UHPROM ";
  return selectPropietarios;
}

public static SELECT_PROPIETARIOSPRINEX(dni: string): string {
  const selectPropietarios = "SELECT CL.CLCODI CODIGO_PRINEX, F.UHPROM PROMOCION,  F.UHALIA NOMBRE_INMUEBLE, F.UHNETO NETO_VIVIENDA, F.UHMTCO METROS_CONS, F.UHMTUT METROS_UTILES, F.UHPTCO EUROS_METRO_CONS, F.UHPTUT EUROS_METRO_UTIL, F.UHDEES ESTADO, F.UHACTO  ACTO,F.UHEDIT VIVIENDA, CO.COCODI CODIGO_PRINEX, CL.CLNOMC NOMBRE, CL.CLAPEC APELLIDO, CL.CLCIF  DNI, CL.CLTELF FIJO, CL.CLMOVIL MOVIL, CL.CLTELX EMAIL, CO.COPROF PROFESION, CL.CLPOBL POBLACION, CL.CLPOVI PROVINCIA, CL.CLCODP CODIGO_POSTAL, CL.CLSGVP TIPO_VIA, CL.CLDOMI NOMBRE_VIA, CL.CLNUME NUMERO_VIA, CL.CLESCA ESCALERA, CL.CLPISO PISO, CL.CLPUER PUERTA, CL.CLESTC ESTADO_CIVIL, CL.CLANNA AÑO, CL.CLMENA MES, CL.CLDINA  DIA, FA.ACIBAN IBAN FROM FUNHIP F, FCOMPRA CO , FCLIENT CL, FACTOCO FA WHERE CO.COPROM = F.UHPROM AND F.UHACTO = CO.COACTO AND CL.CLCODI = CO.COCODI AND FA.ACACTO = f.UHACTO AND FA.ACPROM = f.UHPROM  and CL.CLCIF like '%"+dni+"%' order by F.UHALIA desc"
  return selectPropietarios;
}

public static SELECT_DNI(dni: string): string {
  const selectdni = "SELECT COMAIL, COCODI, CONOMP, COAPEP, COTLFP, COMOVIL FROM FCOMPRA WHERE CODNI LIKE '%"+dni+"%'";
  return selectdni;
}

public static SELECT_NOMBREPROMOS(promotionid: string): string {
  const selectNombrePromo = "SELECT * FROM FPROMOC where PMPROM =" +promotionid;
  return selectNombrePromo;
}

public static SELECT_CODANDNOMBREPROMOS(): string {
  const selectcodandnombrePromo = "SELECT PMPROM, PMNOMP FROM FPROMOC ORDER BY 1";
  return selectcodandnombrePromo;
}

public static SELECT_CODANDNOMBREPROMOSUSER(userEmail: string): string {
  const selectcodandnombrePromo = "select  puprom, pmnomp from facprom,fusuari,fpromoc where puusua = ususua and puprom = pmprom AND lower(usmail) = '" +userEmail+ "' ORDER BY 1";
  return selectcodandnombrePromo;
}

public static SELECT_DISTINCT_PROMOS(): string {
  const selectdistinctPromo = "SELECT  DISTINCT (UHPROM) FROM FUNHIP ORDER BY 1";
  return selectdistinctPromo;
}

public static SELECT_VIVIENDAS_V_C (promotionid: string): string {
  const selectviviendas_v_c = "SELECT UHEDIT FROM FUNHIP WHERE UHPROM = "+promotionid+" AND (UHEDIT LIKE 'V%' OR UHEDIT LIKE 'C%')";
  return selectviviendas_v_c;
}

public static SELECT_USUARIOS(fechaInicial: string, fechaFinal: string) : string {

  const selectusuarios = "SELECT CL.CLCODI CODIGO_PRINEX, CL.CLNOMC NOMBRE, CL.CLAPEC APELLIDO, CL.CLCIF DNI, CL.CLTELF FIJO, CL.CLPROF PROFESION, CL.CLMOVIL MOVIL, CL.CLTELX EMAIL, CL.CLPOBL POBLACION, CL.CLPOVI PROVINCIA, CL.CLCODP CODIGO_POSTAL, CL.CLSGVP TIPO_VIA, CL.CLDOMI NOMBRE_VIA, CL.CLNUME NUMERO_VIA, CL.CLESCA ESCALERA, CL.CLPISO PISO, CL.CLPUER PUERTA, CL.CLESTC ESTADO_CIVIL, CL.CLANNA AÑO, CL.CLMENA MES, CL.CLDINA DIA, (CL.FECALTA)+1 F_ALTA, CL.HORALTA H_ALTA, CL.CLTIPO TIPO, CL.CLCPAI CODPAIS, P.TPNOMB PAIS FROM FCLIENT CL, FTABPAI P WHERE CL.FECALTA BETWEEN TO_DATE ('"+fechaInicial+"', 'yyyy/mm/dd') AND TO_DATE ('"+fechaFinal+"', 'yyyy/mm/dd') AND CL.CLCPAI = P.TPCODI";

  //"SELECT CL.CLCODI CODIGO_PRINEX, F.UHPROM PROMOCION, F.UHNETO NETO_VIVIENDA, F.UHMTCO METROS_CONS, F.UHMTUT METROS_UTILES, F.UHPTCO EUROS_METRO_CONS, F.UHPTUT EUROS_METRO_UTIL, F.UHACTO ACTO,F.UHEDIT VIVIENDA, CO.COCODI CODIGO_PRINEX, CL.CLNOMC NOMBRE, CL.CLAPEC APELLIDO, CL.CLCIF  DNI, CL.CLTELF FIJO, CL.CLMOVIL MOVIL, CL.CLTELX EMAIL, CO.COPROF PROFESION, CL.CLPOBL POBLACION, CL.CLPOVI PROVINCIA, CL.CLCODP CODIGO_POSTAL, CL.CLSGVP TIPO_VIA, CL.CLDOMI NOMBRE_VIA, CL.CLNUME NUMERO_VIA, CL.CLESCA ESCALERA, CL.CLPISO PISO, CL.CLPUER PUERTA, CL.CLESTC ESTADO_CIVIL, CL.CLANNA AÑO, CL.CLMENA MES, CL.CLDINA  DIA, FA.ACIBAN IBAN, CL.FECALTA FALTA, CL.HORALTA HALTA, P.TPNOMB PAIS, CL.CLCPAI CODPAIS, f.UHPROM  , f.UHEDIT, fa.ACESTA , ES.EUDENO ESTADO_VIVIENDA , cl.CLTIPO , CO.CONONO FROM FUNHIP F, FCOMPRA CO , FCLIENT CL, FACTOCO FA, FTABPAI P , FTABEUH ES WHERE CO.COPROM = F.UHPROM AND F.UHACTO = CO.COACTO AND CL.CLCODI = CO.COCODI AND FA.ACACTO = f.UHACTO AND FA.ACPROM = f.UHPROM AND CL.FECALTA BETWEEN TO_DATE ('"+fechaInicial+"', 'yyyy/mm/dd') AND TO_DATE ('"+fechaFinal+"', 'yyyy/mm/dd') AND CL.CLCPAI = P.TPCODI AND fa.ACESTA = ES.EUESTA"
  //"SELECT CL.CLCODI CODIGO_PRINEX, F.UHPROM PROMOCION, F.UHNETO NETO_VIVIENDA, F.UHMTCO METROS_CONS, F.UHMTUT METROS_UTILES, F.UHPTCO EUROS_METRO_CONS, F.UHPTUT EUROS_METRO_UTIL, F.UHDEES ESTADO, F.UHACTO ACTO,F.UHEDIT VIVIENDA, CO.COCODI CODIGO_PRINEX,CL.CLNOMC NOMBRE, CL.CLAPEC APELLIDO, CL.CLCIF  DNI, CL.CLTELF FIJO, CL.CLMOVIL MOVIL, CL.CLTELX EMAIL, CO.COPROF PROFESION, CL.CLPOBL POBLACION, CL.CLPOVI PROVINCIA, CL.CLCODP CODIGO_POSTAL, CL.CLSGVP TIPO_VIA, CL.CLDOMI NOMBRE_VIA, CL.CLNUME NUMERO_VIA, CL.CLESCA ESCALERA, CL.CLPISO PISO, CL.CLPUER PUERTA, CL.CLESTC ESTADO_CIVIL, CL.CLANNA AÑO, CL.CLMENA MES, CL.CLDINA  DIA, FA.ACIBAN IBAN, CL.FECALTA FALTA, CL.HORALTA HALTA , cl.CLTIPO , f.UHPROM  , f.UHEDIT, fa.ACESTA FROM FUNHIP F, FCOMPRA CO , FCLIENT CL, FACTOCO FA WHERE CO.COPROM = F.UHPROM AND F.UHACTO = CO.COACTO AND CL.CLCODI = CO.COCODI AND FA.ACACTO = f.UHACTO AND FA.ACPROM = f.UHPROM AND CL.FECALTA BETWEEN TO_DATE ('"+fechaInicial+"', 'yyyy/mm/dd') AND TO_DATE ('"+fechaFinal+"', 'yyyy/mm/dd')";
  return selectusuarios;
}

public static SELECT_VIVIENDA_USUARIOS(codigoPrinex: string) : string {

  const selectviviendausuarios = "SELECT CL.CLCODI PRINEX, F.UHALIA NOMBRE_VIVIENDA,F.UHPROM PROMOCION, F.UHNETO NETO_VIVIENDA, F.UHMTCO METROS_CONS, F.UHMTUT METROS_UTILES, F.UHPTCO EUROS_METRO_CONS, F.UHPTUT EUROS_METRO_UTIL, F.UHACTO ACTO, F.UHEDIT VIVIENDA, F.UHPROM PROMOCION, CO.CONONO CONO, FA.ACESTA CESTA, ES.EUDENO ESTADO_VIVIENDA FROM FUNHIP F, FCOMPRA CO, FCLIENT CL, FACTOCO FA, FTABEUH ES WHERE CO.COPROM = F.UHPROM AND F.UHACTO = CO.COACTO AND CL.CLCODI = CO.COCODI AND FA.ACACTO = F.UHACTO AND FA.ACPROM = F.UHPROM AND CL.CLCODI = '"+codigoPrinex+"' AND fa.ACESTA = ES.EUESTA"
  //"SELECT CL.CLCODI PRINEX, F.UHALIA NOMBRE_VIVIENDA,F.UHPROM PROMOCION, F.UHNETO NETO_VIVIENDA, F.UHMTCO METROS_CONS, F.UHMTUT METROS_UTILES, F.UHPTCO EUROS_METRO_CONS, F.UHPTUT EUROS_METRO_UTIL, F.UHACTO ACTO, F.UHEDIT VIVIENDA, F.UHPROM PROMOCION, CO.CONONO CONO, FA.ACESTA CESTA, ES.EUDENO ESTADO_VIVIENDA FROM FUNHIP F, FCOMPRA CO, FCLIENT CL, FACTOCO FA, FTABEUH ES WHERE CO.COPROM = F.UHPROM AND F.UHACTO = CO.COACTO AND CL.CLCODI = CO.COCODI AND FA.ACACTO = F.UHACTO AND FA.ACPROM = F.UHPROM CL.CLCODI = '"+codigoPrinex+"'AND fa.ACESTA = ES.EUESTA";
  //"SELECT CL.CLCODI CODIGO_PRINEX, F.UHPROM PROMOCION, F.UHNETO NETO_VIVIENDA, F.UHMTCO METROS_CONS, F.UHMTUT METROS_UTILES, F.UHPTCO EUROS_METRO_CONS, F.UHPTUT EUROS_METRO_UTIL, F.UHACTO ACTO,F.UHEDIT VIVIENDA, CO.COCODI CODIGO_PRINEX, CL.CLNOMC NOMBRE, CL.CLAPEC APELLIDO, CL.CLCIF  DNI, CL.CLTELF FIJO, CL.CLMOVIL MOVIL, CL.CLTELX EMAIL, CO.COPROF PROFESION, CL.CLPOBL POBLACION, CL.CLPOVI PROVINCIA, CL.CLCODP CODIGO_POSTAL, CL.CLSGVP TIPO_VIA, CL.CLDOMI NOMBRE_VIA, CL.CLNUME NUMERO_VIA, CL.CLESCA ESCALERA, CL.CLPISO PISO, CL.CLPUER PUERTA, CL.CLESTC ESTADO_CIVIL, CL.CLANNA AÑO, CL.CLMENA MES, CL.CLDINA  DIA, FA.ACIBAN IBAN, CL.FECALTA FALTA, CL.HORALTA HALTA, P.TPNOMB PAIS, CL.CLCPAI CODPAIS, f.UHPROM  , f.UHEDIT, fa.ACESTA , ES.EUDENO ESTADO_VIVIENDA , cl.CLTIPO , CO.CONONO FROM FUNHIP F, FCOMPRA CO , FCLIENT CL, FACTOCO FA, FTABPAI P , FTABEUH ES WHERE CO.COPROM = F.UHPROM AND F.UHACTO = CO.COACTO AND CL.CLCODI = CO.COCODI AND FA.ACACTO = f.UHACTO AND FA.ACPROM = f.UHPROM AND CL.FECALTA BETWEEN TO_DATE ('"+fechaInicial+"', 'yyyy/mm/dd') AND TO_DATE ('"+fechaFinal+"', 'yyyy/mm/dd') AND CL.CLCPAI = P.TPCODI AND fa.ACESTA = ES.EUESTA"
  //"SELECT CL.CLCODI CODIGO_PRINEX, F.UHPROM PROMOCION, F.UHNETO NETO_VIVIENDA, F.UHMTCO METROS_CONS, F.UHMTUT METROS_UTILES, F.UHPTCO EUROS_METRO_CONS, F.UHPTUT EUROS_METRO_UTIL, F.UHDEES ESTADO, F.UHACTO ACTO,F.UHEDIT VIVIENDA, CO.COCODI CODIGO_PRINEX,CL.CLNOMC NOMBRE, CL.CLAPEC APELLIDO, CL.CLCIF  DNI, CL.CLTELF FIJO, CL.CLMOVIL MOVIL, CL.CLTELX EMAIL, CO.COPROF PROFESION, CL.CLPOBL POBLACION, CL.CLPOVI PROVINCIA, CL.CLCODP CODIGO_POSTAL, CL.CLSGVP TIPO_VIA, CL.CLDOMI NOMBRE_VIA, CL.CLNUME NUMERO_VIA, CL.CLESCA ESCALERA, CL.CLPISO PISO, CL.CLPUER PUERTA, CL.CLESTC ESTADO_CIVIL, CL.CLANNA AÑO, CL.CLMENA MES, CL.CLDINA  DIA, FA.ACIBAN IBAN, CL.FECALTA FALTA, CL.HORALTA HALTA , cl.CLTIPO , f.UHPROM  , f.UHEDIT, fa.ACESTA FROM FUNHIP F, FCOMPRA CO , FCLIENT CL, FACTOCO FA WHERE CO.COPROM = F.UHPROM AND F.UHACTO = CO.COACTO AND CL.CLCODI = CO.COCODI AND FA.ACACTO = f.UHACTO AND FA.ACPROM = f.UHPROM AND CL.FECALTA BETWEEN TO_DATE ('"+fechaInicial+"', 'yyyy/mm/dd') AND TO_DATE ('"+fechaFinal+"', 'yyyy/mm/dd')";
  return selectviviendausuarios;
}

public static SELECT_USUARIOS_COMPLETA(fechaInicial: string, fechaFinal: string) : string {

  const selectusuarioscompleta = "SELECT CL.CLCODI CODIGO_PRINEX, CL.CLNOMC NOMBRE, CL.CLAPEC APELLIDO, CL.CLCIF DNI, CL.CLTELF FIJO, CL.CLPROF PROFESION, CL.CLMOVIL MOVIL, CL.CLTELX EMAIL, CL.CLPOBL POBLACION, CL.CLPOVI PROVINCIA, CL.CLCODP CODIGO_POSTAL, CL.CLSGVP TIPO_VIA, CL.CLDOMI NOMBRE_VIA, CL.CLNUME NUMERO_VIA, CL.CLESCA ESCALERA, CL.CLPISO PISO, CL.CLPUER PUERTA, CL.CLESTC ESTADO_CIVIL, CL.CLANNA AÑO, CL.CLMENA MES, CL.CLDINA DIA, (CL.FECALTA)+1 F_ALTA, CL.HORALTA H_ALTA, CL.CLTIPO TIPO, CL.CLCPAI CODPAIS, P.TPNOMB PAIS, F.UHALIA NOMBRE_VIVIENDA,F.UHPROM PROMOCION, F.UHNETO NETO_VIVIENDA, F.UHMTCO METROS_CONS, F.UHMTUT METROS_UTILES, F.UHPTCO EUROS_METRO_CONS, F.UHPTUT EUROS_METRO_UTIL, F.UHACTO ACTO, F.UHEDIT VIVIENDA, F.UHPROM PROMOCION, CO.CONONO CONO, FA.ACESTA CESTA, ES.EUDENO ESTADO_VIVIENDA FROM FCLIENT CL, FTABPAI P, FUNHIP F, FCOMPRA CO, FACTOCO FA, FTABEUH ES WHERE CL.FECALTA BETWEEN TO_DATE ('"+fechaInicial+"', 'yyyy/mm/dd') AND TO_DATE ('"+fechaFinal+"', 'yyyy/mm/dd') AND CL.CLCPAI = P.TPCODI AND CO.COPROM = F.UHPROM AND F.UHACTO = CO.COACTO AND CL.CLCODI = CO.COCODI AND FA.ACACTO = F.UHACTO AND FA.ACPROM = F.UHPROM AND fa.ACESTA = ES.EUESTA"
  return selectusuarioscompleta;
}

public static URL_POST(): string {
  const url_post = url_prinex.url;//'http://10.92.92.201:8001/query/execute';//'http://89.202.161.237:8001/query/execute';
  return url_post;
}

/***********************************************************/




}

export interface IBasicType {
  value_id: number;
  value: string;
  label: string;
  cnt_type: ICntType;
}

export enum BasicTypeValue {
  CCAA = '',
  PromotionsTypes = 'Tipo de promoción'
}

export const constantes = {
  promoTypes: 'Tipo de promocion'
};

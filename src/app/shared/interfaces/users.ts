export interface IUser {
  user_id: number;
  email: string;
  name: string;
  valid: boolean;
  last_login?: string;
  usr_rol: Usrrol;
  usr_tokens: Usrtoken[];
}

export interface Usrtoken {
  token_id: number;
  token: string;
  user_id: number;
}

export interface Usrrol {
  role_id: number;
  role_name: string;
}

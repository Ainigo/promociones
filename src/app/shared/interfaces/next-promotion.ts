import { ICntValue } from './cntvalue';

export declare module INextPromotionModule {
  interface INextPromotionFull {
    next_promotion_id: number;
    promoter: string;
    name: string;
    address: string;
    coordinates: string;
    study_id: number;
    promo_type: ICntValue;
    environment: ICntValue;
  }
}

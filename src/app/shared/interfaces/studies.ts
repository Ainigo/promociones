import { ICntValue } from './cntvalue';

export interface IStudy {
  study_id: number;
  code: string;
  city: string;
  zone: string;
  enabled: boolean;
  created_at: Date;
  status: ICntValue;
  region: ICntValue;
  territorial: ICntValue;
}

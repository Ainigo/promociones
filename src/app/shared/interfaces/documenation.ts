export declare module IDocumentationFullModule {
  export interface IDocumentationFull {
    name: string;
    type: string;
    downloadUrl: string;
  }
}

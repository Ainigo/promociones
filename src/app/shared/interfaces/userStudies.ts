import { Study } from '../models/study';

export interface IUserStudies {
  userStudies: Study[];
  freeStudies: Study[];
}

import { Cnttype } from './promotionFull';

export declare module IQualityFullModule {
  export interface IQualityFull {
    label: string;
    value: string;
    cnt_types: Cnttype[];
  }
}

import { ICntValue } from './cntvalue';
export declare module ISecondHandRentModule {
  interface ISecondHandRentFull {
    study_id: number;
    shr_id: number;
    address: string;
    coordinates: string;
    year: number;
    floor: number;
    rooms: number;
    bathrooms: number;
    area: string;
    registry: string;
    price: string;
    url: string;
    comments?: any;
    transaction: ICntValue;
    second_hand_status: ICntValue;
    commercial_status: ICntValue;
  }
}

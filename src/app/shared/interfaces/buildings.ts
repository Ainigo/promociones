import { ICntValue } from './cntvalue';

export interface IListBuilding {
  promotion_id: string;
  version: number;
  buildings: IBuilding[];
}

export interface IBuildingFull {
  promotion_id: string;
  version: number;
  buildings: IBuilding;
}

export interface IBuilding {
  building_id: number;
  rooms: number;
  bathrooms: number;
  terrace_area: number;
  pvp_commercial: number;
  created_at: string;
  updated_at: string;
  observations: string;
  status?: ICntValue;
  building_type?: ICntValue;
  pro_building_multi?: IMultipleBuilding;
  pro_building_single?: ISingleBuilding;
}

interface IBaseProBuilding {
  building_id: number;
  version_id: number;
}

export interface IMultipleBuilding extends IBaseProBuilding {
  util_area: number;
  constructed_area: string;
  garage: boolean;
  garages_number: number;
  pvp_garage: string;
  storage: boolean;
  pvp_storage: string;
  building_id: number;
  version_id: number;
  floor: ICntValue;
}

export interface ISingleBuilding extends IBaseProBuilding {
  plot_area: number;
  sbr: number;
  spb: number;
  spp: number;
  sbc: number;
  sbr_constructed: number;
  spb_constructed: number;
  spp_constructed: number;
  sbc_constructed: number;
  single_type: ICntValue;
}

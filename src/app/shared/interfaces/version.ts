export interface Version {
  version_id: number;
  created_at: string;
  version_number: number;
  study_id: number;
}

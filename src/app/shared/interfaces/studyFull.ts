import { IStudy } from './studies';
import { Version } from './version';

export declare module IStudyFullModule {
  export interface IStudyFull {
    study: IStudy;
    study_status: Studystatus;
    second_hand_rent: Secondhandrent[];
    next_promotions: Nextpromotion[];
    promotions: Promotion[];
    version: Version[];
  }

  export interface Promotion {
    promotion_id: number;
    promoter: string;
    name: string;
    address: string;
    coordinates: string;
    promo_type: Status;
  }

  interface Studystatus {
    previous: string;
    actual: string;
    next: string;
  }
}

export interface Secondhandrent {
  shr_id: number;
  address: string;
  coordinates: string;
  rooms: number;
  price: string;
  transaction: Status;
}

export interface Status {
  value_id: number;
  value: string;
  label: string;
}

export interface Nextpromotion {
  next_promotion_id: number;
  promoter: string;
  name: string;
  address: string;
  coordinates: string;
  promo_type: Status;
  environment: Status;
}

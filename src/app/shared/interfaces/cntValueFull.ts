export interface CntValueFull {
  value_id: number;
  value: string;
  label: string;
  enabled: boolean;
}

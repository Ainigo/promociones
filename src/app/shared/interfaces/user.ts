import { IUser } from './users';

declare module IUsers {
  interface RootObject {
    user: IUser;
    usr_rol: Usrrol;
  }

  interface Usrrol {
    role_id: number;
    role_name: string;
  }
}

export interface Masters {
  type_id: number;
  type_value: string;
  type_label: string;
  first_level_id?: number;
  enabled: boolean;
  data_type: number;
}

export interface FirstLevelQualities {
  first_level_id: number;
  value: string;
  label: string;
  enabled: boolean;
  cnt_types: Cnttype[];
}

interface Cnttype {
  type_id: number;
  type_value: string;
  type_label: string;
  first_level_id: number;
  enabled: boolean;
  data_type: number;
}

interface ICntType {
  type_id: number;
  type_value: string;
  type_label: string;
}

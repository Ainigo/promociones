import { ICntValue } from './cntvalue';
//import { Study } from '../models/study';

export declare module IPromotionFullModule {
  export interface IPromotionFull {
    promotion: Promotion;
    building: Building[];
    finance: Finance;
    program: Program[];
    quality: Quality[];
    //study: Study;
  }

  export interface Quality {
    label: string;
    value: string;
    cnt_types: Cnttype[];
  }

  export interface Program {
    room_number: number;
    room_label: string;
    pro_program: Proprogram;
  }

  export interface Proprogram {
    program_id: number;
    initial: number;
    stock: number;
    rooms: number;
    version_id: number;
    promotion_id: number;
    created_at: string;
    updated_at?: any;
  }

  interface Building {
    building_id: number;
    rooms: number;
    pvp_commercial: number;
    version_id: number;
    status: ICntType;
    pro_building_multi?: Probuildingmulti;
  }

  interface Finance {
    banc: string;
    reserve: string;
    on_sign: number;
    postponed_during_construction: number;
    additional_payment: string;
    mortgage: number;
    endorse: number;
    promotion_id: number;
    deferment_payments: ICntValue;
  }

  interface Probuildingmulti {
    floor: number;
    util_area: string;
  }
}

export interface Promotion {
  promotion_id: number;
  promoter: string;
  name: string;
  address: string;
  coordinates: string;
  marketer?: any;
  sales_start_date: string;
  web?: any;
  telephone?: any;
  observations?: any;
  study_id: number;
  promo_type: ICntValue;
  environment: ICntValue;
  protection_type: ICntValue;
  pro_promotion_versions: Propromotionversion[];
  architect: string;
  comparable: boolean;
}

export interface Propromotionversion {
  delivery_date: string;
  version_id: number;
  work_status: ICntValue;
}

export interface Cnttype {
  type_id: number;
  type_value: string;
  type_label: string;
  data_type: number;
  pro_quality?: Proquality | null;
}

export interface Proquality {
  type_id: number;
  type_value: string;
  type_label: string;
  data_type: number;
  value: boolean;
  cnt_value?: ICntValue;
}

import { Encuestas } from './encuestas';

export class Oportunidades {

  ID: string;
  NAME: string;
  PROMOCION: string;
  STAGE: string;
  ENCUESTAS: Encuestas[];

  constructor() {
  }
}

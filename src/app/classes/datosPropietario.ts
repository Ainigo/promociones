import { SafeUrl, SafeResourceUrl } from "@angular/platform-browser";
import { datosVivienda } from './datosVivienda';



export class datosPropietario {

    CODIGO_PRINEX: Number;
    VIVIENDA: string;
    PROMOCION: string;
    NOMBRE: string;
    APELLIDOS: string;
    DNI: string;
    MOVIL: string;
    TELEFONOFIJO: string;
    EMAIL: string;
    PROFESION: string;
    POBLACION: string;
    PROVINCIA: string;
    CODIGO_POSTAL: string;
    TIPO_VIA: string;
    NOMBRE_VIA: string;
    NUMERO_VIA: Number;
    ESCALERA: string;
    PISO: string;
    PUERTA: string;
    ESTADO_CIVIL: string;
    ANIO: Number;
    MES: Number;
    DIA: Number;
    IBAN: string;
    FECHALTA: string; //"2020-03-15T23:00:00.000Z"
    HORALTA: string;//"15:58:31"
    CLTIPO: string;  //"F"
    UHPROM: Number; //2210
    UHEDIT: string; //"VI-03 -A"
    ACESTA: Number; //10
    PAIS: string;    
    DATOSVIVIENDAS: datosVivienda [];

    // //Campos Vivienda
    // NOMBRE_VIVIENDA: string;
    // PRECIO_VIVIENDA: number;
    // METROS_CONSTRUIDOS: number;
    // METROS_HABILES: number;
    // ESTADO_VIVIENDA: number;

    // //DATOS VIVIENDA NUEVOS
    // EUROS_METRO_CONS: number;
    // EUROS_METRO_UTIL: number;
    // ACTO: number;


    constructor() {
    }



}

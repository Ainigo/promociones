import { SafeUrl, SafeResourceUrl } from "@angular/platform-browser";



export class ProductosCat {

    nombre_producto: string;
    url_producto: SafeResourceUrl;
    id_producto: number;
    id_categoria: number;
    name: string;
    nombrePDF: string;
    urlsproducts: string;
    orden: number;


    constructor() {
    }



}


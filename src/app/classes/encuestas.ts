
export class Encuestas {

  ID: string;
  ID_URL: string;
  NAME: string;
  URL: string;
  CREATED_DATE: Date;
  FILL_DATE: Date;
  FILLED: string;

  constructor() {
  }
}

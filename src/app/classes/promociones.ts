import { SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { datosPropietario} from '../classes/datosPropietario';
import { datosAnejos } from '../classes/datosAnejos';
import { datosVivienda } from '../classes/datosVivienda';

export class Promociones {

  UHPROM: string;
  UHALIA: string [];
  COMPRADORES: datosPropietario [];
  DATOSANEJOS: datosAnejos [];
  DATOSVIVIENDAS: datosVivienda [];

  constructor() {
  }
}


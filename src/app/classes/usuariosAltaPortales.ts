


export class UsuariosAltaPortales {

    public id : number;
    public id_portal : number;
    public portal : string;
    public id_salesforce : string;
    public nombre_promocion : string;
    public fecha_creacion : Date;
    public user : string;
    public nombre : string;
    public email : string;
    public mensaje : string;
    public referencia : string;
    public fecha_procesado : Date;
    public fecha_lead : Date;
    public ip : string;
    public navegador : string;
    public validacion_spam : string;
    public idioma : string;

    constructor() {
    }



}


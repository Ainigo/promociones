import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Categoria} from '../../classes/categoria';
import { ProductosService } from 'src/app/services/productos.service';
import { WebsocketService } from '../../services/websocket.service';
import { finalize } from 'rxjs/operators';
import { Promociones} from '../../classes/promociones';
import { datosPropietario} from '../../classes/datosPropietario';
import { AuthenticationService } from '../../core';
import * as $ from 'jquery';
import { datosAnejos } from 'src/app/classes/datosAnejos';
import { datosVivienda } from 'src/app/classes/datosVivienda';
import { AppSettings } from '../../shared/app-settings';
import { datosPromociones } from 'src/app/classes/datosPromociones';

declare var html2canvas: any;
declare var jsPDF: any;
declare var req: any;


@Component({
  selector: 'app-disponibilidad',
  templateUrl: './disponibilidad.component.html',
  styleUrls: ['./disponibilidad.component.css']
})



export class DisponibilidadComponent implements OnInit {

  public promociones: datosPromociones [] = [];
  public viviendas: string [] = [];
  public Viviendasanejos: Promociones [] = [];
  public Viviendaanejosdownload: Promociones [] = [];
  public PromocionesID: string;
  public nombreUsuario: string;
  public arrayViviendas: string [] = [];
  public cambios: string [] = [];
  public usuario: datosPropietario;
  public anejos: datosAnejos;
  public datosViviendas: datosVivienda;
  public datosViendaArray: datosVivienda [] = [];
  public estadocivil = new Map([['X', 'CASADO'], ['S', 'SOLTERO'], ['D', 'DIVORCIADO'], ['V', 'VIUDO / PAREJA DE HECHO']]);
  public tipovia = new Map([['AD', 'ALDEA'],['AL', 'ALAMEDA'],['AP', 'APARTAMENTO'],['AV', 'AVENIDA'],['BL', 'BLOQUE'],['BO', 'BARRIO'],['CH', 'CHALET'],['CL', 'CALLE'],['CM', 'CAMINO'], ['CO', 'COLONIA'],['CR', 'CARRETERA'],['CS', 'CASERIO'],['CT', 'CUESTA'],['ED', 'EDIFICIO'],['GL', 'GLORIETA'],['GR', 'GRUPO'],['LG', 'LUGAR'],['MC', 'MERCADO'],['MN', 'MUNICIPIO'],['MZ', 'MANZANA'],['PB', 'POBLADO'],['PG', 'POLIGONO'],['PJ', 'PASAJE'],['PQ', 'PARQUE'],['PR', 'PROLONGACION'],['PS', 'PASEO'],['PZ', 'PLAZA'],['RB', 'RAMBLA'],['RD', 'RONDA'],['TR', 'TRAVESIA'],['UR', 'URBANIZACION']]);
  public p: number = 1;



  constructor(private router: Router, public wsService: WebsocketService, public authenticationService: AuthenticationService,private route: ActivatedRoute) { }
  public usuarioAdmin;
  ngOnInit() {

    const credentials = this.authenticationService.credentials;

    const nombreUsuario = credentials.username;
    console.log('El nombre de usuario es: ' + nombreUsuario);
    console.log('vamos a mirar el perfil');


    this.wsService.getUserApp(nombreUsuario)
  .pipe(
    finalize(() => {
    })
  )
  .subscribe(
    result => {
      let valor = JSON.stringify(result);
      let valor2 = JSON.parse(valor);
      console.log('Vamos a la obtención del perfil' +valor2["result"] );
      this.usuarioAdmin = valor2["result"];
      console.log('El resultado es: '+this.usuarioAdmin);
      if(this.usuarioAdmin.toLowerCase() == 'admin')
      {
        this.viviendas.push('Elija una opción');

        let promocion = new datosPromociones();
        promocion.PMNOMP = ' ';
        promocion.PMPROM = 'Elija una opción';
        this.promociones.push(promocion);
    
        this.getPromotions();
    
    
          ($('#selectanejos') as any).select2();
          $('#checkbox').click(function(){
            if($('#checkbox').is(':checked') ) {
                $('#selectanejos > option').prop('selected','selected');
                $('#selectanejos').trigger('change');
            }else{
                 $('#selectanejos').val(null).trigger('change');
             }
        });
    
      }
      if(this.usuarioAdmin.toLowerCase() == 'user')
      {
        this.viviendas.push('Elija una opción');

        let promocion = new datosPromociones();
        promocion.PMNOMP = ' ';
        promocion.PMPROM = 'Elija una opción';
        this.promociones.push(promocion);
    
        this.getPromotions();
    
    
          ($('#selectanejos') as any).select2();
          $('#checkbox').click(function(){
            if($('#checkbox').is(':checked') ) {
                $('#selectanejos > option').prop('selected','selected');
                $('#selectanejos').trigger('change');
            }else{
                 $('#selectanejos').val(null).trigger('change');
             }
        });
    
      }
      if(this.usuarioAdmin.toLowerCase()== 'notario' || this.usuarioAdmin.toLowerCase()== 'portales' || this.usuarioAdmin.toLowerCase()== 'marketing')
      {
        this.router.navigateByUrl('/login');
      }

    },
    error => {
      console.log('Error GORDO');
    }
  );




  }

  public getPromotions(): void {

    this.wsService.getPromocionName()
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      result => {
        console.log(result.length);
        for(let i = 0; i < result.length; i++) {
          let promocionName = new datosPromociones();
          promocionName.PMNOMP = result[i]['PMPROM'];
          promocionName.PMPROM = result[i]['PMNOMP'];
          this.promociones.push(promocionName);
        }

      },
      error => {
        console.log('Error');
      }
    );

  }

  public getViviendas(): void {

    this.PromocionesID = (<HTMLSelectElement>document.getElementById('selectpromociones')).value;
    this.viviendas = [];

    if(this.PromocionesID === 'Elija una opción'){
      this.viviendas.push('Elija una opción');
    }
    else
    {
      this.wsService.getViviendas(this.PromocionesID)
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      result => {
        console.log(result);
        console.log(result.length);
        this.viviendas.push('Elija una opción');

        for(let i = 0; i < result.length; i++) {
          this.viviendas.push(result[i]['UHEDIT']);
        }

      },
      error => {
        console.log('Error');
      }
    );
    }
  }

  public getViviendaSeleccionada(): void {
    this.Viviendasanejos = [];
    let viviendaSeleccionada = <HTMLSelectElement>document.getElementById('selectpromociones');
    let generarPdf = <HTMLElement>document.getElementById('GenerarPdf');


    console.log(viviendaSeleccionada);
    this.arrayViviendas = [];
    let anejosArray: string [] = ['UHPROM','UHTIEX','UHPOEX','UHPLEX','UHLEEX','UHDAEX'];

    let selectViviendasLibres = AppSettings.SELECT_VIVIENDASLIBRES(this.PromocionesID);
    this.wsService.getViviendasLibres(selectViviendasLibres)
              .pipe(
                finalize(() => {
                })
              )
              .subscribe(
                result => {
                  console.log(result);
                  if(result.length === 0)
                  {
                    alert('No existen viviendas libres para esa promoción');
                  }
                   for (let i=0; i<result.length; i++)
                   {
                      
                      let vivienda = result[i]['UHEDIT'];
                      this.arrayViviendas.push(vivienda);
                   }

                   for(let j = 0 ; j < this.arrayViviendas.length; j++)
                   {
             
                     let viviendapartes: string [] = [];
                     viviendapartes = (this.arrayViviendas[j]).split('-');
                     const partes = viviendapartes.length;
                     console.log('la vivienda ' + this.arrayViviendas[j] + ' tiene ' + partes);
                     const viviendaconAnejo = new Promociones();
                     let select;
             
                     select = AppSettings.SELECT_PROMOCIONES(this.PromocionesID);
             
                     for(let k = 1; k <= partes; k++)
                     {
                       select = select + "AND " + anejosArray[k] + "='" +viviendapartes[k-1].trim()+"'";
                     }
                     console.log('La select es: '+ select);
             
                     let selectVivienda = AppSettings.SELECT_VIVIENDA(this.PromocionesID, this.arrayViviendas[j]);
                     this.wsService.getDatosViviendas(selectVivienda)
                     .pipe(
                       finalize(() => {
                       })
                     )
                     .subscribe(
                       result => {
             
                       viviendaconAnejo.DATOSVIVIENDAS =  [];
                       for(let i = 0; i < result.length; i++) {
                         this.datosViviendas = new datosVivienda();
                         this.datosViviendas.NOMBRE_VIVIENDA = result[i]['UHALIA'];
                         this.datosViviendas.PRECIO_VIVIENDA = result[i]['UHNETO'];
                         if(result[i]['UHMTUT'] !== null && result[i]['UHMTUT'] !== '')
                         {
                           this.datosViviendas.METROS_HABILES = result[i]['UHMTUT'].toFixed(2);
                         }
                         else
                         {
                           this.datosViviendas.METROS_HABILES = result[i]['UHMTUT'];
                         }
             
                         if(result[i]['UHMTCO'] !== null && result[i]['UHMTCO'] !== '')
                         {
                           this.datosViviendas.METROS_CONSTRUIDOS = result[i]['UHMTCO'].toFixed(2);
                         }
                         else
                         {
                           this.datosViviendas.METROS_CONSTRUIDOS = result[i]['UHMTCO'];
                         }
             
             
                         this.datosViviendas.ESTADO_VIVIENDA = result[i]['UHDEES'];
                         viviendaconAnejo.DATOSVIVIENDAS.push(this.datosViviendas);
                       }
                       this.wsService.getAnejos(select)
                     .pipe(
                       finalize(() => {
                       })
                     )
                     .subscribe(
                       result => {
                         console.log(result);
                         console.log(result.length);
             
                         viviendaconAnejo.UHPROM = this.arrayViviendas[j];
                         viviendaconAnejo.DATOSANEJOS =  [];
                         for(let i = 0; i < result.length; i++) {
                           this.anejos = new datosAnejos();
                           this.anejos.NOMBRE_ANEJO = result[i]['UHNOCO'];
                           if(result[i]['UHMTCO'] !== null && result[i]['UHMTCO'] !== '')
                           {
                             this.anejos.METROS_ANEJO = result[i]['UHMTCO'].toFixed(2);
                           }
                           else
                           {
                             this.anejos.METROS_ANEJO = result[i]['UHMTCO'];
                           }
                           this.anejos.PRECIO_ANEJO = result[i]['UHNETO'];
             
                           viviendaconAnejo.DATOSANEJOS.push(this.anejos);
                         }
             
                         
                         this.Viviendasanejos.push(viviendaconAnejo);
                       },
                       error => {
                         console.log('Error obtención de Anejos');
                       }
                     );
             
             
                     },
                     error => {
                       console.log('Error obtención de los Datos de las viviendas');
                     }
             
                     );
             
             
                   }

                },
                error => {
                  console.log('Error Obtención de Viviendas Libres');
                }
              );


  }

  public setAllElements(): void {

  const selectBox = <HTMLSelectElement>document.getElementById('selectanejos');
  for (let aux_count = 0; aux_count < selectBox.options.length; aux_count++) {

    selectBox.options[aux_count].selected = true;
    selectBox.options[0].selected = false;
  }


  }


  public connectSalesForce(codigoprinex: string): void {

    const codigopromocion = (<HTMLInputElement>document.getElementById('codigoPromocion' + codigoprinex)).value;
    const codigovivienda = (<HTMLInputElement>document.getElementById('codigoVivienda' + codigoprinex)).value;
    const productcode = codigopromocion+'-'+codigovivienda;


    this.wsService.connectSaleForce(codigoprinex).subscribe(
        data => {
          // const json = JSON.stringify({data});
          // alert(data['result']);
          console.log('el data es ' +data['result'].length);
          if(data['result'].length === 0){
            alert('El usuario con código prinex '+codigoprinex+' no existe en SalesForce, revíselo');
          }
          else
          {
            data['result'].forEach(element => {
              localStorage.setItem('userSalesForce', element['name']);
              localStorage.setItem('emailuserSalesForce', element['email']);
              localStorage.setItem('areaprivadaSalesForce', element['fechaareaprivada']);
              localStorage.setItem('IdSalesForce', element['Id']);
              localStorage.setItem('productCode', productcode);
              this.route.queryParams.subscribe(params => {
                this.router.navigate(['/salesforce'], { replaceUrl: true });
              });
  
            });
          }        
        },
        error => {
          console.log(error, { depth: null});
        }
      );
  }

  public registrarCambios(campoPrinex : string, idPrinex : string, valornuevo : string, valorantiguo : string, user :string, aplicacion : string): void {

  this.wsService.addCambioBBDD(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion)
      .pipe(
      finalize(() => {
      })
      )
      .subscribe(
      result => {
       console.log(campoPrinex +' modificado correctamente');
       },
          error => {
             console.log('Error');
             console.log('Se ha producido un error al subir un documento en BBDD');
            }
       );


  }

  public download(promotion: string) {

  const viviendaconAnejo = new Promociones();
  this.Viviendaanejosdownload = [];
  let anejosArray: string [] = ['UHPROM','UHTIEX','UHPOEX','UHPLEX','UHLEEX','UHDAEX'];
  let width = 160;

  var doc = new jsPDF();
  var img = new Image();
  img.src = 'assets/secciones/LOGOaedas.png';
  doc.addImage(img, 'png', 5, 5, 35, 20);
  doc.setFont('helvetica');
  doc.setFontType('bold');
  doc.setFontSize(18);
  doc.text(50, 20, 'INFORMACIÓN DE VIVIENDA Y ANEJOS');
  doc.setFontSize(10);

  let selectNombrePromo = AppSettings.SELECT_NOMBREPROMOS(this.PromocionesID);

  this.wsService.getDatosViviendas(selectNombrePromo)
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      result => {
        console.log(result);
        console.log(result.length);
        for(let j = 0; j < result.length; j++)
        {
          doc.text(10, 30, 'Promoción: ' + result[j]['PMNOMP']);
        }
        let selectVivienda = AppSettings.SELECT_VIVIENDA(this.PromocionesID, promotion);
        console.log('LA SELECT DE LA VIVIENDA ES: '+selectVivienda);

        this.wsService.getDatosViviendas(selectVivienda)
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(
          result => {
            console.log(result);
            console.log(result.length);
            doc.setFontSize(12);
            for(let j = 0; j < result.length; j++)
            {
              let tipovia = '';
              let nombrevia = '';
              let numerovia = '';
              let poblacion = '';
              if (result[j]['UHSGVP'] !== null && result[j]['UHSGVP'] !== '')
              {
                tipovia = result[j]['UHSGVP'];
              }
              if (result[j]['UHNOVP'] !== null && result[j]['UHNOVP'] !== '')
              {
                nombrevia = result[j]['UHNOVP'];
              }
              if (result[j]['UHNUME'] !== null && result[j]['UHNUME'] !== '')
              {
                numerovia = result[j]['UHNUME'];
              }
              if (result[j]['UHPOBLACION'] !== null && result[j]['UHPOBLACION'] !== '')
              {
                poblacion = result[j]['UHPOBLACION'];
              }

              doc.text(10, 40, '* DIRECCIÓN: ' + tipovia + ' ' + nombrevia + ' ' + numerovia + ' ' + poblacion);
              doc.text(10, 50, '* VIVIENDA: ' + result[j]['UHALIA']);
              doc.setFontSize(14);
              doc.setFontType('italic');
              doc.text(60, 70, 'DATOS DE LA VIVIENDA');
              doc.setFontSize(12);
              doc.setFontType('normal');
              doc.text(10, 100, '* Metros Hábiles: ' + result[j]['UHMTUT'].toFixed(2) + ' m2');
              doc.text(10, 110, '* Metros Construidos: ' + result[j]['UHMTCO'].toFixed(2) + ' m2');
              doc.text(10, 120, '* PVP: ' + result[j]['UHNETO'] + ' €');


            }

          },
          error => {
            console.log('Error Obtención los datos de la vivienda');
          }
        );

        let viviendapartes: string [] = [];
        viviendapartes = (promotion.split('-'));
        const partes = viviendapartes.length;
        console.log('la vivienda ' + promotion + ' tiene ' + partes);
        const viviendaconAnejo = new Promociones();


        //select = "SELECT * FROM FUNHIP WHERE UHPROM ='" + this.PromocionesID + "'";
        let select = AppSettings.SELECT_PROMOCIONES(this.PromocionesID);

        for(let k = 1; k <= partes; k++)
        {
          select = select + "AND " + anejosArray[k] + "='" +viviendapartes[k-1].trim()+"'";
        }
        console.log('La select es: '+ select);

        this.wsService.getDatosViviendas(select)
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(
          result => {
            console.log(result);
            console.log(result.length);
            doc.text(10, 150, '* ANEJOS');

            if(result.length === 0)
            {
              doc.text(10, width, '* Esta vivienda no tiene anejos');
              width = width + 10;
            }
            else
            {
              for(let j = 0; j < result.length; j++)
              {
                doc.text(10, width, '* '+result[j]['UHNOCO']+' ---> PVP. '+result[j]['UHNETO']+'€');
                width = width + 10;
              }
            }

              let valida = promotion.replace(/ /g, '');
              doc.save(valida+'.pdf');
          },
          error => {
            console.log('Error Obtención los datos de la vivienda');
          }
        );

      },
      error => {
        console.log('Error Obtención de la Dirección');
      }
    );
  }

}

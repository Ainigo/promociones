import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Categoria} from '../../classes/categoria';
import { ProductosService } from 'src/app/services/productos.service';
import { WebsocketService } from '../../services/websocket.service';
import { finalize } from 'rxjs/operators';
import { datosPropietario} from '../../classes/datosPropietario';
import { AuthenticationService } from '../../core';
import * as $ from 'jquery';
import { datosAnejos } from 'src/app/classes/datosAnejos';
import { datosVivienda } from 'src/app/classes/datosVivienda';
import { AppSettings } from '../../shared/app-settings';
import { datosViviendaSalesForce } from 'src/app/classes/datosViviendaSalesForce';
import { datosUsuarioSalesForce } from 'src/app/classes/datosUsuarioSalesForce';
import { datosUsuarioPrinex } from 'src/app/classes/datosUsuarioPrinex';
import { DatosVariasPromociones } from 'src/app/classes/datosVariasPromociones';
import { Oportunidades } from 'src/app/classes/oportunidades';
import { Encuestas } from 'src/app/classes/encuestas';

declare var html2canvas: any;
declare var jsPDF: any;
declare var req: any;

@Component({
  selector: 'app-encuestas',
  templateUrl: './encuestas.component.html',
  styleUrls: ['./encuestas.component.css']
})

export class EncuestasComponent implements OnInit {
  public promociones: string [] = [];
  public viviendas: string [] = [];
  public PromocionesID: string;
  public nombreUsuario: string;
  public arrayViviendas: string [] = [];
  public cambios: string [] = [];
  public usuario: datosPropietario;
  public anejos: datosAnejos;
  public datosViviendas: datosVivienda;
  public datosViendaArray: datosVivienda [] = [];
  public activadoAreaPrivada = '';
  public Viviendassalesforce: datosViviendaSalesForce [] = [];
  public arrayInmueblesIDS: DatosVariasPromociones [] = [];
  public DatosUsuariosalesforce: datosUsuarioSalesForce [] = [];
  public DatosUsuariosalesforce2: datosUsuarioSalesForce [] = [];
  public DatosUsuarioprinex: datosUsuarioPrinex [] = [];
  public DatosUsuarioprinex2: datosUsuarioPrinex [] = [];
  public DatoViviendaPrinex: datosVivienda [] = [];
  public IdInmueble = '';
  public estadoComercialSalesForce = '';
  public estadoViviendaEscriturada = '';
  public Inmuebles: string [] = [];
  public IdUserDNI: string = '';
  public idOportunidadSalesForce : string = '';
  public stageNameSalesForce : string = '';
  public Oportunidades: Oportunidades [] = [];
  // public userLogedId: string;

  constructor(private router: Router, public wsService: WebsocketService, public authenticationService: AuthenticationService, private route: ActivatedRoute) { }
  public usuarioAdmin;
  ngOnInit() {
    console.log('Entra en onInit Encuestas');
    // document.getElementById('VariasPromociones').style.display = 'none';
    document.getElementById('titleOportunidades').style.display = 'none';
    document.getElementById('cabeceraOportunidades').style.display = 'none';

    const credentials = this.authenticationService.credentials;
    const salesForceUser = this.authenticationService.SalesForce;
    this.nombreUsuario = credentials.username;
    // this.userLogedId = salesForceUser.userSalesForce;
    this.wsService.getpintarTrazasLogs(this.nombreUsuario, 'El nombre de usuario es: ' + this.nombreUsuario);
    this.wsService.getpintarTrazasLogs(this.nombreUsuario, 'vamos a mirar el perfil');
    document.getElementById('VariasPromociones').style.display = 'none';

    this.wsService.getUserApp(this.nombreUsuario)
      .pipe(
        finalize(() => {
        })
      )
      .subscribe(
        result => {
          let valor = JSON.stringify(result);
          let valor2 = JSON.parse(valor);
          this.wsService.getpintarTrazasLogs(this.nombreUsuario,'Vamos a la obtención del perfil' +valor2["result"] );
          this.usuarioAdmin = valor2["result"];
          this.wsService.getpintarTrazasLogs(this.nombreUsuario,'El resultado es: '+this.usuarioAdmin);
          if(this.usuarioAdmin.toLowerCase() != 'admin'
            && this.usuarioAdmin.toLowerCase() != 'notario') {
              this.router.navigateByUrl('/login');
          }
        },
        error => {
          this.wsService.getpintarTrazasLogs(this.nombreUsuario,'Error');
        }
      );
  }

  volver() {
    this.route.queryParams.subscribe(params => {
      this.router.navigate(['/promociones'], { replaceUrl: true });
    });
  }

  GetViviendasuser() {
    this.IdInmueble = '';
    this.Inmuebles = [];
    this.estadoViviendaEscriturada = "Verdadero";
    document.getElementById('titleOportunidades').style.display = 'none';
    document.getElementById('cabeceraOportunidades').style.display = 'none';
    let dni = (<HTMLInputElement>document.getElementById('dniUserSalesForce')).value;
    let disponibilidad = document.getElementById('disponiblePrinex') as HTMLInputElement;
    let idUser;
    this.IdUserDNI = "";
    this.DatosUsuariosalesforce = [];
    this.DatosUsuariosalesforce2 = [];
    this.DatosUsuarioprinex = [];
    this.DatosUsuarioprinex2 = [];
    this.Viviendassalesforce = [];
    this.arrayInmueblesIDS = [];
    this.DatoViviendaPrinex = [];

    if(dni != "")
    {
      this.wsService.getIdUserSalesForce(dni).subscribe(
        data => {
          if(data['result'].length === 0){
            disponibilidad.style.display = 'none';
            this.estadoViviendaEscriturada = "Verdadero";
            document.getElementById('datosInmuebles').style.display = 'none';
            alert('El usuario con el DNI '+dni+' no existe en SalesForce, revíselo');
          }
          else
          {
            data['result'].forEach(element => {
              idUser = element['Id'];
              this.IdUserDNI = idUser;
              let UserSalesForce = new datosUsuarioSalesForce();
              UserSalesForce.NAME_USER = element['name'];
              UserSalesForce.EMAIL_USER = element['email'];
              UserSalesForce.TELEFONO1 = element['telefono1'];
              UserSalesForce.TELEFONO2 = element['telefono2'];
              if(element['fechaareaprivada'] === null)
              {
                UserSalesForce.FECHA_AREAPRIVADA = 'Falso';
                this.activadoAreaPrivada = 'Falso';
              }
              else
              {
                UserSalesForce.FECHA_AREAPRIVADA = 'Verdadero';
                this.activadoAreaPrivada = 'Verdadero';
              }
              UserSalesForce.ID_USER = element['Id'];
              this.DatosUsuariosalesforce.push(UserSalesForce);
            });

            let selectDNI = AppSettings.SELECT_DNI(dni);
            this.wsService.getDNIPrinex(selectDNI)
              .pipe(
                finalize(() => {
                })
              )
              .subscribe(
                result => {
                  this.wsService.getpintarTrazasLogs(this.nombreUsuario,result);
                  this.wsService.getpintarTrazasLogs(this.nombreUsuario,result.length);

                  if(result.length === 0)
                  {
                    document.getElementById('disponibilidadPrinex').innerHTML = 'El usuario con este DNI no existe en PRINEX';
                    disponibilidad.style.display = 'contents';
                    //dnidisponibilidad.value = 'El usuario con este DNI no existe en PRINEX';
                  }
                  else
                  {
                    document.getElementById('disponibilidadPrinex').innerHTML = 'El usuario con este DNI está también en PRINEX';
                    disponibilidad.style.display = 'contents';
                    //dnidisponibilidad.value = 'El usuario con este DNI está también en PRINEX';
                    let UserPrinex = new datosUsuarioPrinex();
                    UserPrinex.NAME_USER = result[0]['CONOMP'] + result[0]['COAPEP'];
                    UserPrinex.TELEFONO1 = result[0]['COTLFP'];
                    UserPrinex.EMAIL = result[0]['COMAIL'];
                    if(result[0]['COMOVIL'] !== null)
                    {
                      UserPrinex.TELEFONO2 = result[0]['COMOVIL'];
                    }
                    else
                    {
                      UserPrinex.TELEFONO2 = ' '
                    }
                    UserPrinex.COD_PRINEX = result[0]['COCODI'];
                    this.DatosUsuarioprinex.push(UserPrinex);
                  }
                },
                error => {
                  this.wsService.getpintarTrazasLogs(this.nombreUsuario,'Error Obtención del DNI primario');
                }
              );
          }
        },
        error => {
          //console.log(error, { depth: null});
          this.wsService.getpintarTrazasLogs(this.nombreUsuario,error);
        }
      );

      this.GetUserOportunities();
    }
  }

  GetUserOportunities() {
    console.log('Inio GetUserOportunities');
    let dni = (<HTMLInputElement>document.getElementById('dniUserSalesForce')).value;
    let idUser;
    let idiomaUser;
    this.Oportunidades = [];

    this.wsService.getIdUserSalesForce(dni).subscribe(
      data => {
        if(data['result'].length === 0){
          // document.getElementById('VariasPromociones').style.display = 'none';
          this.estadoViviendaEscriturada = "Verdadero";
          document.getElementById('datosInmuebles').style.display = 'none';
          alert('El usuario con el DNI '+dni+' no existe en SalesForce, revíselo');
        }
        else
        {
          data['result'].forEach(element => {
            idUser = element['Id'];
            idiomaUser = element['Idioma'];
            console.log('IdUser: ' + idUser);
            this.wsService.getListaOportunidadesByUser(idUser).subscribe(
              data => {
                if (data['result'].length === 0) {
                  console.log('Oculto VariasPromociones');
                  // document.getElementById('VariasPromociones').style.display = 'none';
                }
                else {
                  document.getElementById('titleOportunidades').style.display = 'table-row';
                  document.getElementById('cabeceraOportunidades').style.display = 'table-row';
                  document.getElementById('VariasPromociones').style.display = 'block';
                  data['result'].forEach(element => {
                    console.log(element);
                    let oportunidad = new Oportunidades();
                    oportunidad.ID = element['id'];
                    oportunidad.NAME = element['name'];
                    oportunidad.PROMOCION = element['promocionName'];
                    oportunidad.STAGE = element['StageName'];
                    oportunidad.ENCUESTAS = [];

                    let oportunidadId = element['id'];

                    this.wsService.getListaEncuestasOportunidad(idUser, oportunidad.ID).subscribe(
                      data => {
                        console.log(data);
                        if (data['result'].length === 0) {
                          oportunidad.ENCUESTAS = this.GetEncuestasNorellenas(idiomaUser, idUser, oportunidadId);
                        } else {
                          data['result'].forEach(element => {
                            console.log(element);
                            let encuesta = new Encuestas();
                            encuesta.ID = element['id'];
                            encuesta.ID_URL = element['idUrl'];
                            encuesta.NAME = element['name'];
                            encuesta.URL = this.GetURLEncuesta(element['idUrl'], idUser, oportunidadId);
                            encuesta.CREATED_DATE = element['createdDate'];
                            encuesta.FILL_DATE = element['completionDate'];
                            if(element['filled'] == true)
                              encuesta.FILLED = 'Rellenada';
                            else
                            encuesta.FILLED = 'Sin rellenar';

                            oportunidad.ENCUESTAS.push(encuesta);
                          });

                          if (oportunidad.ENCUESTAS.length != 2) {
                            let encuestasAux = this.GetEncuestasNorellenas(idiomaUser, idUser, oportunidadId, oportunidad.ENCUESTAS[0].ID_URL);
                            encuestasAux.forEach(element => {
                              oportunidad.ENCUESTAS.push(element);
                            });
                          }

                          oportunidad.ENCUESTAS.sort(function (a, b) {
                            if (a.NAME > b.NAME) {
                              return 1;
                            }
                            if (a.NAME < b.NAME) {
                              return -1;
                            }
                            // a must be equal to b
                            return 0;
                          });
                        }
                      }
                    );

                    this.Oportunidades.push(oportunidad);
                  });
                }
              }
            );
          });
        }
      }
    );
  }

  GetEncuestasNorellenas(idioma: string, idUser: string, oportunidadId: string, encuestaId: string = "") {
    let encuestas: Encuestas[] = [];
    if(idioma == 'Español') {
      if(encuestaId != '695203') {
        let encuesta = new Encuestas();
        encuesta.ID = '695203';
        encuesta.NAME = '4. Visita de Cortesía';
        encuesta.URL = this.GetURLEncuesta(encuesta.ID, idUser, oportunidadId);
        encuesta.FILLED = 'Sin rellenar';

        encuestas.push(encuesta);
      }

      if (encuestaId != '695205') {
        let encuesta2 = new Encuestas();
        encuesta2.ID = '695205';
        encuesta2.NAME = '5. Proceso de escrituración';
        encuesta2.URL = this.GetURLEncuesta(encuesta2.ID, idUser, oportunidadId);
        encuesta2.FILLED = 'Sin rellenar';

        encuestas.push(encuesta2);
      }
    }
    else {
      if (encuestaId != '711087') {
        let encuesta = new Encuestas();
        encuesta.ID = '711087';
        encuesta.NAME = '4. Courtesy visit';
        encuesta.URL = this.GetURLEncuesta(encuesta.ID, idUser, oportunidadId);
        encuesta.FILLED = 'Sin rellenar';

        encuestas.push(encuesta);
      }

      if (encuestaId != '711089') {
        let encuesta2 = new Encuestas();
        encuesta2.ID = '711089';
        encuesta2.NAME = '5. Deed process';
        encuesta2.URL = this.GetURLEncuesta(encuesta2.ID, idUser, oportunidadId);
        encuesta2.FILLED = 'Sin rellenar';

        encuestas.push(encuesta2);
      }
    }

    return encuestas;
  }

  GetURLEncuesta(encuestaId: string, accountId: string, opportunityId: string) {
    let url = '';

    if(encuestaId == '695203'){
      url = 'https://www.getfeedback.com/r/4EIqKC8b?IdAccount=' + accountId + '&Opportunity=' + opportunityId + '&UTM_SOURCE=' + this.nombreUsuario;
    }
    if(encuestaId == '711087'){
      url = 'https://www.getfeedback.com/r/Kd6LZZZs?IdAccount=' + accountId + '&Opportunity=' + opportunityId + '&UTM_SOURCE=' + this.nombreUsuario;
    }
    if(encuestaId == '695205'){
      url = 'https://www.getfeedback.com/r/JCWFUyXP?IdAccount=' + accountId + '&Opportunity=' + opportunityId + '&UTM_SOURCE=' + this.nombreUsuario;
    }
    if(encuestaId == '711089'){
      url = 'https://www.getfeedback.com/r/lNvU6L7J?IdAccount=' + accountId + '&Opportunity=' + opportunityId + '&UTM_SOURCE=' + this.nombreUsuario;
    }

    return url;
  }
}

import { Component, OnInit } from '@angular/core';
import { WebsocketService } from '../../services/websocket.service';
import { finalize } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService, Logger } from '../../core';
import { authParameters } from '../../../environments/environment';

const log = new Logger('Login');

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public nombre = '';
  public pass = '';
  public nombreUsuario: string;

  private templateAuthzUrl: string = authParameters.authorityHostUrl +
  authParameters.tenant +
  '/oauth2/authorize?response_type=code&' +
  'client_id=<client_id>&' +
  'redirect_uri=<redirect_uri>&' +
  'state=<state>&resource=<resource>';

  constructor(
    public wsService: WebsocketService,
    private router: Router,
    private route: ActivatedRoute,
    // private location: Location,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    console.log('LoginComponent starts.');
    const code = this.route.queryParams['value'].code;
    console.log('El code new2 es: ' + code);

    if (code) {
      this.wsService.getpintarTrazasLogs(this.nombreUsuario, 'El code segundo paso');
      this.authenticationService.getAuthToken(code).subscribe( result => {
        this.wsService.getpintarTrazasLogs(this.nombreUsuario, 'paso el getAuthToken');
        this.wsService.getpintarTrazasLogs(this.nombreUsuario, result);
        console.log('El result es: ' + result);
        this.completaLogin(result, code);
      },
      error => {
        document.getElementById('msg-error').style.display = 'block';
        this.wsService.getpintarTrazasLogs(this.nombreUsuario, 'Error');
      }
      );
    }
  }

  createAuthorizationUrl(state: string) {
    let authorizationUrl = this.templateAuthzUrl.replace('<client_id>', authParameters.clientId);
    authorizationUrl = authorizationUrl.replace('<redirect_uri>', authParameters.redirectUri);
    authorizationUrl = authorizationUrl.replace('<state>', state);
    authorizationUrl = authorizationUrl.replace('<resource>', authParameters.resource);
    return authorizationUrl;
  }

  ingresar() {
    window.location.href = this.createAuthorizationUrl('123456');
  }

  completaLogin(result: any, code: string) {
    const data = {
      // username: this.loginForm.value.username,
      username: result.username,
      token: result.token,
      role_name: result.role_name
    };
    this.authenticationService.setCredentials(result);
    localStorage.setItem('name', data.username);
    localStorage.setItem('role', data.role_name);
    localStorage.setItem('token', data.token);
    this.route.queryParams.subscribe(params => {

      this.wsService.getUserApp(data.username)
      .pipe(
        finalize(() => {
        })
      )
      .subscribe(
        resultado => {
          const valor = JSON.stringify(resultado);
          const valor2 = JSON.parse(valor);
          this.wsService.getpintarTrazasLogs(data.username, 'Vamos a la obtención del perfil' + valor2['result'] );
          const usuarioUserApp = valor2['result'];
          this.wsService.getpintarTrazasLogs(data.username, 'El resultado es: ' + usuarioUserApp);
          const url: string = localStorage.getItem('urlActual');
          console.log('url_localStorage: ' + url);
          // alert(window.location.origin + '/login?code='+code)
          // console.log(window.location.origin + '/login?code='+code);
          // if(url == window.location.origin + '/login?code='+code)
          // {
          //   alert('Hola mundo')
          // }
          // else
          // {
          //   alert('adios mundo');
          // }
          if (url != null && url !== window.location.origin + '/login'
            && url !== window.location.origin + '/' && url.indexOf('/login?code=') === -1) {
              window.location.href = url;
          } else {
            if (usuarioUserApp.toLowerCase() === 'admin') {
              this.router.navigate(['/promociones'], { replaceUrl: true });
            }
            if (usuarioUserApp.toLowerCase() === 'user') {
              this.router.navigate(['/promociones'], { replaceUrl: true });
            }
            if (usuarioUserApp.toLowerCase() === 'notario') {
              this.router.navigate(['/firmanotaria'], { replaceUrl: true });
            }
            if (usuarioUserApp.toLowerCase() === 'marketing') {
              this.router.navigate(['/mktcloud'], { replaceUrl: true });
            }
          }
        },
        error => {
          this.wsService.getpintarTrazasLogs(data.username, 'Error');
        }
      );
    });
  }
}

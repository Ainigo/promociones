import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirmanotariaComponent } from './firmanotaria.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('FirmanotariaComponent', () => {
  let component: FirmanotariaComponent;
  let fixture: ComponentFixture<FirmanotariaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgbModule],
      declarations: [ FirmanotariaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirmanotariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

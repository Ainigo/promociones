import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Categoria} from '../../classes/categoria';
import { ProductosService } from 'src/app/services/productos.service';
import { WebsocketService } from '../../services/websocket.service';
import { finalize } from 'rxjs/operators';
import { datosPropietario} from '../../classes/datosPropietario';
import { AuthenticationService } from '../../core';
import * as $ from 'jquery';
import { datosAnejos } from 'src/app/classes/datosAnejos';
import { datosVivienda } from 'src/app/classes/datosVivienda';
import { AppSettings } from '../../shared/app-settings';
import { datosViviendaSalesForce } from 'src/app/classes/datosViviendaSalesForce';
import { datosUsuarioSalesForce } from 'src/app/classes/datosUsuarioSalesForce';
import { datosUsuarioPrinex } from 'src/app/classes/datosUsuarioPrinex';
import { DatosVariasPromociones } from 'src/app/classes/datosVariasPromociones';

declare var html2canvas: any;
declare var jsPDF: any;
declare var req: any;


@Component({
  selector: 'app-firmanotaria',
  templateUrl: './firmanotaria.component.html',
  styleUrls: ['./firmanotaria.component.css']
})



export class FirmanotariaComponent implements OnInit {

  public promociones: string [] = [];
  public viviendas: string [] = [];
  public PromocionesID: string;
  public nombreUsuario: string;
  public arrayViviendas: string [] = [];
  public cambios: string [] = [];
  public usuario: datosPropietario;
  public anejos: datosAnejos;
  public datosViviendas: datosVivienda;
  public datosViendaArray: datosVivienda [] = [];
  public activadoAreaPrivada = '';
  public Viviendassalesforce: datosViviendaSalesForce [] = [];
  public arrayInmueblesIDS: DatosVariasPromociones [] = [];
  public DatosUsuariosalesforce: datosUsuarioSalesForce [] = [];
  public DatosUsuariosalesforce2: datosUsuarioSalesForce [] = [];
  public DatosUsuarioprinex: datosUsuarioPrinex [] = [];
  public DatosUsuarioprinex2: datosUsuarioPrinex [] = [];
  public DatoViviendaPrinex: datosVivienda [] = [];
  public IdInmueble = '';
  public estadoComercialSalesForce = '';
  public estadoViviendaEscriturada = '';
  public Inmuebles: string [] = [];
  public IdUserDNI: string = '';

  public idOportunidadSalesForce : string = '';
  public stageNameSalesForce : string = '';



  constructor(private router: Router, public wsService: WebsocketService, public authenticationService: AuthenticationService, private route: ActivatedRoute) { }
  public usuarioAdmin;
  ngOnInit() {
    
    const credentials = this.authenticationService.credentials;

    this.nombreUsuario = credentials.username;
    this.wsService.getpintarTrazasLogs(this.nombreUsuario,'El nombre de usuario es: ' + this.nombreUsuario)
    this.wsService.getpintarTrazasLogs(this.nombreUsuario,'vamos a mirar el perfil');
    document.getElementById('VariasPromociones').style.display = 'none';


    this.wsService.getUserApp(this.nombreUsuario)
  .pipe(
    finalize(() => {
    })
  )
  .subscribe(
    result => {
      let valor = JSON.stringify(result);
      let valor2 = JSON.parse(valor);
      this.wsService.getpintarTrazasLogs(this.nombreUsuario,'Vamos a la obtención del perfil' +valor2["result"] );
      this.usuarioAdmin = valor2["result"];
      this.wsService.getpintarTrazasLogs(this.nombreUsuario,'El resultado es: '+this.usuarioAdmin);
      if(this.usuarioAdmin.toLowerCase() == 'admin')
      {
    
      }
      if(this.usuarioAdmin.toLowerCase() == 'user')
      {
        
      }
      if(this.usuarioAdmin.toLowerCase() == 'notario')
      {
        
      }
      if(this.usuarioAdmin.toLowerCase() == 'portales' || this.usuarioAdmin.toLowerCase() == 'marketing')
      {
        

        this.router.navigateByUrl('/login');
      }

    },
    error => {
      this.wsService.getpintarTrazasLogs(this.nombreUsuario,'Error');
    }
  );
  }

  volver() {
    this.route.queryParams.subscribe(params => {
      this.router.navigate(['/promociones'], { replaceUrl: true });
    });
  }

  GetViviendasuser() {
    this.IdInmueble = '';
    this.Inmuebles = [];
    document.getElementById('VariasPromociones').style.display = 'none';
    let dni = (<HTMLInputElement>document.getElementById('dniUserSalesForce')).value;
    this.estadoViviendaEscriturada = "Verdadero";

    let disponibilidad = document.getElementById('disponiblePrinex') as HTMLInputElement;
    let datosViviendacabecera = document.getElementById('datosVivienda') as HTMLInputElement;
    let codigoSalesForceUser = "";
    let fechaactual;
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    fechaactual = yyyy + '-' + mm + '-' + dd;



    let idUser;
    this.IdUserDNI = "";
    this.DatosUsuariosalesforce = [];
    this.DatosUsuariosalesforce2 = [];
    this.DatosUsuarioprinex = [];
    this.DatosUsuarioprinex2 = [];
    this.Viviendassalesforce = [];
    this.arrayInmueblesIDS = [];
    this.DatoViviendaPrinex = [];





    if(dni != "")
    {
      
        this.wsService.getIdUserSalesForce(dni).subscribe(
          data => {
            if(data['result'].length === 0){
              disponibilidad.style.display = 'none';
              this.estadoViviendaEscriturada = "Verdadero";
              document.getElementById('datosInmuebles').style.display = 'none';
              alert('El usuario con el DNI '+dni+' no existe en SalesForce, revíselo');
            }
            else
            {
              
                data['result'].forEach(element => {
                  idUser = element['Id'];
                  this.IdUserDNI = idUser;
                  let UserSalesForce = new datosUsuarioSalesForce();
                  UserSalesForce.NAME_USER = element['name'];
                  UserSalesForce.EMAIL_USER = element['email'];
                  UserSalesForce.TELEFONO1 = element['telefono1'];
                  UserSalesForce.TELEFONO2 = element['telefono2'];
                  if(element['fechaareaprivada'] === null)
                  {
                    UserSalesForce.FECHA_AREAPRIVADA = 'Falso';
                    this.activadoAreaPrivada = 'Falso';
                  }
                  else
                  {
                    UserSalesForce.FECHA_AREAPRIVADA = 'Verdadero';
                    this.activadoAreaPrivada = 'Verdadero';
                  }
                  UserSalesForce.ID_USER = element['Id'];
                  this.DatosUsuariosalesforce.push(UserSalesForce);
                });
                

                this.wsService.getIdInmuebleSalesForce(idUser).subscribe(
                  data => {
                    if(data['result'].length > 1)
                    {
                      document.getElementById('VariasPromociones').style.display = 'inline';
                      data['result'].forEach(element => {
                      let idinmueble = data['result'][0]["id"];              
                      console.log("Id Inmuebles"+idinmueble);                      
                      let valorPromociones = new DatosVariasPromociones();
                      valorPromociones.IDOPORTUNIDAD = element["id"];
                      valorPromociones.NAMEOPORTUNIDAD = element["name"];
                      valorPromociones.IDPROMOCION = element["promocion__c"];
                      valorPromociones.STAGENAME = element["stagename"];
                      this.wsService.getNamePromotion(valorPromociones.IDPROMOCION).subscribe(
                        data =>{
                          if(data['result'].length != 0){
                            valorPromociones.NAMEPROMOCION = data['result'];
                          }
                        }
                      );
                      this.arrayInmueblesIDS.push(valorPromociones);
                      });
                      
                    }
                    //}
                    else
                    {
                      if(data['result'][0]["id"] != 0)
                      {
                        this.idOportunidadSalesForce = data['result'][0]["id"];
                        this.stageNameSalesForce = data['result'][0]["stagename"]
                        this.wsService.getDatosInmuebleSalesForce(data['result'][0]["id"]).subscribe(
                          data => {
                            let datosinmuebles = JSON.stringify(data['result']);
                            if(data['result'] != undefined)
                            {
                              data['result'].forEach((element, index) => {
                                this.Inmuebles.push(element['idInmueble']);
                                if(index == 0)
                                {
                                  this.IdInmueble = element['idInmueble'];
                                  this.estadoComercialSalesForce = element['estadoComercialSalesForce'];
                                  console.log("VIVIENDA:"+element['estadoComercialSalesForce']);
                                  if(element['estadoComercialSalesForce'] != 'Escriturada')
                                  {
                                    this.estadoViviendaEscriturada = 'Falso';
                                  }
                                  else
                                  {
                                    this.estadoViviendaEscriturada = 'Verdadero';
                                  }
                                }
                                
                                let VivSalesForce = new datosViviendaSalesForce();
                                VivSalesForce.NAME_INMUEBLE = element['nameInmueble'];
                                VivSalesForce.PVP = element['pvp'];
                                VivSalesForce.NOMBRE_PROMOCION = element['nombrePromocion'];
                                VivSalesForce.ESTADO_COMERCIAL = element['estadoComercial'];

                                if(element['estadoComercialSalesForce'] != 'Escriturada')
                                {
                                  if(element['estadoComercialSalesForce'] == null)
                                    VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = "(va a pasar a Escriturada)";
                                  else
                                    VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = element['estadoComercialSalesForce']+" – (va a pasar a Escriturada)";
                                }
                                else
                                  VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = element['estadoComercialSalesForce'];
                                
                                
                                if(fechaactual != element['Fecha_de_entrega__c'])
                                {
                                  if(element['estadoComercialSalesForce'] == null)
                                    VivSalesForce.FECHA_DE_ENTREGA__C = "(va a pasar a "+fechaactual+")";
                                  else
                                    VivSalesForce.FECHA_DE_ENTREGA__C = element['Fecha_de_entrega__c']+" – (va a pasar a "+fechaactual+")";
                                }
                                else                              
                                  VivSalesForce.FECHA_DE_ENTREGA__C = element['Fecha_de_entrega__c'];
                                
                                
                                this.Viviendassalesforce.push(VivSalesForce);
                              });
                            }
                          },
                          error => {
                            //console.log(error, { depth: null});
                            this.wsService.getpintarTrazasLogs(this.nombreUsuario,error);
                          }
                        );

                      }
                      else
                      {
                        this.wsService.getIdSecundarioInmuebleSalesForce(idUser).subscribe(
                          data => {
                            let idinmueble = data['result'];
                            if(idinmueble != 0)
                            {
                              this.wsService.getDatosInmuebleSalesForce(idinmueble).subscribe(
                                data => {
                                  let datosinmuebles = JSON.stringify(data['result']);
                                  
                                  data['result'].forEach((element, index) => {
                                    this.Inmuebles.push(element['idInmueble']);
                                    if(index == 0)
                                    {
                                      this.IdInmueble = element['idInmueble'];
                                      this.estadoComercialSalesForce = element['estadoComercialSalesForce'];
                                    }
                                    let VivSalesForce = new datosViviendaSalesForce();
                                    VivSalesForce.NAME_INMUEBLE = element['nameInmueble'];
                                    VivSalesForce.PVP = element['pvp'];
                                    VivSalesForce.NOMBRE_PROMOCION = element['nombrePromocion'];
                                    VivSalesForce.ESTADO_COMERCIAL = element['estadoComercial'];
                                    if(element['estadoComercialSalesForce'] != 'Escriturada')
                                {
                                  if(element['estadoComercialSalesForce'] == null)
                                    VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = "(va a pasar a Escriturada)";
                                  else
                                    VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = element['estadoComercialSalesForce']+" – (va a pasar a Escriturada)";
                                }
                                else
                                  VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = element['estadoComercialSalesForce'];
                                
                                
                                if(fechaactual != element['Fecha_de_entrega__c'])
                                {
                                  if(element['estadoComercialSalesForce'] == null)
                                    VivSalesForce.FECHA_DE_ENTREGA__C = "(va a pasar a "+fechaactual+")";
                                  else
                                    VivSalesForce.FECHA_DE_ENTREGA__C = element['Fecha_de_entrega__c']+" – (va a pasar a "+fechaactual+")";
                                }
                                else                              
                                  VivSalesForce.FECHA_DE_ENTREGA__C = element['Fecha_de_entrega__c'];
                                    this.Viviendassalesforce.push(VivSalesForce);
                                  });
                                  
                                 

            
                                },
                                error => {
                                  //console.log(error, { depth: null});
                                  this.wsService.getpintarTrazasLogs(this.nombreUsuario,error);
                                }
                              );
                            }
                            
                          },
                          error => {
                            //console.log(error, { depth: null});
                            this.wsService.getpintarTrazasLogs(this.nombreUsuario,error);
                          }
                        );
                        

                        //this.activadoAreaPrivada = 'Verdadero';
                      }
                    }

                    //COMPROBACION SI EXISTE EL DNI EN PRINEX
                    let selectDNI = AppSettings.SELECT_DNI(dni);
                    this.wsService.getDNIPrinex(selectDNI)
                      .pipe(
                        finalize(() => {
                        })
                      )
                      .subscribe(
                        result => {
                          this.wsService.getpintarTrazasLogs(this.nombreUsuario,result);
                          this.wsService.getpintarTrazasLogs(this.nombreUsuario,result.length);

                            // for(let i = 0; i < result.length; i++) {
                            // }
                            if(result.length === 0)
                            {
                              document.getElementById('disponibilidadPrinex').innerHTML = 'El usuario con este DNI no existe en PRINEX';
                              disponibilidad.style.display = 'contents';
                              //dnidisponibilidad.value = 'El usuario con este DNI no existe en PRINEX';
                            }
                            else
                            {
                              document.getElementById('disponibilidadPrinex').innerHTML = 'El usuario con este DNI está también en PRINEX';
                              disponibilidad.style.display = 'contents';
                              //dnidisponibilidad.value = 'El usuario con este DNI está también en PRINEX';
                              let UserPrinex = new datosUsuarioPrinex();
                              UserPrinex.NAME_USER = result[0]['CONOMP'] + result[0]['COAPEP'];
                              UserPrinex.TELEFONO1 = result[0]['COTLFP'];
                              UserPrinex.EMAIL = result[0]['COMAIL'];
                              if(result[0]['COMOVIL'] !== null)
                              {
                                UserPrinex.TELEFONO2 = result[0]['COMOVIL'];
                              }
                              else
                              {
                                UserPrinex.TELEFONO2 = ' '
                              }
                              UserPrinex.COD_PRINEX = result[0]['COCODI'];
                              this.DatosUsuarioprinex.push(UserPrinex);
                            }

                            
                        },
                        error => {
                          this.wsService.getpintarTrazasLogs(this.nombreUsuario,'Error Obtención del DNI primario');
                        }
                      );

                      let selectInmueble = AppSettings.SELECT_PROPIETARIOSPRINEX(dni);
                      datosViviendacabecera.style.display = 'contents';
                      this.wsService.getPropiedadesPrinex(selectInmueble)
                      .pipe(
                        finalize(() => {
                        })
                      )
                      .subscribe(
                        result => {
                          this.wsService.getpintarTrazasLogs(this.nombreUsuario,result);
                          this.wsService.getpintarTrazasLogs(this.nombreUsuario,result.length);
                            for(let k = 0; k<result.length; k++)
                            {
                              let ViviendaPrinex = new datosVivienda();
                              ViviendaPrinex.NOMBRE_VIVIENDA = result[k]['NOMBRE_INMUEBLE'];
                              ViviendaPrinex.PRECIO_VIVIENDA = result[k]['NETO_VIVIENDA'];
                              if(result[k]['METROS_UTILES'] !== null && result[k]['METROS_UTILES'] !== '')
                              {
                                ViviendaPrinex.METROS_HABILES = result[k]['METROS_UTILES'].toFixed(2);
                              }
                              else
                              {
                                ViviendaPrinex.METROS_HABILES = result[k]['METROS_UTILES'];
                              }
                  
                              if(result[k]['METROS_CONS'] !== null && result[k]['METROS_CONS'] !== '')
                              {
                                ViviendaPrinex.METROS_CONSTRUIDOS = result[k]['METROS_CONS'].toFixed(2);
                              }
                              else
                              {
                                ViviendaPrinex.METROS_CONSTRUIDOS = result[k]['METROS_CONS'];
                              }
                  
                  
                              ViviendaPrinex.ESTADO_VIVIENDA = result[k]['ESTADO'];
                              this.DatoViviendaPrinex.push(ViviendaPrinex);
                            }
                            

                        },
                        error => {
                          this.wsService.getpintarTrazasLogs(this.nombreUsuario,'Error Obtención del datos vivienda');
                        }
                      );
                    



                  },
                  error => {
                    //console.log(error, { depth: null});
                    this.wsService.getpintarTrazasLogs(this.nombreUsuario,error);
                  }
                );

            }
            

            
          },
          error => {
            //console.log(error, { depth: null});
            this.wsService.getpintarTrazasLogs(this.nombreUsuario,error);
          }
        );

    }
    // else
    // {
    //   if(idSalesForce != "")
    //   {
    //     datosViviendacabecera.style.display = 'none';
    //     disponibilidad.style.display = 'none';
    //     this.wsService.getdatosUserSalesForce(idSalesForce).subscribe(
    //       data => {
  
    //         if(data['result'].length === 0){
    //           alert('El usuario con el id '+idSalesForce+' no existe en SalesForce, revíselo');
    //         }
    //         else
    //         {
              
    //             data['result'].forEach(element => {
    //               idUser = element['Id'];
    //               let UserSalesForce = new datosUsuarioSalesForce();
    //               UserSalesForce.NAME_USER = element['name'];
    //               UserSalesForce.EMAIL_USER = element['email'];
    //               UserSalesForce.TELEFONO1 = element['telefono1'];
    //               UserSalesForce.TELEFONO2 = element['telefono2'];
    //               if(element['registrado'] === false)
    //               {
    //                 UserSalesForce.FECHA_AREAPRIVADA = 'Falso';
    //                 this.activadoAreaPrivada = 'Falso';
    //               }
    //               else
    //               {
    //                 UserSalesForce.FECHA_AREAPRIVADA = 'Verdadero';
    //                 this.activadoAreaPrivada = 'Verdadero';
    //               }
    //               UserSalesForce.ID_USER = element['Id'];
    //               this.DatosUsuariosalesforce.push(UserSalesForce);
    //             });
                
  
    //             this.wsService.getIdInmuebleSalesForce(idUser).subscribe(
    //               data => {
    //                 let idinmueble = [];
    //                 idinmueble = data['result'];
    //                 if(idinmueble[0] !== 0)
    //                 {
    //                   for(let z = 0; z < idinmueble.length; z++)
    //                   {
    //                     this.wsService.getDatosInmuebleSalesForce(idinmueble[z]).subscribe(
    //                       data => {
    //                         let datosinmuebles = JSON.stringify(data['result']);
                            
    //                         data['result'].forEach((element, index) => {
    //                           if(index == 0)
    //                           {
    //                             this.IdInmueble = element['idInmueble'];
    //                             this.estadoComercialSalesForce = element['estadoComercialSalesForce'];
    //                           }
    //                           let VivSalesForce = new datosViviendaSalesForce();
    //                           VivSalesForce.NAME_INMUEBLE = element['nameInmueble'];
    //                           VivSalesForce.PVP = element['pvp'];
    //                           VivSalesForce.NOMBRE_PROMOCION = element['nombrePromocion'];
    //                           VivSalesForce.ESTADO_COMERCIAL = element['estadoComercial'];
    //                           VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = element['estadoComercialSalesForce'];
    //                           this.Viviendassalesforce.push(VivSalesForce);
    //                         });
                            
    //                       },
    //                       error => {
    //                         console.log(error, { depth: null});
    //                       }
    //                     );
    
    //                   }
                      
    //                 }
    //                 else
    //                 {                      
  
    //                   this.wsService.getIdSecundarioInmuebleSalesForce(idUser).subscribe(
    //                     data => {
    //                       let idinmueble = data['result'];
    //                         this.wsService.getDatosInmuebleSalesForce(idinmueble).subscribe(
    //                           data => {
    //                             let datosinmuebles = JSON.stringify(data['result']);
                                
    //                             data['result'].forEach((element, index) => {
    //                               if(index == 0)
    //                               {
    //                                 this.IdInmueble = element['idInmueble'];
    //                                 this.estadoComercialSalesForce = element['estadoComercialSalesForce'];
    //                               }
    //                               let VivSalesForce = new datosViviendaSalesForce();
    //                               VivSalesForce.NAME_INMUEBLE = element['nameInmueble'];
    //                               VivSalesForce.PVP = element['pvp'];
    //                               VivSalesForce.NOMBRE_PROMOCION = element['nombrePromocion'];
    //                               VivSalesForce.ESTADO_COMERCIAL = element['estadoComercial'];
    //                               VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = element['estadoComercialSalesForce'];
    //                               this.Viviendassalesforce.push(VivSalesForce);
    //                             });
                                
    //                           },
    //                           error => {
    //                              console.log(error, { depth: null});
    //                          }
    //                         );
    //                     },
    //                     error => {
    //                       console.log(error, { depth: null});
    //                     }
    //                   );
                      
  
                              
    //                   //this.activadoAreaPrivada = 'Verdadero';
                      
  
    //               }
    //               },
    //               error => {
    //                 console.log(error, { depth: null});
    //               }
    //             );
  
    //         }         
   
    //       },
    //       error => {
    //         console.log(error, { depth: null});
    //       }
    //     );
  
    //   }
    //   else
    //   {
    //     alert("Debe indicar el Dni del usuario o el Id de SalesForce");
    //   }

    // }


  }

  Updateuser() {

    let id = this.IdInmueble; 
    let name = 'Escriturada';
    let uploadFile = document.getElementById("actualizando");
    uploadFile.style.display = "block";
    let cargando = document.getElementById("datosSalesForce");
    cargando.style.display = "none";
    let email:any = (<HTMLInputElement>document.getElementById('emailUserSalesForce')).value;
    let idUser:any = (<HTMLInputElement>document.getElementById('idUserSalesForce')).value;
    this.IdUserDNI = idUser;


    if(email == " ")
    {
      alert("El usuario con id "+idUser+" no tiene email, por lo tanto no puede ser dado de alta en el área privada");
      uploadFile.style.display = "none";
      cargando.style.display = "block";
    }
    else
    {
     
     this.wsService.getUserAdminAreaPrivada(email).subscribe(
     data => {
       if(data != false)
       {
           this.wsService.updateuserSaleForceAccount(idUser).subscribe(
             data => {
               if(data == true)
               {
                 alert('Actualización del usuario '+idUser+' correcta en SalesForce');
                 this.route.queryParams.subscribe(params => {
                      uploadFile.style.display = "none";
                      cargando.style.display = "block";
                      this.GetViviendasuser();
                  });

               }
               else
               {
                 alert('Se produjo un error al actualizar el campo en SalesForce');
                 uploadFile.style.display = "none";
                 cargando.style.display = "block";
               }
             },
             error => {
                alert('Error al dar de alta a los usuarios en el área privada')
                this.wsService.getpintarTrazasLogs(this.nombreUsuario,error);
                uploadFile.style.display = "none";
                cargando.style.display = "block";

             }
           );                 
       }
       else
       {
        alert('Se produjo un error al crear el usuario en magnolia o en el active directory');
        uploadFile.style.display = "none";
        cargando.style.display = "block";
       }    
       
     },
     error => {
      alert('Error al gestionar el alta a los usuarios en el área privada')
       this.wsService.getpintarTrazasLogs(this.nombreUsuario,error);
       uploadFile.style.display = "none";
       cargando.style.display = "block";
     }
   ); 
 }

    // if(this.estadoComercialSalesForce == "Escriturada")
    //   name = 'Contrato CV';


    // this.wsService.updateuserSaleForce(id, name).subscribe(
    //   data => {
    //     if(data == true)
    //     {
    //       setTimeout( () => { 
            
    //         this.wsService.updateuserSaleForce(id, this.estadoComercialSalesForce).subscribe(
    //           data => {
    //             if(data == true)
    //             {
    //               alert('Actualización correcta en SalesForce');
    //               this.route.queryParams.subscribe(params => {
    //                 this.router.navigate(['/promociones'], { replaceUrl: true });
    //               });
    //             }
    //             else
    //             {
    //               alert('Se produjo un error al actualizar el campo en SalesForce Fase 2');
    //             }
    //           },
    //           error => {
    //             console.log(error, { depth: null});
    //           }
    //         );
          
    //       }, 5000 );
    //     }
    //     else
    //     {
    //       alert('Se produjo un error al actualizar el campo en SalesForce Fase 1');
    //     }
    //   },
    //   error => {
    //     console.log(error, { depth: null});
    //   }
    // );

  }


  UpdateInmueble() {

    let id = this.IdInmueble; 
    let name = 'Escriturada';
    let fechaactual;
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    fechaactual = yyyy + '-' + mm + '-' + dd;
    let uploadFile = document.getElementById("actualizando");
    uploadFile.style.display = "block";
    let cargando = document.getElementById("datosSalesForce");
    cargando.style.display = "none";


          setTimeout( () => {

            this.Inmuebles.forEach((element, index) => {
            
            this.wsService.updateuserSaleForce(element, name, fechaactual).subscribe(
              data => {
                var valor = index + 1;
                if(data == true && valor == this.Inmuebles.length)
                {
                  if(this.stageNameSalesForce == 'Reserva')
                  {
                    this.wsService.updateoportunitySaleForce(this.idOportunidadSalesForce).subscribe(
                    data2 => {
                      if(data2 == true)
                      {
                        alert('Actualización correcta en SalesForce');
                        this.route.queryParams.subscribe(params => {
                        uploadFile.style.display = "none";
                        cargando.style.display = "block";
                        this.GetViviendasuser();
                        });
                      }
                      else
                      {
                        alert('Se produjo un error al actualizar el campo en SalesForce');
                      }
                    },
                    error => {
                      //console.log(error, { depth: null});
                      this.wsService.getpintarTrazasLogs(this.nombreUsuario,error);
                    }

                    );

                  }
                  else
                  {
                    alert('Actualización correcta en SalesForce');
                    this.route.queryParams.subscribe(params => {
                      uploadFile.style.display = "none";
                      cargando.style.display = "block";
                      this.GetViviendasuser();
                    });

                  }
                  
                }
                else
                {
                  if(data == false)
                    alert('Se produjo un error al actualizar el campo en SalesForce');
                }
              },
              error => {
                //console.log(error, { depth: null});
                this.wsService.getpintarTrazasLogs(this.nombreUsuario,error);
              }
            );

          });
          
          }, 5000 );

  }

  
  GetViviendaOportunity(idOportunidad: string, stageName: string) {
    this.IdInmueble = '';
    this.stageNameSalesForce = stageName;
    this.Inmuebles = [];
    let dni = (<HTMLInputElement>document.getElementById('dniUserSalesForce')).value;
    let oportunidad = idOportunidad;
    this.idOportunidadSalesForce = idOportunidad;

    let disponibilidad = document.getElementById('disponiblePrinex') as HTMLInputElement;
    let datosViviendacabecera = document.getElementById('datosVivienda') as HTMLInputElement;
    let codigoSalesForceUser = "";
    let fechaactual;
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    fechaactual = yyyy + '-' + mm + '-' + dd;

    let idUser;
    this.DatosUsuariosalesforce = [];
    this.DatosUsuariosalesforce2 = [];
    this.DatosUsuarioprinex = [];
    this.DatosUsuarioprinex2 = [];
    this.Viviendassalesforce = [];
    this.arrayInmueblesIDS = [];
    this.DatoViviendaPrinex = [];

    if(oportunidad != "")
    {
      if(dni != "")
      {
        document.getElementById('VariasPromociones').style.display = 'none';
          this.wsService.getIdUserSalesForce(dni).subscribe(
            data => {
              if(data['result'].length === 0){
                disponibilidad.style.display = 'none';
                this.estadoViviendaEscriturada = "Verdadero";
                document.getElementById('datosInmuebles').style.display = 'none';
                alert('El usuario con el DNI '+dni+' no existe en SalesForce, revíselo');
              }
              else
              {                
                  data['result'].forEach(element => {
                    idUser = element['Id'];
                    let UserSalesForce = new datosUsuarioSalesForce();
                    UserSalesForce.NAME_USER = element['name'];
                    UserSalesForce.EMAIL_USER = element['email'];
                    UserSalesForce.TELEFONO1 = element['telefono1'];
                    UserSalesForce.TELEFONO2 = element['telefono2'];
                    if(element['fechaareaprivada'] === null)
                    {
                      UserSalesForce.FECHA_AREAPRIVADA = 'Falso';
                      this.activadoAreaPrivada = 'Falso';
                    }
                    else
                    {
                      UserSalesForce.FECHA_AREAPRIVADA = 'Verdadero';
                      this.activadoAreaPrivada = 'Verdadero';
                    }
                    UserSalesForce.ID_USER = element['Id'];
                    this.DatosUsuariosalesforce.push(UserSalesForce);
                  });

                        if(oportunidad != String(0))
                        {
                          let idinmueble = oportunidad;
                          this.wsService.getDatosInmuebleSalesForce(idinmueble).subscribe(
                            data => {
                              let datosinmuebles = JSON.stringify(data['result']);
                              if(data['result'] != undefined)
                              {
                                data['result'].forEach((element, index) => {
                                  this.Inmuebles.push(element['idInmueble']);
                                  if(index == 0)
                                  {
                                    this.IdInmueble = element['idInmueble'];
                                    this.estadoComercialSalesForce = element['estadoComercialSalesForce'];
                                    console.log("VIVIENDA:"+element['estadoComercialSalesForce']);
                                    if(element['estadoComercialSalesForce'] != 'Escriturada')
                                    {
                                      this.estadoViviendaEscriturada = 'Falso';
                                    }
                                    else
                                    {
                                      this.estadoViviendaEscriturada = 'Verdadero';
                                    }
                                  }
                                  
                                  let VivSalesForce = new datosViviendaSalesForce();
                                  VivSalesForce.NAME_INMUEBLE = element['nameInmueble'];
                                  VivSalesForce.PVP = element['pvp'];
                                  VivSalesForce.NOMBRE_PROMOCION = element['nombrePromocion'];
                                  VivSalesForce.ESTADO_COMERCIAL = element['estadoComercial'];
  
                                  if(element['estadoComercialSalesForce'] != 'Escriturada')
                                  {
                                    if(element['estadoComercialSalesForce'] == null)
                                      VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = "(va a pasar a Escriturada)";
                                    else
                                      VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = element['estadoComercialSalesForce']+" – (va a pasar a Escriturada)";
                                  }
                                  else
                                    VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = element['estadoComercialSalesForce'];
                                  
                                  
                                  if(fechaactual != element['Fecha_de_entrega__c'])
                                  {
                                    if(element['estadoComercialSalesForce'] == null)
                                      VivSalesForce.FECHA_DE_ENTREGA__C = "(va a pasar a "+fechaactual+")";
                                    else
                                      VivSalesForce.FECHA_DE_ENTREGA__C = element['Fecha_de_entrega__c']+" – (va a pasar a "+fechaactual+")";
                                  }
                                  else                              
                                    VivSalesForce.FECHA_DE_ENTREGA__C = element['Fecha_de_entrega__c'];
                                  
                                  
                                  this.Viviendassalesforce.push(VivSalesForce);
                                });
                              }
                            },
                            error => {
                              //console.log(error, { depth: null});
                              this.wsService.getpintarTrazasLogs(this.nombreUsuario,error);
                            }
                          );
  
                        }
                        else
                        {
                          this.wsService.getIdSecundarioInmuebleSalesForce(this.IdUserDNI).subscribe(
                            data => {
                              let idinmueble = data['result'];
                              if(idinmueble != 0)
                              {
                                this.wsService.getDatosInmuebleSalesForce(idinmueble).subscribe(
                                  data => {
                                    let datosinmuebles = JSON.stringify(data['result']);
                                    
                                    data['result'].forEach((element, index) => {
                                      this.Inmuebles.push(element['idInmueble']);
                                      if(index == 0)
                                      {
                                        this.IdInmueble = element['idInmueble'];
                                        this.estadoComercialSalesForce = element['estadoComercialSalesForce'];
                                      }
                                      let VivSalesForce = new datosViviendaSalesForce();
                                      VivSalesForce.NAME_INMUEBLE = element['nameInmueble'];
                                      VivSalesForce.PVP = element['pvp'];
                                      VivSalesForce.NOMBRE_PROMOCION = element['nombrePromocion'];
                                      VivSalesForce.ESTADO_COMERCIAL = element['estadoComercial'];
                                      if(element['estadoComercialSalesForce'] != 'Escriturada')
                                  {
                                    if(element['estadoComercialSalesForce'] == null)
                                      VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = "(va a pasar a Escriturada)";
                                    else
                                      VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = element['estadoComercialSalesForce']+" – (va a pasar a Escriturada)";
                                  }
                                  else
                                    VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = element['estadoComercialSalesForce'];
                                  
                                  
                                  if(fechaactual != element['Fecha_de_entrega__c'])
                                  {
                                    if(element['estadoComercialSalesForce'] == null)
                                      VivSalesForce.FECHA_DE_ENTREGA__C = "(va a pasar a "+fechaactual+")";
                                    else
                                      VivSalesForce.FECHA_DE_ENTREGA__C = element['Fecha_de_entrega__c']+" – (va a pasar a "+fechaactual+")";
                                  }
                                  else                              
                                    VivSalesForce.FECHA_DE_ENTREGA__C = element['Fecha_de_entrega__c'];
                                      this.Viviendassalesforce.push(VivSalesForce);
                                    });

                                  },
                                  error => {
                                    //console.log(error, { depth: null});
                                    this.wsService.getpintarTrazasLogs(this.nombreUsuario,error);
                                  }
                                );
                              }
                              
                            },
                            error => {
                              //console.log(error, { depth: null});
                              this.wsService.getpintarTrazasLogs(this.nombreUsuario,error);
                            }
                          );
                          
  
                          //this.activadoAreaPrivada = 'Verdadero';
                        }

                        
                                    //COMPROBACION SI EXISTE EL DNI EN PRINEX
                                    let selectDNI = AppSettings.SELECT_DNI(dni);
                                    this.wsService.getDNIPrinex(selectDNI)
                                      .pipe(
                                        finalize(() => {
                                        })
                                      )
                                      .subscribe(
                                        result => {
                                          this.wsService.getpintarTrazasLogs(this.nombreUsuario,result);
                                          this.wsService.getpintarTrazasLogs(this.nombreUsuario,result.length);
        
        
                                            // for(let i = 0; i < result.length; i++) {
                                            // }
                                            if(result.length === 0)
                                            {
                                              document.getElementById('disponibilidadPrinex').innerHTML = 'El usuario con este DNI no existe en PRINEX';
                                              disponibilidad.style.display = 'contents';
                                              //dnidisponibilidad.value = 'El usuario con este DNI no existe en PRINEX';
                                            }
                                            else
                                            {
                                              document.getElementById('disponibilidadPrinex').innerHTML = 'El usuario con este DNI está también en PRINEX';
                                              disponibilidad.style.display = 'contents';
                                            }
                                            let UserPrinex = new datosUsuarioPrinex();
                                            UserPrinex.NAME_USER = result[0]['CONOMP'] + result[0]['COAPEP'];
                                            UserPrinex.EMAIL = result[0]['COMAIL'];
                                            UserPrinex.TELEFONO1 = result[0]['COTLFP'];
                                            if(result[0]['COMOVIL'] !== null)
                                            {
                                              UserPrinex.TELEFONO2 = result[0]['COMOVIL'];
                                            }
                                            else
                                            {
                                              UserPrinex.TELEFONO2 = ' '
                                            }
                                            UserPrinex.COD_PRINEX = result[0]['COCODI'];
                                            console.log("zzzzz"+UserPrinex);
                                            this.DatosUsuarioprinex.push(UserPrinex);
                                                                                  
                                      },
                                        error => {
                                          this.wsService.getpintarTrazasLogs(this.nombreUsuario,'Error Obtención del DNI secundario');
                                        }
                                      );

                              
                                      let selectInmueble = AppSettings.SELECT_PROPIETARIOSPRINEX(dni);
                                      datosViviendacabecera.style.display = 'contents';
                                      this.wsService.getPropiedadesPrinex(selectInmueble)
                                      .pipe(
                                        finalize(() => {
                                        })
                                      )
                                      .subscribe(
                                        result => {
                                          
                                          this.wsService.getpintarTrazasLogs(this.nombreUsuario,result);
                                          this.wsService.getpintarTrazasLogs(this.nombreUsuario,result.length);
                                            for(let k = 0; k<result.length; k++)
                                            {
                                              let ViviendaPrinex = new datosVivienda();
                                              ViviendaPrinex.NOMBRE_VIVIENDA = result[k]['NOMBRE_INMUEBLE']
                                              ViviendaPrinex.PRECIO_VIVIENDA = result[k]['NETO_VIVIENDA'];
                                              if(result[k]['METROS_UTILES'] !== null && result[k]['METROS_UTILES'] !== '')
                                              {
                                                ViviendaPrinex.METROS_HABILES = result[k]['METROS_UTILES'].toFixed(2);
                                              }
                                              else
                                              {
                                                ViviendaPrinex.METROS_HABILES = result[k]['METROS_UTILES'];
                                              }
                                  
                                              if(result[k]['METROS_CONS'] !== null && result[k]['METROS_CONS'] !== '')
                                              {
                                                ViviendaPrinex.METROS_CONSTRUIDOS = result[k]['METROS_CONS'].toFixed(2);
                                              }
                                              else
                                              {
                                                ViviendaPrinex.METROS_CONSTRUIDOS = result[k]['METROS_CONS'];
                                              }                                                                
                                              ViviendaPrinex.ESTADO_VIVIENDA = result[k]['ESTADO'];
                                              this.DatoViviendaPrinex.push(ViviendaPrinex);
                                            }
                                            
        
                                        },
                                        error => {
                                          this.wsService.getpintarTrazasLogs(this.nombreUsuario,'Error Obtención del datos vivienda');
                                        }
                                      );
  
              }
              
  
              
            },
            error => {
              console.log(error, { depth: null});
              this.wsService.getpintarTrazasLogs(this.nombreUsuario,error);
            }
          );
  
      }

    }
    else
    {
      alert("No existe ninguna oportunidad con ese ID");
    }

  }


}

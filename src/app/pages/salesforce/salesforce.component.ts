import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Categoria} from '../../classes/categoria';
import { ProductosService } from 'src/app/services/productos.service';
import { WebsocketService } from '../../services/websocket.service';
import { finalize } from 'rxjs/operators';
import { datosPropietario} from '../../classes/datosPropietario';
import { AuthenticationService } from '../../core';
import * as $ from 'jquery';
import { datosAnejos } from 'src/app/classes/datosAnejos';
import { datosVivienda } from 'src/app/classes/datosVivienda';
import { AppSettings } from '../../shared/app-settings';
import { datosViviendaSalesForce } from 'src/app/classes/datosViviendaSalesForce';
import { datosUsuarioPrinex } from 'src/app/classes/datosUsuarioPrinex';

declare var html2canvas: any;
declare var jsPDF: any;
declare var req: any;


@Component({
  selector: 'app-salesforce',
  templateUrl: './salesforce.component.html',
  styleUrls: ['./salesforce.component.css']
})



export class SalesforceComponent implements OnInit {

  public promociones: string [] = [];
  public viviendas: string [] = [];
  public Viviendassalesforce: datosViviendaSalesForce [] = [];
  public PromocionesID: string;
  public nombreUsuario: string;
  public arrayViviendas: string [] = [];
  public cambios: string [] = [];
  public usuario: datosPropietario;
  public anejos: datosAnejos;
  public datosViviendas: datosVivienda;
  public datosViendaArray: datosVivienda [] = [];
  public activadoAreaPrivada = '';
  public IdInmueble = '';
  public estadoComercialSalesForce = '';
  public codUsersOportunidad : string = '';
  public nameUsersOportunidad : string = '';
  public idOportunidad : string = '';
  public DatosUsuarioprinex: datosUsuarioPrinex [] = [];
  public emailSalesForce : string = '';
  public idSalesForce : string = '';


  constructor(private router: Router, public wsService: WebsocketService, public authenticationService: AuthenticationService, private route: ActivatedRoute) { }

  // ngOnInit() {
  //   const salesforce = this.authenticationService.SalesForce;
  //   this.IdInmueble = '';
  //   this.idOportunidad = '';
  //   this.DatosUsuarioprinex = [];

  public usuarioAdmin;
  ngOnInit() {
    
    
    const credentials = this.authenticationService.credentials;

    const nombreUsuario = credentials.username;
    console.log('El nombre de usuario es: ' + nombreUsuario);
    console.log('vamos a mirar el perfil');


  this.wsService.getUserApp(nombreUsuario)
  .pipe(
    finalize(() => {
    })
  )
  .subscribe(
    result => {
      let valor = JSON.stringify(result);
      let valor2 = JSON.parse(valor);
      console.log('Vamos a la obtención del perfil' +valor2["result"] );
      this.usuarioAdmin = valor2["result"];
      console.log('El resultado es: '+this.usuarioAdmin);
      if(this.usuarioAdmin.toLowerCase() == 'admin')
      {
        if(localStorage.getItem('IdSalesForce') != '')
        {
          let nameuser = localStorage.getItem('userSalesForce');
          let dniUserSalesForce = localStorage.getItem('dniSalesForce');
          let email = localStorage.getItem('emailuserSalesForce');
          this.emailSalesForce = email;
         
          if(localStorage.getItem('areaprivadaSalesForce') === 'null')
          {
            this.activadoAreaPrivada = 'Falso';
          }
          else
          {
            this.activadoAreaPrivada = 'Verdadero';
          }
          let idsalesforce = localStorage.getItem('IdSalesForce');
          this.idSalesForce = idsalesforce;
          let containerName = document.getElementById('nameUserSalesForce') as HTMLInputElement;
          containerName.value = nameuser;
          let containeremail = document.getElementById('EmailSalesForce') as HTMLInputElement;
          containeremail.value = email;
          let containerAreaPrivada = document.getElementById('AreaPrivadaSalesForce') as HTMLInputElement;
          containerAreaPrivada.value = this.activadoAreaPrivada;
          let productcode = localStorage.getItem('productCode');
          let telefono1 = localStorage.getItem('telefono1');
          let Telefono1SalesForce = document.getElementById('Telefono1SalesForce') as HTMLInputElement;
          Telefono1SalesForce.value = telefono1;
          let telefono2 = localStorage.getItem('telefono2');
          let Telefono2SalesForce = document.getElementById('Telefono2SalesForce') as HTMLInputElement;
          Telefono2SalesForce.value = telefono2;
    
          
    
    
          this.wsService.getIdInmuebleSalesForce(idsalesforce).subscribe(
            data => {
              let idinmueble = data['result'];
              if(idinmueble != 0)
              {
                this.idOportunidad = idinmueble;            
                this.wsService.getDatosInmuebleSalesForce(idinmueble).subscribe(
                  data => {
                    let datosinmuebles = JSON.stringify(data['result']);
                    
                    data['result'].forEach((element, index) => {
                      if(index == 0)
                      {
                        this.IdInmueble = element['idInmueble'];
                        this.estadoComercialSalesForce = element['estadoComercialSalesForce'];
                      }
                      let VivSalesForce = new datosViviendaSalesForce();
                      VivSalesForce.ID = element['idInmueble'];
                      VivSalesForce.NAME_INMUEBLE = element['nameInmueble'];
                      VivSalesForce.PVP = element['pvp'];
                      VivSalesForce.NOMBRE_PROMOCION = element['nombrePromocion'];
                      VivSalesForce.ESTADO_COMERCIAL = element['estadoComercial'];
                      VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = element['estadoComercialSalesForce'];
                      this.Viviendassalesforce.push(VivSalesForce);
                    });
                    
                    
                    let selectDNI = AppSettings.SELECT_DNI(dniUserSalesForce);
                      this.wsService.getDNIPrinex(selectDNI)
                        .pipe(
                        finalize(() => {
                        })
                        )
                        .subscribe(
                          result => {
                               console.log("EL RESULT ES AHORA: "+result);
                               console.log(result.length);
    
                               // for(let i = 0; i < result.length; i++) {
                               // }
                              //  if(result.length === 0)
                              //  {
                              //     document.getElementById('disponibilidadPrinex').innerHTML = 'El usuario con este DNI no existe en PRINEX';
                              //     disponibilidad.style.display = 'contents';
                              //                 //dnidisponibilidad.value = 'El usuario con este DNI no existe en PRINEX';
                              //  }
                              //  else
                              //  {
                              //     document.getElementById('disponibilidadPrinex').innerHTML = 'El usuario con este DNI está también en PRINEX';
                              //     disponibilidad.style.display = 'contents';
                              //    //dnidisponibilidad.value = 'El usuario con este DNI está también en PRINEX';
                              //  }
    
                               let UserPrinex = new datosUsuarioPrinex();
                               UserPrinex.NAME_USER = result[0]['CONOMP'] + result[0]['COAPEP'];
                               UserPrinex.TELEFONO1 = result[0]['COTLFP'];
                               UserPrinex.EMAIL = result[0]['COMAIL'];
                               if(result[0]['COMOVIL'] !== null)
                               {
                                  UserPrinex.TELEFONO2 = result[0]['COMOVIL'];
                              }
                              else
                              {
                                UserPrinex.TELEFONO2 = ' '
                              }
                                 UserPrinex.COD_PRINEX = result[0]['COCODI'];
                                 this.DatosUsuarioprinex.push(UserPrinex);
                              },
                                 error => {
                                  console.log('Error Obtención del DNI primario');
                              }
                      );
    
                    
                  },
                  error => {
                    console.log(error, { depth: null});
                  }
                );
    
              }
              else
              {
                this.wsService.getIdPromocionProductCode(productcode).subscribe(
                  data => {
                     let idOportunidad = data['result'];
                     this.idOportunidad = idOportunidad;
                     if(idOportunidad != null)
                     {
                       
                     
                      this.wsService.getDatosInmuebleSalesForce(idOportunidad).subscribe(
                        data => {
                          let datosinmuebles = JSON.stringify(data['result']);
                          
                          data['result'].forEach((element, index) => {
                            if(index == 0)
                            {
                              this.IdInmueble = element['idInmueble'];
                              this.estadoComercialSalesForce = element['estadoComercialSalesForce'];
                            }
                            let VivSalesForce = new datosViviendaSalesForce();
                            VivSalesForce.ID = element['idInmueble'];
                            VivSalesForce.NAME_INMUEBLE = element['nameInmueble'];
                            VivSalesForce.PVP = element['pvp'];
                            VivSalesForce.NOMBRE_PROMOCION = element['nombrePromocion'];
                            VivSalesForce.ESTADO_COMERCIAL = element['estadoComercial'];
                            VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = element['estadoComercialSalesForce'];
                            this.Viviendassalesforce.push(VivSalesForce);
                          });           
                        },
                        error => {
                          console.log(error, { depth: null});
                        }
                      );
                    }
                    else
                    {
                      alert("Este usuario no tiene un id de Oportunidad correcto, realice una revisión");
                    }
                  },
                  error => {
                    console.log(error, { depth: null});
                  }
                );  
    
              }
            },
            error => {
              console.log(error, { depth: null});
            }
          );
    
          localStorage.setItem('userSalesForce', '');
          localStorage.setItem('emailuserSalesForce', '');
          localStorage.setItem('areaprivadaSalesForce', '');
          localStorage.setItem('IdSalesForce', '');
          localStorage.setItem('productCode', '');
        }
        else
        {
          this.route.queryParams.subscribe(params => {
            this.router.navigate(['/promociones'], { replaceUrl: true });
          });
        }
        
      }
      if(this.usuarioAdmin.toLowerCase() == 'user')
      {
        if(localStorage.getItem('IdSalesForce') != '')
        {
          let nameuser = localStorage.getItem('userSalesForce');
          let dniUserSalesForce = localStorage.getItem('dniSalesForce');
          let email = localStorage.getItem('emailuserSalesForce');
          this.emailSalesForce = email;
         
          if(localStorage.getItem('areaprivadaSalesForce') === 'null')
          {
            this.activadoAreaPrivada = 'Falso';
          }
          else
          {
            this.activadoAreaPrivada = 'Verdadero';
          }
          let idsalesforce = localStorage.getItem('IdSalesForce');
          this.idSalesForce = idsalesforce;
          let containerName = document.getElementById('nameUserSalesForce') as HTMLInputElement;
          containerName.value = nameuser;
          let containeremail = document.getElementById('EmailSalesForce') as HTMLInputElement;
          containeremail.value = email;
          let containerAreaPrivada = document.getElementById('AreaPrivadaSalesForce') as HTMLInputElement;
          containerAreaPrivada.value = this.activadoAreaPrivada;
          let productcode = localStorage.getItem('productCode');
          let telefono1 = localStorage.getItem('telefono1');
          let Telefono1SalesForce = document.getElementById('Telefono1SalesForce') as HTMLInputElement;
          Telefono1SalesForce.value = telefono1;
          let telefono2 = localStorage.getItem('telefono2');
          let Telefono2SalesForce = document.getElementById('Telefono2SalesForce') as HTMLInputElement;
          Telefono2SalesForce.value = telefono2;
    
          
    
    
          this.wsService.getIdInmuebleSalesForce(idsalesforce).subscribe(
            data => {
              let idinmueble = data['result'];
    
              if(idinmueble !== 0)
              {
                this.idOportunidad = idinmueble;            
                this.wsService.getDatosInmuebleSalesForce(idinmueble).subscribe(
                  data => {
                    let datosinmuebles = JSON.stringify(data['result']);
                    
                    data['result'].forEach((element, index) => {
                      if(index == 0)
                      {
                        this.IdInmueble = element['idInmueble'];
                        this.estadoComercialSalesForce = element['estadoComercialSalesForce'];
                      }
                      let VivSalesForce = new datosViviendaSalesForce();
                      VivSalesForce.ID = element['idInmueble'];
                      VivSalesForce.NAME_INMUEBLE = element['nameInmueble'];
                      VivSalesForce.PVP = element['pvp'];
                      VivSalesForce.NOMBRE_PROMOCION = element['nombrePromocion'];
                      VivSalesForce.ESTADO_COMERCIAL = element['estadoComercial'];
                      VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = element['estadoComercialSalesForce'];
                      this.Viviendassalesforce.push(VivSalesForce);
                    });
                    
                    
                    let selectDNI = AppSettings.SELECT_DNI(dniUserSalesForce);
                      this.wsService.getDNIPrinex(selectDNI)
                        .pipe(
                        finalize(() => {
                        })
                        )
                        .subscribe(
                          result => {
                               console.log("EL RESULT ES AHORA: "+result);
                               console.log(result.length);
    
                               // for(let i = 0; i < result.length; i++) {
                               // }
                              //  if(result.length === 0)
                              //  {
                              //     document.getElementById('disponibilidadPrinex').innerHTML = 'El usuario con este DNI no existe en PRINEX';
                              //     disponibilidad.style.display = 'contents';
                              //                 //dnidisponibilidad.value = 'El usuario con este DNI no existe en PRINEX';
                              //  }
                              //  else
                              //  {
                              //     document.getElementById('disponibilidadPrinex').innerHTML = 'El usuario con este DNI está también en PRINEX';
                              //     disponibilidad.style.display = 'contents';
                              //    //dnidisponibilidad.value = 'El usuario con este DNI está también en PRINEX';
                              //  }
    
                               let UserPrinex = new datosUsuarioPrinex();
                               UserPrinex.NAME_USER = result[0]['CONOMP'] + result[0]['COAPEP'];
                               UserPrinex.TELEFONO1 = result[0]['COTLFP'];
                               UserPrinex.EMAIL = result[0]['COMAIL'];
                               if(result[0]['COMOVIL'] !== null)
                               {
                                  UserPrinex.TELEFONO2 = result[0]['COMOVIL'];
                              }
                              else
                              {
                                UserPrinex.TELEFONO2 = ' '
                              }
                                 UserPrinex.COD_PRINEX = result[0]['COCODI'];
                                 this.DatosUsuarioprinex.push(UserPrinex);
                              },
                                 error => {
                                  console.log('Error Obtención del DNI primario');
                              }
                      );
    
                    
                  },
                  error => {
                    console.log(error, { depth: null});
                  }
                );
    
              }
              else
              {
    
                this.wsService.getIdPromocionProductCode(productcode).subscribe(
                  data => {
                     let idOportunidad = data['result'];
                     this.idOportunidad = idOportunidad;
                     this.wsService.getDatosInmuebleSalesForce(idOportunidad).subscribe(
                      data => {
                        let datosinmuebles = JSON.stringify(data['result']);
                        
                        data['result'].forEach((element, index) => {
                          if(index == 0)
                          {
                            this.IdInmueble = element['idInmueble'];
                            this.estadoComercialSalesForce = element['estadoComercialSalesForce'];
                          }
                          let VivSalesForce = new datosViviendaSalesForce();
                          VivSalesForce.ID = element['idInmueble'];
                          VivSalesForce.NAME_INMUEBLE = element['nameInmueble'];
                          VivSalesForce.PVP = element['pvp'];
                          VivSalesForce.NOMBRE_PROMOCION = element['nombrePromocion'];
                          VivSalesForce.ESTADO_COMERCIAL = element['estadoComercial'];
                          VivSalesForce.ESTADO_COMERCIAL_SALESFORCE = element['estadoComercialSalesForce'];
                          this.Viviendassalesforce.push(VivSalesForce);
                        });           
                      },
                      error => {
                        console.log(error, { depth: null});
                      }
                    );
                  },
                  error => {
                    console.log(error, { depth: null});
                  }
                );  
    
              }
            },
            error => {
              console.log(error, { depth: null});
            }
          );
    
          localStorage.setItem('userSalesForce', '');
          localStorage.setItem('emailuserSalesForce', '');
          localStorage.setItem('areaprivadaSalesForce', '');
          localStorage.setItem('IdSalesForce', '');
          localStorage.setItem('productCode', '');
        }
        else
        {
          this.route.queryParams.subscribe(params => {
            this.router.navigate(['/promociones'], { replaceUrl: true });
          });
        }
      }
      if(this.usuarioAdmin.toLowerCase() == 'notario' || this.usuarioAdmin.toLowerCase() == 'portales'|| this.usuarioAdmin.toLowerCase() == 'marketing')
      {
        this.router.navigateByUrl('/login');
      }

    },
    error => {
      console.log('Error GORDO');
    }
  );





    
  }

  volver() {
    this.route.queryParams.subscribe(params => {
      this.router.navigate(['/promociones'], { replaceUrl: true });
    });
  }

  Updateuser() {


    this.wsService.getUserAdminAreaPrivada(this.emailSalesForce).subscribe(
      data => {

        if(data != 0)
        {
            this.wsService.updateuserSaleForceAccount(this.idSalesForce).subscribe(
              data => {
                if(data == true)
                {
                  alert('Actualización del usuario '+this.idSalesForce+' correcta en SalesForce');
                }
                else
                {
                  alert('Se produjo un error al actualizar el campo en SalesForce');
                }
              },
              error => {
                console.log(error, { depth: null});
              }
            );                 
        }    
        
      },
      error => {
        console.log(error, { depth: null});
      }
    ); 



    // let id = this.IdInmueble; //(<HTMLInputElement>document.getElementById('idUserSalesForce')).value;
    // let name = 'Escriturada'; //(<HTMLInputElement>document.getElementById('nameUserSalesForce')).value;



    // if(this.estadoComercialSalesForce === 'Escriturada')
    // {
    //   name = 'Reserva';
    // }
    // else
    // {
    //   name = 'Escriturada';
    // }

    // console.log('El estado original es: '+this.estadoComercialSalesForce);

    
    // this.wsService.getUsuariosOportunidad(this.idOportunidad).subscribe(
    //   data => {
    //     let datosinmuebles = JSON.stringify(data['result']);
    //     this.codUsersOportunidad = '';
    //     console.log(datosinmuebles);
    //     if(data['result'].length === 1)
    //     {
    //       this.codUsersOportunidad += "'"+data['result'][0]['cliente']+"'";
    //     }
    //     else
    //     {
    //       this.codUsersOportunidad += "'"+data['result'][0]['cliente']+"','"+data['result'][1]['cliente']+"'";
    //     }
        

    //         this.wsService.getNombreUsersSalesForce(this.codUsersOportunidad).subscribe(
    //         data => {
    //           this.nameUsersOportunidad = "";                      
    //           data['result'].forEach((element, index) => {                     
    //               //INCLUIREMOS LOS NOMBRES DE LOS USUARIOS
    //               this.nameUsersOportunidad += element['name'] + '\n';
    //           });

    //           var opcion = confirm("Se procederá a Dar de alta en Área Privada a: \n"+this.nameUsersOportunidad);
    //           if (opcion == true) {
    //             let uploadFile = document.getElementById("actualizando");
    //             uploadFile.style.display = "block";
    //             let cargando = document.getElementById("datosSalesForce");
    //             cargando.style.display = "none";
    //             this.wsService.updateuserSaleForce(id, name).subscribe(
    //               data => {
    //                 if(data == true)
    //                 {
    //                   setTimeout( () => { 
                        
    //                     this.wsService.updateuserSaleForce(id, this.estadoComercialSalesForce).subscribe(
    //                       data => {
    //                         if(data == true)
    //                         {
    //                           alert('Actualización correcta en SalesForce');
    //                           this.route.queryParams.subscribe(params => {
    //                             this.router.navigate(['/promociones'], { replaceUrl: true });
    //                           });
    //                         }
    //                         else
    //                         {
    //                           alert('Se produjo un error al actualizar el campo en SalesForce Fase 2');
    //                         }
    //                       },
    //                       error => {
    //                         console.log(error, { depth: null});
    //                       }
    //                     );
                      
    //                   }, 5000 );
    //                 }
    //                 else
    //                 {
    //                   alert('Se produjo un error al actualizar el campo en SalesForce Fase 1');
    //                 }
    //               },
    //               error => {
    //                 console.log(error, { depth: null});
    //               }
    //             );
            
    //           }            

    //         },
    //         error => {
    //           console.log(error, { depth: null});
    //         }
    //       );
        
    //   },
    //   error => {
    //     console.log(error, { depth: null});
    //   }
    // );


   
    
    
    
    
    // this.wsService.updateuserSaleForce(id, name).subscribe(
    //   data => {
    //     if(data == true)
    //     {
    //       setTimeout( () => { 
            
    //         this.wsService.updateuserSaleForce(id, this.estadoComercialSalesForce).subscribe(
    //           data => {
    //             if(data == true)
    //             {
    //               alert('Actualización correcta en SalesForce');
    //               this.route.queryParams.subscribe(params => {
    //                 this.router.navigate(['/promociones'], { replaceUrl: true });
    //               });
    //             }
    //             else
    //             {
    //               alert('Se produjo un error al actualizar el campo en SalesForce Fase 2');
    //             }
    //           },
    //           error => {
    //             console.log(error, { depth: null});
    //           }
    //         );
          
    //       }, 5000 );
    //     }
    //     else
    //     {
    //       alert('Se produjo un error al actualizar el campo en SalesForce Fase 1');
    //     }
    //   },
    //   error => {
    //     console.log(error, { depth: null});
    //   }
    // );

  }


}

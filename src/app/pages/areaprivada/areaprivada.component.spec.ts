import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaPrivadaComponent } from './areaprivada.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('AreaPrivadaComponent', () => {
  let component: AreaPrivadaComponent;
  let fixture: ComponentFixture<AreaPrivadaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgbModule],
      declarations: [ AreaPrivadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaPrivadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

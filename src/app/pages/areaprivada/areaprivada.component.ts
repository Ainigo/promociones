import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Categoria} from '../../classes/categoria';
import { ProductosService } from 'src/app/services/productos.service';
import { WebsocketService } from '../../services/websocket.service';
import { finalize } from 'rxjs/operators';
import { Promociones} from '../../classes/promociones';
import { datosPropietario} from '../../classes/datosPropietario';
import { AuthenticationService } from '../../core';
import * as $ from 'jquery';
import { datosAnejos } from 'src/app/classes/datosAnejos';
import { datosVivienda } from 'src/app/classes/datosVivienda';
import { AppSettings } from '../../shared/app-settings';
import { datosPromociones } from 'src/app/classes/datosPromociones';
import { datosPromocionesSalesforce } from 'src/app/classes/datosPromocionesSalesforce';
import { datosUsuariosPromocionesSalesForce } from 'src/app/classes/datosUsuariosPromocionesSalesForce';
import {datosUsuariosEmailSalesForce} from 'src/app/classes/datosUsuariosEmailSalesForce';
import {datosUsuariosSalesForceAreaPrivada} from 'src/app/classes/datosUsuariosSalesForceAreaPrivada';



declare var html2canvas: any;
declare var jsPDF: any;
declare var req: any;


@Component({
  selector: 'app-areaprivada',
  templateUrl: './areaprivada.component.html',
  styleUrls: ['./areaprivada.component.css']
})



export class AreaPrivadaComponent implements OnInit {

  public promociones: datosPromociones [] = [];
  public viviendas: string [] = [];
  public Viviendasanejos: Promociones [] = [];
  public Viviendaanejosdownload: Promociones [] = [];
  public PromocionesID: string;
  public nombreUsuario: string;
  public arrayViviendas: string [] = [];
  public cambios: string [] = [];
  public usuario: datosPropietario;
  public anejos: datosAnejos;
  public datosViviendas: datosVivienda;
  public datosViendaArray: datosVivienda [] = [];
  public datosPromocionesArray: datosPromocionesSalesforce [] = [];
  public datosUsuariosPromociones: datosUsuariosPromocionesSalesForce [] = [];
  public datosIdsPromociones: datosUsuariosPromocionesSalesForce [] = [];
  public emailsUsers: datosUsuariosEmailSalesForce [] = [];
  public emailsAreaPrivada: datosUsuariosSalesForceAreaPrivada [] = [];
  public p: number = 1;
  public valuesAndalucia: string = "a0P0Y000002uZ20UAE','a0P0Y000001bhPbUAI','a0P0Y000004mhg3UAA','a0P0Y00000Ghlc9UAB','a0P0Y00000hNDBhUAO','a0P0Y00000RfoLzUAJ','a0P0Y00000kjUyWUAU','a0P0Y00000oCSLrUAO', 'a0P1v00000qsSHvEAM','a0P1v00000qstoOEAQ','a0P1v00000qILFYEA4','a0P1v00000qG5JxEAK','a0P1v00000rD4PcEAK','a0P1v00000rxE0JEAU','a0P1v00000sinB4EAI','a0P1v00000sinC2EAI','a0P1v00000sinClEAI','a0P1v00000sinD5EAI','a0P1v00000sypJFEAY','a0P1v00000tCleDEAS','a0P1v00000tKMxsEAG','a0P1v00000tOKgtEAG','a0P1v00000v14snEAA','a0P1v00000v9YZiEAM";



  constructor(private router: Router, public wsService: WebsocketService, public authenticationService: AuthenticationService,private route: ActivatedRoute) { }
  public usuarioAdmin;
  ngOnInit() {

    const credentials = this.authenticationService.credentials;
    const nombreUsuario = credentials.username;
    console.log('El nombre de usuario es: ' + nombreUsuario);
    console.log('vamos a mirar el perfil');


    this.wsService.getUserApp(nombreUsuario)
  .pipe(
    finalize(() => {
    })
  )
  .subscribe(
    result => {
      let valor = JSON.stringify(result);
      let valor2 = JSON.parse(valor);
      console.log('Vamos a la obtención del perfil' +valor2["result"] );
      this.usuarioAdmin = valor2["result"];
      console.log('El resultado es: '+this.usuarioAdmin);
      if(this.usuarioAdmin.toLowerCase() == 'admin')
      {
        let promocion = new datosPromocionesSalesforce();
        promocion.IdPromocionSalesForce = ' ';
        promocion.NamePromocionSalesForce = 'Elija una opción';
        this.datosPromocionesArray.push(promocion);
    
        this.getPromotions();
      }
      if(this.usuarioAdmin.toLowerCase() == 'user')
      {

        let promocion = new datosPromocionesSalesforce();
        promocion.IdPromocionSalesForce = ' ';
        promocion.NamePromocionSalesForce = 'Elija una opción';
        this.datosPromocionesArray.push(promocion);
    
        this.getPromotions();

      }
      if(this.usuarioAdmin.toLowerCase() == 'notario' || this.usuarioAdmin.toLowerCase() == 'portales' || this.usuarioAdmin.toLowerCase() == 'marketing')
      {       
        this.router.navigateByUrl('/login');
      }

    },
    error => {
      console.log('Error GORDO');
    }
  );

  }

  public getPromotions(): void {

    this.wsService.getListaPromociones().subscribe(
      data => {
        let datosinmuebles = JSON.stringify(data['result']);
        
        data['result'].forEach((element, index) => {
          let VivSalesForce = new datosPromocionesSalesforce();
          VivSalesForce.IdPromocionSalesForce = element['id'];
          VivSalesForce.NamePromocionSalesForce = element['name'];
          VivSalesForce.Id_Prinex = element['IdentificadorPrinex__c'];
          this.datosPromocionesArray.push(VivSalesForce);
        });
        
        
      },
      error => {
        console.log(error, { depth: null});
      }
    );

  }

  public getViviendas(): void {

    this.PromocionesID = (<HTMLSelectElement>document.getElementById('selectpromociones')).value;
    this.viviendas = [];
    if(this.PromocionesID === 'Elija una opción'){
      this.viviendas.push('Elija una opción');
    }
    else
    {
      this.wsService.getViviendas(this.PromocionesID)
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      result => {
        console.log(result);
        console.log(result.length);
        this.viviendas.push('Elija una opción');

        for(let i = 0; i < result.length; i++) {
          this.viviendas.push(result[i]['UHEDIT']);
        }

      },
      error => {
        console.log('Error');
      }
    );
    }
  }

  public getPromocionSeleccionada(): void {
    this.PromocionesID = (<HTMLSelectElement>document.getElementById('selectpromociones')).value;
    document.getElementById('datosCompradores').style.display = 'none';
    document.getElementById('Viviedas').style.display = 'none';
    let uploadFile = document.getElementById("actualizando");
    let buttonToUpload = document.getElementById("DarAltaUsuarios");
    buttonToUpload.style.display = 'none';
    let cabecera = document.getElementById("datosCompradores");
    cabecera.innerHTML = 'DATOS DE LOS USUARIOS NO REGISTRADOS';

    
    this.emailsUsers = [];
    this.datosUsuariosPromociones = [];
    this.datosIdsPromociones = [];

    if(this.PromocionesID == ' ')
    {
      alert('Debes seleccionar una promoción');
    }
    else
    {
      uploadFile.style.display = "inline";
      this.emailsUsers = [];
      this.datosUsuariosPromociones = [];
      this.wsService.getListaOportunidadesGanadas(this.PromocionesID).subscribe(
        data => {
          let datosusuarios = JSON.stringify(data['result']);
          
          data['result'].forEach((element, index) => {
            let UserVivSalesForce = new datosUsuariosPromocionesSalesForce();
            UserVivSalesForce.Id = element['id'];
            UserVivSalesForce.AccountId = element['account_id'];
            console.log("UserVivSalesForce: "+element['account_id']);
            this.datosUsuariosPromociones.push(UserVivSalesForce);
          });
          if(this.datosUsuariosPromociones.length == 0)
          {
            alert('No existen datos para esa promoción');
            uploadFile.style.display = "none";
          }
          else
          {
            let i = 0;            
            this.datosUsuariosPromociones.forEach((element, index) =>{
              console.log(element['AccountId']);
              console.log(element['Id']);
              

              this.wsService.getIDUserOportunidades(element['Id']).subscribe(
                data => {
                  if(data['result'] != 0)
                  {
                    this.datosIdsPromociones = [];
                    data['result'].forEach((element, index) => {
                      let objectIdsUsers = new datosUsuariosPromocionesSalesForce();
                      
                      objectIdsUsers.Id = element['id'];
                      this.datosIdsPromociones.push(objectIdsUsers);
                    });                    
                  }
                  this.datosIdsPromociones.forEach((element, index) =>{
                    this.wsService.getEmailUsuarioOportunidades(element['Id']).subscribe(
                      data => {
                        let datosusuariosemails = JSON.stringify(data['result']);
        
                        if(data['result'] != 0)
                        {
                          buttonToUpload.style.display = 'inline';
                          data['result'].forEach((element, index) => {
                            let objectEmailUsers = new datosUsuariosEmailSalesForce();
                            
                            objectEmailUsers.Id = element['id'];
                            objectEmailUsers.Email = element['email'];
                            objectEmailUsers.Registrado = element['registrado'];
                           

                            this.emailsUsers.push(objectEmailUsers);
                          });                    
                        }
                        i++;
                        console.log(i);
                        if(i == this.datosUsuariosPromociones.length)
                        {
                          let valores:any = document.getElementsByClassName('idSalesForce');
                          console.log("El número de usuarios sin dar de alta es: "+this.datosUsuariosPromociones.length);
                          
                          document.getElementById('datosCompradores').style.display = 'table-cell'
                          uploadFile.style.display = "none";
                          alert("Finalizó el proceso de búsqueda");
                          document.getElementById('Viviedas').style.display = 'inline-table';
                          document.getElementById('datosCompradores').style.display = 'table-cell'
                          if(valores.length !== 0)
                            buttonToUpload.style.display = 'inline';
                        } 
                      },
                      error => {
                        console.log(error, { depth: null});
                      }
                    );
                  })
                },
                error => {
                  console.log(error, { depth: null});
                }
              );
            }) 
          }          
        },
        error => {
          console.log(error, { depth: null});
        }
      );  
    }

  }

  public getUsuariosAlta(): void {
    let buttonToUpload = document.getElementById("DarAltaUsuarios");
    buttonToUpload.style.display = 'none';
    this.PromocionesID = (<HTMLSelectElement>document.getElementById('selectpromociones')).value;
    document.getElementById('datosCompradores').style.display = 'none';
    document.getElementById('Viviedas').style.display = 'none';
    let uploadFile = document.getElementById("actualizando");


    let cabecera = document.getElementById("datosCompradores");
    cabecera.innerHTML = 'DATOS DE LOS USUARIOS DADOS DE ALTA';

    
    this.emailsUsers = [];
    this.datosUsuariosPromociones = [];
    this.datosIdsPromociones = [];

    if(this.PromocionesID == ' ')
    {
      alert('Debes seleccionar una promoción');
    }
    else
    {
      uploadFile.style.display = "inline";
      this.emailsUsers = [];
      this.datosUsuariosPromociones = [];
      this.wsService.getListaOportunidadesGanadas(this.PromocionesID).subscribe(
        data => {
          let datosusuarios = JSON.stringify(data['result']);
          
          data['result'].forEach((element, index) => {
            let UserVivSalesForce = new datosUsuariosPromocionesSalesForce();
            UserVivSalesForce.Id = element['id'];
            UserVivSalesForce.AccountId = element['account_id'];
            console.log("UserVivSalesForce: "+element['account_id']);
            this.datosUsuariosPromociones.push(UserVivSalesForce);
          });
          if(this.datosUsuariosPromociones.length == 0)
          {
            alert('No existen datos para esa promoción');
            uploadFile.style.display = "none";
          }
          else
          {
            let i = 0;            
            this.datosUsuariosPromociones.forEach((element, index) =>{
              console.log(element['AccountId']);
              console.log(element['Id']);
              

              this.wsService.getIDUserOportunidades(element['Id']).subscribe(
                data => {
                  if(data['result'] != 0)
                  {
                    this.datosIdsPromociones = [];
                    data['result'].forEach((element, index) => {
                      let objectIdsUsers = new datosUsuariosPromocionesSalesForce();
                      
                      objectIdsUsers.Id = element['id'];
                      this.datosIdsPromociones.push(objectIdsUsers);
                    });                    
                  }
                  this.datosIdsPromociones.forEach((element, index) =>{
                    this.wsService.getEmailUsuarioAltaOportunidades(element['Id']).subscribe(
                      data => {
                        let datosusuariosemails = JSON.stringify(data['result']);
                        if(data['result'] != 0)
                        {
                          
                          data['result'].forEach((element, index) => {
                            let objectEmailUsers = new datosUsuariosEmailSalesForce();
                            
                            objectEmailUsers.Id = element['id'];
                            objectEmailUsers.Email = element['email'];
                            objectEmailUsers.Registrado = element['registrado'];
                            var a = new Date(element['fecha']);
                            var dd = a.getDate(); 
                            var mm = a.getMonth()+1; 
                            var yyyy = a.getFullYear();                
                            var today = dd+'/'+mm+'/'+yyyy;
                            objectEmailUsers.FechaAlta = today;
                            this.emailsUsers.push(objectEmailUsers);
                          }); 
                          cabecera.innerHTML = 'DATOS DE LOS USUARIOS DADOS DE ALTA. TOTAL: '+this.emailsUsers.length;                   
                        }
                        i++;
                        console.log(i);
                        if(i == this.datosUsuariosPromociones.length)
                        {
                          let valores:any = document.getElementsByClassName('idSalesForce');
                          console.log("El número de usuarios sin dar de alta es: "+this.datosUsuariosPromociones.length);
                          
                          document.getElementById('datosCompradores').style.display = 'table-cell'
                          uploadFile.style.display = "none";
                          alert("Finalizó el proceso de búsqueda");
                          document.getElementById('Viviedas').style.display = 'inline-table';
                          document.getElementById('datosCompradores').style.display = 'table-cell'
                          // if(valores.length !== 0)
                          //   buttonToUpload.style.display = 'inline';
                        } 
                      },
                      error => {
                        console.log(error, { depth: null});
                      }
                    );
                  })
                },
                error => {
                  console.log(error, { depth: null});
                }
              );
            }) 
          }          
        },
        error => {
          console.log(error, { depth: null});
        }
      );  
    }

  }

  public getPromocionSeleccionadaAndalucia(): void {
    this.PromocionesID = this.valuesAndalucia;
    document.getElementById('datosCompradores').style.display = 'none';
    document.getElementById('Viviedas').style.display = 'none';
    let uploadFile = document.getElementById("actualizando");
    let buttonToUpload = document.getElementById("DarAltaUsuarios");
    buttonToUpload.style.display = 'none';
    uploadFile.style.display = "inline";
    
    this.emailsUsers = [];
    this.datosUsuariosPromociones = [];
     
      this.emailsUsers = [];
      this.datosUsuariosPromociones = [];
      this.wsService.getListaOportunidadesGanadas(this.PromocionesID).subscribe(
        data => {
          let datosusuarios = JSON.stringify(data['result']);        
          data['result'].forEach((element, index) => {
            let UserVivSalesForce = new datosUsuariosPromocionesSalesForce();
            UserVivSalesForce.Id = element['id'];
            UserVivSalesForce.AccountId = element['account_id'];
            console.log("UserVivSalesForce: "+element['account_id']);
            this.datosUsuariosPromociones.push(UserVivSalesForce);
          });
          if(this.datosUsuariosPromociones.length == 0)
          {
            alert('No existen datos para esa promoción');
          }
          else
          {
            let i = 0;
            this.datosUsuariosPromociones.forEach((element, index) =>{
              this.wsService.getEmailUsuarioOportunidades(element['AccountId']).subscribe(
                data => {
                  let datosusuariosemails = JSON.stringify(data['result']);
                  if(data['result'] != 0)
                  {
                    data['result'].forEach((element, index) => {
                      let objectEmailUsers = new datosUsuariosEmailSalesForce();
    
                      objectEmailUsers.Id = element['id'];
                      objectEmailUsers.Email = element['email'];
                      objectEmailUsers.Registrado = element['registrado'];
                      this.emailsUsers.push(objectEmailUsers);
                    });                    
                  }
                  i++;
                  console.log(i);
                  if(i == this.datosUsuariosPromociones.length)
                  {
                    console.log("El número de usuarios en Andalucía sin dar de alta es: "+this.datosUsuariosPromociones.length);
                    document.getElementById('datosCompradores').style.display = 'table-cell'
                    uploadFile.style.display = "none";
                    alert("Finalizó el proceso de búsqueda");
                    document.getElementById('Viviedas').style.display = 'inline-table';
                    if(this.emailsUsers.length == 0)
                      alert("No existen elementos nuevos");
                    else
                      buttonToUpload.style.display = 'inline';
                  }    
                  
                },
                error => {
                  console.log(error, { depth: null});
                }
              );
  
            })
           
          }
          
        },
        error => {
          console.log(error, { depth: null});
        }
      );  

  }


  public getPromocionSeleccionadaTodos(): void {
    this.PromocionesID = this.valuesAndalucia;
    document.getElementById('datosCompradores').style.display = 'none';
    let uploadFile = document.getElementById("actualizando");
    let buttonToUpload = document.getElementById("DarAltaUsuarios");
    buttonToUpload.style.display = 'none';
    uploadFile.style.display = "inline";
    
    this.emailsUsers = [];
    this.datosUsuariosPromociones = [];
     
      this.emailsUsers = [];
      this.datosUsuariosPromociones = [];
      this.wsService.getListaAllOportunidadesGanadas().subscribe(
        data => {
          let datosusuarios = JSON.stringify(data['result']);        
          data['result'].forEach((element, index) => {
            let UserVivSalesForce = new datosUsuariosPromocionesSalesForce();
            UserVivSalesForce.Id = element['id'];
            UserVivSalesForce.AccountId = element['account_id'];
            console.log("UserVivSalesForce: "+element['account_id']);
            this.datosUsuariosPromociones.push(UserVivSalesForce);
          });
          if(this.datosUsuariosPromociones.length == 0)
          {
            alert('No existen datos para esa promoción');
          }
          else
          {
            let i = 0;
            alert(this.datosUsuariosPromociones.length);
            this.datosUsuariosPromociones.forEach((element, index) =>{
              this.wsService.getEmailUsuarioOportunidades(element['AccountId']).subscribe(
                data => {
                  let datosusuariosemails = JSON.stringify(data['result']);
                  if(data['result'] != 0)
                  {
                    data['result'].forEach((element, index) => {
                      let objectEmailUsers = new datosUsuariosEmailSalesForce();
    
                      objectEmailUsers.Id = element['id'];
                      objectEmailUsers.Email = element['email'];
                      objectEmailUsers.Registrado = element['registrado'];
                      this.emailsUsers.push(objectEmailUsers);
                    });                    
                  }
                  i++;
                  console.log(i);
                  if(i == this.datosUsuariosPromociones.length)
                  {
                    document.getElementById('datosCompradores').style.display = 'table-cell'
                    uploadFile.style.display = "none";
                    alert("Finalizó el proceso de búsqueda");
                    if(this.emailsUsers.length == 0)
                      alert("No existen elementos nuevos");
                    else
                      buttonToUpload.style.display = 'inline';
                  }    
                  
                },
                error => {
                  console.log(error, { depth: null});
                }
              );
  
            })           
          }          
        },
        error => {
          console.log(error, { depth: null});
        }
      );
  }


  public Updateuser(): void{

    this.cargaUsuariosTabla();    

    for(let z = 0; z < this.emailsAreaPrivada.length; z++){
         if(this.emailsAreaPrivada[z].Email == " ")
         {
           alert("El usuario con id "+this.emailsAreaPrivada[z].Id+" no tiene email, por lo tanto no puede ser dado de alta en el área privada");
         }
         else
         {
              //----------------

              this.wsService.getcheckEmail(this.emailsAreaPrivada[z].Email).subscribe(
                data => {
  
                  if(data == true)
                  {
                    console.log('El usuario: ' +this.emailsAreaPrivada[z].Email+ ' tiene un email válido');
                    this.wsService.getUserAdminAreaPrivada(this.emailsAreaPrivada[z].Email).subscribe(
                      data => {
                        if(data == false)
                        {
                          alert("El usuario "+this.emailsAreaPrivada[z].Id+" no se pudo dar de alta en Magnolia o en Active Directory")
                        }
                        else
                        {
                          if(data != 0)
                          {
                              this.wsService.updateuserSaleForceAccount(this.emailsAreaPrivada[z].Id).subscribe(
                                data => {
                                  if(data == true)
                                  {
                                    alert('Actualización del usuario '+this.emailsAreaPrivada[z].Id+' correcta en SalesForce');
                                  }
                                  else
                                  {
                                    alert('Se produjo un error al actualizar el campo en SalesForce');
                                  }
                                },
                                error => {
                                  alert('Error al dar de alta a los usuarios en el área privada')
                                  console.log(error, { depth: null});
                                }
                              );                 
                          }  
                        }
                          
                        
                      },
                      error => {
                        alert('Error al gestionar el alta a los usuarios en el área privada')
                        console.log(error, { depth: null});
                      }
                    );
                  }
                  else
                  {
                    console.log('El usuario: ' +this.emailsAreaPrivada[z].Email+ ' no tiene un email válido');
                  }
                  
                },
                error => {
                  alert('Error al gestionar el alta a los usuarios en el área privada')
                  console.log(error, { depth: null});
                }
              ); 
            

          
              //----------------
 
          }
    }

  }

  public cargaUsuariosTabla() {
    let valores:any = document.getElementsByClassName('idSalesForce');
    let valores2:any = document.getElementsByClassName('emailsSalesForce');
    this.emailsAreaPrivada = [];

    for(let x of valores2){
      var user = new datosUsuariosSalesForceAreaPrivada();
      user.Email = x.value;
      this.emailsAreaPrivada.push(user);
    }

    for(let y = 0; y < valores.length; y++){
      this.emailsAreaPrivada[y].Id = valores[y].value;
    }


  }





}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Categoria} from '../../classes/categoria';
import { ProductosService } from 'src/app/services/productos.service';
import { WebsocketService } from '../../services/websocket.service';
import { finalize } from 'rxjs/operators';
import { Promociones} from '../../classes/promociones';
import { Usuariosnuevos} from '../../classes/usuariosnuevos';


import { datosPropietario} from '../../classes/datosPropietario';
import { AuthenticationService } from '../../core';
import * as $ from 'jquery';
import { datosAnejos } from 'src/app/classes/datosAnejos';
import { datosVivienda } from 'src/app/classes/datosVivienda';
import { AppSettings } from '../../shared/app-settings';
import { datosPromociones } from 'src/app/classes/datosPromociones';
import { convertActionBinding, ConvertActionBindingResult } from '@angular/compiler/src/compiler_util/expression_converter';
import { usuariosSalesForce } from 'src/app/classes/usuariosSalesForce';

declare var html2canvas: any;
declare var jsPDF: any;
declare var req: any;


@Component({
  selector: 'app-clientesnuevossalesforce',
  templateUrl: './clientesnuevossalesforce.component.html',
  styleUrls: ['./clientesnuevossalesforce.component.css']
})



export class ClientesNuevosSalesForceComponent implements OnInit {

  public promociones: datosPromociones [] = [];
  public viviendas: string [] = [];
  public Viviendasanejos: Promociones [] = [];
  public Viviendaanejosdownload: Promociones [] = [];
  public Arrayusuarios: Usuariosnuevos [] = [];
  public PromocionesID: string;
  public FechaInicial: string;
  public FechaFinal: string;
  public nombreUsuario: string;
  public arrayViviendas: string [] = [];
  public cambios: string [] = [];
  public usuario: datosPropietario;
  public anejos: datosAnejos;
  public datosViviendas: datosVivienda;
  public datosViendaArray: datosVivienda [] = [];
  public p: number = 1;
  public nuevosUsuariosSalesForce : usuariosSalesForce [] = []



  constructor(private router: Router, public wsService: WebsocketService, public authenticationService: AuthenticationService,private route: ActivatedRoute) { }
  public usuarioAdmin;
  ngOnInit() {
    
    const credentials = this.authenticationService.credentials;

    const nombreUsuario = credentials.username;
    console.log('El nombre de usuario es: ' + nombreUsuario);
    console.log('vamos a mirar el perfil');


  this.wsService.getUserApp(nombreUsuario)
  .pipe(
    finalize(() => {
    })
  )
  .subscribe(
    result => {
      let valor = JSON.stringify(result);
      let valor2 = JSON.parse(valor);
      console.log('Vamos a la obtención del perfil' +valor2["result"] );
      this.usuarioAdmin = valor2["result"];
      console.log('El resultado es: '+this.usuarioAdmin);
      if(this.usuarioAdmin == 'admin')
      {
        
      }
      if(this.usuarioAdmin.toLowerCase() == 'user')
      {
       
      }
      if(this.usuarioAdmin== 'notario')
      {
        this.router.navigateByUrl('/login');
      }
      if(this.usuarioAdmin== 'portales')
      {
        this.router.navigateByUrl('/login');
      }
      if(this.usuarioAdmin== 'marketing')
      {
        this.router.navigateByUrl('/login');
      }

    },
    error => {
      console.log('Error Clientes Nuevos');
    }
  );

    

  }

  public getUsuarios(): void {
    console.log("Primer Paso");
    this.nuevosUsuariosSalesForce = [];
    this.Arrayusuarios = [];
    
    console.log("Segundo Paso");

    this.FechaInicial = (<HTMLInputElement>document.getElementById('initialcalendar')).value;
    this.FechaFinal = (<HTMLInputElement>document.getElementById('finishcalendar')).value;

    console.log("Tercer Paso");
    let cabecera = document.getElementById("datosCompradores");

    

    if(this.FechaInicial != "" && this.FechaFinal != "")
    {
      this.wsService.getUsersSalesForce(this.FechaInicial,this.FechaFinal).subscribe(
        data => {
          if(data.length != 0)
          {
            this.nuevosUsuariosSalesForce = [];
            data.forEach((element, index) => {
              let objectUsersSalesForce = new usuariosSalesForce();         
              objectUsersSalesForce.Id = element['id'];
              objectUsersSalesForce.Email = element['email'];
              objectUsersSalesForce.Name = element['name'];
              this.nuevosUsuariosSalesForce.push(objectUsersSalesForce);
            });
            console.log(this.nuevosUsuariosSalesForce.length);
            cabecera.innerHTML = 'DATOS DE LOS USUARIOS: '+this.nuevosUsuariosSalesForce.length; 
            document.getElementById('Viviedas').style.display = 'inline-table';                 
          }
        },
        error => {
          console.log(error, { depth: null});
        }
      );
    }
    else
    {
      alert("Debe seleccionar una fecha inicial y una fecha final para mostrar los datos de los usuarios");
    }
  }


  public exportCSVTOTAL() {


    var cabecera = ["CODIGO_PRINEX", "NOMBRE", "APELLIDO", "DNI", "FIJO", "PROFESION", "MOVIL", "EMAIL", "POBLACION", "PROVINCIA", "CODIGO_POSTAL", "TIPO_VIA", "NOMBRE_VIA", "NUMERO_VIA", "ESCALERA", "PISO", "PUERTA", "ESTADO_CIVIL", "AÑO", "MES", "DIA", "F_ALTA", "H_ALTA", "TIPO", "CODPAIS", "PAIS", "NOMBRE_VIVIENDA", "PROMOCION", "NETO_VIVIENDA", "METROS_CONS", "METROS_UTILES", "EUROS_METRO_CONS", "EUROS_METRO_UTIL", "ACTO", "VIVIENDA", "CONO", "CESTA", "ESTADO_VIVIENDA"];



    this.FechaInicial = (<HTMLInputElement>document.getElementById('initialcalendar')).value;
    this.FechaFinal = (<HTMLInputElement>document.getElementById('finishcalendar')).value;

    if(this.FechaInicial != "" && this.FechaFinal != "")
    {
      let selectVivienda = AppSettings.SELECT_USUARIOS_COMPLETA(this.FechaInicial, this.FechaFinal);
                  this.wsService.getViviendasCompradores(selectVivienda)
                  .pipe(
                    finalize(() => {
                    })
                  )
                  .subscribe(
                    resultviviendas => {
                      const jsonData = JSON.parse(JSON.stringify(resultviviendas));
                      const jsonObject = JSON.stringify(jsonData);
                      const csv = this.convertToCSV(jsonObject, cabecera);

                      const exportName = "DatosGlobales.csv" || "exportDatosGlobales.csv";

                      const blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
                      if (navigator.msSaveBlob) {
                        navigator.msSaveBlob(blob, exportName);
                      } else {
                        const link = document.createElement("a");
                        if (link.download !== undefined) {
                        const url = URL.createObjectURL(blob);
                        link.setAttribute("href", url);
                        link.setAttribute("download", exportName);
                        link.style.visibility = "hidden";
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                        }
                      }
                        
                    },
                    error => {
                      console.log('Error Obtención de los datos globales');
                    }
                  );
    }
    else
    {
      alert("Debe seleccionar una fecha inicial y una fecha final para mostrar los datos de los usuarios");
    }
  }

  public exportCSVUSUARIOS() {

    var cabecera = ["CODIGO_PRINEX", "NOMBRE", "APELLIDO", "DNI", "FIJO", "PROFESION", "MOVIL", "EMAIL", "POBLACION", "PROVINCIA", "CODIGO_POSTAL", "TIPO_VIA", "NOMBRE_VIA", "NUMERO_VIA", "ESCALERA", "PISO", "PUERTA", "ESTADO_CIVIL", "AÑO", "MES", "DIA", "F_ALTA", "H_ALTA", "TIPO", "CODPAIS", "PAIS"];

    this.FechaInicial = (<HTMLInputElement>document.getElementById('initialcalendar')).value;
    this.FechaFinal = (<HTMLInputElement>document.getElementById('finishcalendar')).value;

    if(this.FechaInicial != "" && this.FechaFinal != "")
    {
      let selectVivienda = AppSettings.SELECT_USUARIOS(this.FechaInicial, this.FechaFinal);
                  this.wsService.getViviendasCompradores(selectVivienda)
                  .pipe(
                    finalize(() => {
                    })
                  )
                  .subscribe(
                    resultviviendas => {
                      const jsonData = JSON.parse(JSON.stringify(resultviviendas));

                      const jsonObject = JSON.stringify(jsonData);
                      const csv = this.convertToCSV(jsonObject, cabecera);

                      const exportName = "DatosUsuarios.csv" || "exportDatosUsuarios.csv";

                      const blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
                      if (navigator.msSaveBlob) {
                        navigator.msSaveBlob(blob, exportName);
                      } else {
                        const link = document.createElement("a");
                        if (link.download !== undefined) {
                        const url = URL.createObjectURL(blob);
                        link.setAttribute("href", url);
                        link.setAttribute("download", exportName);
                        link.style.visibility = "hidden";
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                        }
                      }
                        
                    },
                    error => {
                      console.log('Error Obtención de los datos usuarios');
                    }
                  );
    }
    else
    {
      alert("Debe seleccionar una fecha inicial y una fecha final para mostrar los datos de los usuarios");
    }
  }




  public convertToCSV(objArray, cabecera) {
  const array = typeof objArray != "object" ? JSON.parse(objArray) : objArray;
  let str = "";


  let line = "";
  for(let j = 0; j < cabecera.length; j++)
  {
     if (line != "") line += ","; 
    line += cabecera[j];
   }
   str += line + "\r\n";
  
 for (let i = 0; i < array.length; i++) {
   let line = "";
   for (let index in array[i]) {
    if (line != "") line += ",";

 line += array[i][index];
   }
 
 str += line + "\r\n";
  }
 
 return str;
  }



}

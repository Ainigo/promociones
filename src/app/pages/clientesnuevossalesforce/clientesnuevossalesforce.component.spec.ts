import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesNuevosSalesForceComponent } from './clientesnuevossalesforce.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('PromocionesComponent', () => {
  let component: ClientesNuevosSalesForceComponent;
  let fixture: ComponentFixture<ClientesNuevosSalesForceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgbModule],
      declarations: [ ClientesNuevosSalesForceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesNuevosSalesForceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

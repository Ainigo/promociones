import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesNuevosComponent } from './clientesnuevos.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('PromocionesComponent', () => {
  let component: ClientesNuevosComponent;
  let fixture: ComponentFixture<ClientesNuevosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgbModule],
      declarations: [ ClientesNuevosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesNuevosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

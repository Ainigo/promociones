import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Categoria} from '../../classes/categoria';
import { ProductosService } from 'src/app/services/productos.service';
import { WebsocketService } from '../../services/websocket.service';
import { finalize } from 'rxjs/operators';
import { Promociones} from '../../classes/promociones';
import { Usuariosnuevos} from '../../classes/usuariosnuevos';
//import { Usuariosnuevos} from '../../classes/usuariosnuevos';

//import {fastcsv} from  '../../../fast-csv';
//import {fs} = require("fs");

import { datosPropietario} from '../../classes/datosPropietario';
import { AuthenticationService } from '../../core';
import * as $ from 'jquery';
import { datosAnejos } from 'src/app/classes/datosAnejos';
import { datosVivienda } from 'src/app/classes/datosVivienda';
import { AppSettings } from '../../shared/app-settings';
import { datosPromociones } from 'src/app/classes/datosPromociones';
import { convertActionBinding, ConvertActionBindingResult } from '@angular/compiler/src/compiler_util/expression_converter';

declare var html2canvas: any;
declare var jsPDF: any;
declare var req: any;


@Component({
  selector: 'app-clientesnuevos',
  templateUrl: './clientesnuevos.component.html',
  styleUrls: ['./clientesnuevos.component.css']
})



export class ClientesNuevosComponent implements OnInit {

  public promociones: datosPromociones [] = [];
  public viviendas: string [] = [];
  public Viviendasanejos: Promociones [] = [];
  public Viviendaanejosdownload: Promociones [] = [];
  public Arrayusuarios: Usuariosnuevos [] = [];
  public PromocionesID: string;
  public FechaInicial: string;
  public FechaFinal: string;
  public nombreUsuario: string;
  public arrayViviendas: string [] = [];
  public cambios: string [] = [];
  public usuario: datosPropietario;
  public anejos: datosAnejos;
  public datosViviendas: datosVivienda;
  public datosViendaArray: datosVivienda [] = [];
  public estadocivil = new Map([['X', 'CASADO'], ['S', 'SOLTERO'], ['D', 'DIVORCIADO'], ['V', 'VIUDO / PAREJA DE HECHO']]);
  public tipovia = new Map([['AD', 'ALDEA'],['AL', 'ALAMEDA'],['AP', 'APARTAMENTO'],['AV', 'AVENIDA'],['BL', 'BLOQUE'],['BO', 'BARRIO'],['CH', 'CHALET'],['CL', 'CALLE'],['CM', 'CAMINO'], ['CO', 'COLONIA'],['CR', 'CARRETERA'],['CS', 'CASERIO'],['CT', 'CUESTA'],['ED', 'EDIFICIO'],['GL', 'GLORIETA'],['GR', 'GRUPO'],['LG', 'LUGAR'],['MC', 'MERCADO'],['MN', 'MUNICIPIO'],['MZ', 'MANZANA'],['PB', 'POBLADO'],['PG', 'POLIGONO'],['PJ', 'PASAJE'],['PQ', 'PARQUE'],['PR', 'PROLONGACION'],['PS', 'PASEO'],['PZ', 'PLAZA'],['RB', 'RAMBLA'],['RD', 'RONDA'],['TR', 'TRAVESIA'],['UR', 'URBANIZACION']]);
  public p: number = 1;



  constructor(private router: Router, public wsService: WebsocketService, public authenticationService: AuthenticationService,private route: ActivatedRoute) { }
  public usuarioAdmin;
  ngOnInit() {
    
    const credentials = this.authenticationService.credentials;

    const nombreUsuario = credentials.username;
    console.log('El nombre de usuario es: ' + nombreUsuario);
    console.log('vamos a mirar el perfil');


  this.wsService.getUserApp(nombreUsuario)
  .pipe(
    finalize(() => {
    })
  )
  .subscribe(
    result => {
      let valor = JSON.stringify(result);
      let valor2 = JSON.parse(valor);
      console.log('Vamos a la obtención del perfil' +valor2["result"] );
      this.usuarioAdmin = valor2["result"];
      console.log('El resultado es: '+this.usuarioAdmin);
      if(this.usuarioAdmin == 'admin')
      {
        // this.viviendas.push('Elija una opción');

        // let promocion = new datosPromociones();
        // promocion.PMNOMP = ' ';
        // promocion.PMPROM = 'Elija una opción';
        // this.promociones.push(promocion);

        // this.getPromotions();


        //   ($('#selectanejos') as any).select2();
        //   $('#checkbox').click(function(){
        //     if($('#checkbox').is(':checked') ) {
        //         $('#selectanejos > option').prop('selected','selected');
        //         $('#selectanejos').trigger('change');
        //     }else{
        //         $('#selectanejos').val(null).trigger('change');
        //     }
        // });
      }
      if(this.usuarioAdmin.toLowerCase() == 'user')
      {
        // this.viviendas.push('Elija una opción');

        // let promocion = new datosPromociones();
        // promocion.PMNOMP = ' ';
        // promocion.PMPROM = 'Elija una opción';
        // this.promociones.push(promocion);

        // this.getPromotions();


        //   ($('#selectanejos') as any).select2();
        //   $('#checkbox').click(function(){
        //     if($('#checkbox').is(':checked') ) {
        //         $('#selectanejos > option').prop('selected','selected');
        //         $('#selectanejos').trigger('change');
        //     }else{
        //         $('#selectanejos').val(null).trigger('change');
        //     }
        // });
      }
      if(this.usuarioAdmin== 'notario')
      {
        this.router.navigateByUrl('/login');
      }
      if(this.usuarioAdmin== 'portales')
      {
        this.router.navigateByUrl('/login');
      }
      if(this.usuarioAdmin== 'marketing')
      {
        this.router.navigateByUrl('/login');
      }

    },
    error => {
      console.log('Error Clientes Nuevos');
    }
  );

    

  }

  public getUsuarios(): void {
    console.log("Primer Paso");
    this.Arrayusuarios = [];
    
    console.log("Segundo Paso");

    this.FechaInicial = (<HTMLInputElement>document.getElementById('initialcalendar')).value;
    this.FechaFinal = (<HTMLInputElement>document.getElementById('finishcalendar')).value;

    console.log("Tercer Paso");

    if(this.FechaInicial != "" && this.FechaFinal != "")
    {
      console.log("Cuarto Paso");
      const usuarioNuevocliente = new Usuariosnuevos();
      
      
      let selectUsuarios = AppSettings.SELECT_USUARIOS(this.FechaInicial, this.FechaFinal);
      console.log("LA SELECT DE USUARIOS ES: "+ selectUsuarios);

      this.wsService.getCompradores(selectUsuarios)
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(
          resultnuevo => {
            console.log("Quinto Paso");           
             
              usuarioNuevocliente.COMPRADORES =  [];
              for(let i = 0; i < resultnuevo.length; i++) {
                console.log(resultnuevo[i]);
                this.usuario = new datosPropietario();
                
                this.usuario.PROMOCION = resultnuevo[i]['PROMOCION'];
                this.usuario.VIVIENDA = resultnuevo[i]['VIVIENDA'];
                this.usuario.CODIGO_PRINEX = resultnuevo[i]['CODIGO_PRINEX'];
                this.usuario.NOMBRE = resultnuevo[i]['NOMBRE'];
                this.usuario.APELLIDOS = resultnuevo[i]['APELLIDO'];
                this.usuario.DNI = resultnuevo[i]['DNI'].trim();
                this.usuario.TELEFONOFIJO = resultnuevo[i]['FIJO'];
                this.usuario.MOVIL = resultnuevo[i]['MOVIL'];
                this.usuario.EMAIL = resultnuevo[i]['EMAIL'];
                this.usuario.PROFESION = resultnuevo[i]['PROFESION'];
                this.usuario.POBLACION = resultnuevo[i]['POBLACION'];
                this.usuario.PROVINCIA = resultnuevo[i]['PROVINCIA'];
                this.usuario.CODIGO_POSTAL = resultnuevo[i]['CODIGO_POSTAL'];
                this.usuario.TIPO_VIA = resultnuevo[i]['TIPO_VIA'];
                this.usuario.NOMBRE_VIA = resultnuevo[i]['NOMBRE_VIA'];
                this.usuario.NUMERO_VIA = resultnuevo[i]['NUMERO_VIA'];
                this.usuario.ESCALERA = resultnuevo[i]['ESCALERA'];
                this.usuario.PISO = resultnuevo[i]['PISO'];
                this.usuario.PUERTA = resultnuevo[i]['PUERTA'];
                this.usuario.ESTADO_CIVIL = resultnuevo[i]['ESTADO_CIVIL'];
                this.usuario.ANIO = resultnuevo[i]['AÑO'];
                this.usuario.MES = resultnuevo[i]['MES'];
                this.usuario.DIA = resultnuevo[i]['DIA'];
                this.usuario.IBAN = resultnuevo[i]['IBAN'];
                this.usuario.FECHALTA = resultnuevo[i]['FALTA'];
                this.usuario.HORALTA = resultnuevo[i]['HALTA'];
                this.usuario.CLTIPO = resultnuevo[i]['CLTIPO'];
                this.usuario.UHPROM = resultnuevo[i]['UHPROM'];
                this.usuario.UHEDIT = resultnuevo[i]['UHEDIT'];
                this.usuario.ACESTA = resultnuevo[i]['ACESTA'];
                this.usuario.PAIS = resultnuevo[i]['PAIS'];
                var a = new Date(resultnuevo[i]['F_ALTA']);
                var dd = a.getDate(); 
                var mm = a.getMonth()+1; 
                var yyyy = a.getFullYear();                
                var today = dd+'/'+mm+'/'+yyyy;
                this.usuario.FECHALTA = today;
                var cltipo = resultnuevo[i]['TIPO'];
                var valortipo;
                if(cltipo = 'F')
                {
                  valortipo = 'Físico';
                }
                else
                  valortipo = 'Jurídico';

                this.usuario.CLTIPO = valortipo;

                usuarioNuevocliente.COMPRADORES.push(this.usuario);
              }
                       
              this.Arrayusuarios.push(usuarioNuevocliente); 
          },
          error => {
            console.log('Error Obtención de los usuarios');
          }
        );
    }
    else
    {
      alert("Debe seleccionar una fecha inicial y una fecha final para mostrar los datos de los usuarios");
    }
  }

  public getDatosInmuebles(codigoprinex: string): void {

    var myEle = document.getElementById(codigoprinex);

    if(myEle == null)
    {
    let selectVivienda = AppSettings.SELECT_VIVIENDA_USUARIOS(codigoprinex);
                this.wsService.getViviendasCompradores(selectVivienda)
                .pipe(
                  finalize(() => {
                  })
                )
                .subscribe(
                  resultviviendas => {
                    console.log(resultviviendas);
                    console.log("EL RESULT NEW ES: "+resultviviendas.length);
                    console.log("Sexto Paso");
                    const divheader = document.createElement('tr');
                    divheader.style.backgroundColor = "#343a40";
                    divheader.style.color = "white";
                    divheader.style.border = "2px solid";                   
                    divheader.innerHTML = '<td style="color: antiquewhite" colspan="6" id='+codigoprinex+'>DATOS DE LOS INMUEBLES &nbsp;&nbsp;&nbsp;&nbsp; PROMOCIÓN: '+resultviviendas[0]['PROMOCION']+'</td>';
                    var target2 = document.getElementById("tabla"+codigoprinex).appendChild(divheader);
 
                      for(let p = 0; p < resultviviendas.length; p++) {

                        
                        this.datosViviendas = new datosVivienda();
                        
                        console.log("Septimo Paso");
                        
                        if(resultviviendas[p]['NETO_VIVIENDA'] !== null && resultviviendas[p]['NETO_VIVIENDA'] !== '')
                        {
                          this.datosViviendas.PRECIO_VIVIENDA = resultviviendas[p]['NETO_VIVIENDA'].toFixed(2);
                        }
                        else
                        {
                          this.datosViviendas.PRECIO_VIVIENDA = resultviviendas[p]['NETO_VIVIENDA'];
                        }

                        if(resultviviendas[p]['METROS_CONS'] !== null && resultviviendas[p]['METROS_CONS'] !== '')
                        {
                          this.datosViviendas.METROS_CONSTRUIDOS = resultviviendas[p]['METROS_CONS'].toFixed(2);
                        }
                        else
                        {
                          this.datosViviendas.METROS_CONSTRUIDOS = resultviviendas[p]['METROS_CONS'];
                        }
                        //this.datosViviendas.METROS_CONSTRUIDOS = resultviviendas[p]['METROS_CONS'];
                        if(resultviviendas[p]['METROS_UTILES'] !== null && resultviviendas[p]['METROS_UTILES'] !== '')
                        {
                          this.datosViviendas.METROS_HABILES = resultviviendas[p]['METROS_UTILES'].toFixed(2);
                        }
                        else
                        {
                          this.datosViviendas.METROS_HABILES = resultviviendas[p]['METROS_UTILES'];
                        }
                        //this.datosViviendas.METROS_HABILES = resultviviendas[p]['METROS_UTILES'];

                        if(resultviviendas[p]['EUROS_METRO_CONS'] !== null && resultviviendas[p]['EUROS_METRO_CONS'] !== '')
                        {
                          this.datosViviendas.EUROS_METRO_CONS = resultviviendas[p]['EUROS_METRO_CONS'].toFixed(2);
                        }
                        else
                        {
                          this.datosViviendas.EUROS_METRO_CONS = resultviviendas[p]['EUROS_METRO_CONS'];
                        }
                        //this.datosViviendas.EUROS_METRO_CONS = resultviviendas[p]['EUROS_METRO_CONS'];
                        if(resultviviendas[p]['EUROS_METRO_UTIL'] !== null && resultviviendas[p]['EUROS_METRO_UTIL'] !== '')
                        {
                          this.datosViviendas.EUROS_METRO_UTIL = resultviviendas[p]['EUROS_METRO_UTIL'].toFixed(2);
                        }
                        else
                        {
                          this.datosViviendas.EUROS_METRO_UTIL = resultviviendas[p]['EUROS_METRO_UTIL'];
                        }
                        //this.datosViviendas.EUROS_METRO_UTIL = resultviviendas[p]['EUROS_METRO_UTIL'];
                        this.datosViviendas.ACTO = resultviviendas[p]['ACTO'];
                        this.datosViviendas.NOMBRE_VIVIENDA = resultviviendas[p]['NOMBRE_VIVIENDA'];
                        this.datosViviendas.ESTADO_VIVIENDA = resultviviendas[p]['ESTADO_VIVIENDA'];

                        const div = document.createElement('tr');
                        div.innerHTML = '<td style="text-align: left;" colspan=2>Inmueble:</td><td style="text-align: center;">'+this.datosViviendas.NOMBRE_VIVIENDA+'</td><td style="text-align: center;" colspan=2>Precio Neto:</td> <td style="text-align: center;">'+this.datosViviendas.PRECIO_VIVIENDA+' €</td>';
                        var target = document.getElementById("tabla"+codigoprinex).appendChild(div);

                        const div2 = document.createElement('tr');
                        div2.innerHTML = '<td style="text-align: left;width:20%" colspan=2>Metros Hábiles:</td>  <td style="text-align: center;">'+this.datosViviendas.METROS_HABILES+' m2</td><td style="text-align: center;width:20%" colspan=2>Metros Construidos:</td> <td style="text-align: center;">'+this.datosViviendas.METROS_CONSTRUIDOS+' m2</td>';
                        var target2 = document.getElementById("tabla"+codigoprinex).appendChild(div2);

                        const div4 = document.createElement('tr');
                        div4.innerHTML = '<td style="text-align: left;width:20%" colspan=2>Estado:</td>  <td style="text-align: center;">'+this.datosViviendas.ESTADO_VIVIENDA+'</td>';
                        var target4 = document.getElementById("tabla"+codigoprinex).appendChild(div4);

                        const div3 = document.createElement('tr');
                        div3.innerHTML = '<td style="text-align: left;" colspan=6><hr></td>';
                        var target3 = document.getElementById("tabla"+codigoprinex).appendChild(div3);

                      }
                      
                  },
                  error => {
                    console.log('Error Obtención de los usuarios');
                  }
                );


              }
    
    
  }

  public exportCSVTOTAL() {


    var cabecera = ["CODIGO_PRINEX", "NOMBRE", "APELLIDO", "DNI", "FIJO", "PROFESION", "MOVIL", "EMAIL", "POBLACION", "PROVINCIA", "CODIGO_POSTAL", "TIPO_VIA", "NOMBRE_VIA", "NUMERO_VIA", "ESCALERA", "PISO", "PUERTA", "ESTADO_CIVIL", "AÑO", "MES", "DIA", "F_ALTA", "H_ALTA", "TIPO", "CODPAIS", "PAIS", "NOMBRE_VIVIENDA", "PROMOCION", "NETO_VIVIENDA", "METROS_CONS", "METROS_UTILES", "EUROS_METRO_CONS", "EUROS_METRO_UTIL", "ACTO", "VIVIENDA", "CONO", "CESTA", "ESTADO_VIVIENDA"];



    this.FechaInicial = (<HTMLInputElement>document.getElementById('initialcalendar')).value;
    this.FechaFinal = (<HTMLInputElement>document.getElementById('finishcalendar')).value;

    if(this.FechaInicial != "" && this.FechaFinal != "")
    {
      let selectVivienda = AppSettings.SELECT_USUARIOS_COMPLETA(this.FechaInicial, this.FechaFinal);
                  this.wsService.getViviendasCompradores(selectVivienda)
                  .pipe(
                    finalize(() => {
                    })
                  )
                  .subscribe(
                    resultviviendas => {
                      const jsonData = JSON.parse(JSON.stringify(resultviviendas));
                      const jsonObject = JSON.stringify(jsonData);
                      const csv = this.convertToCSV(jsonObject, cabecera);

                      const exportName = "DatosGlobales.csv" || "exportDatosGlobales.csv";

                      const blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
                      if (navigator.msSaveBlob) {
                        navigator.msSaveBlob(blob, exportName);
                      } else {
                        const link = document.createElement("a");
                        if (link.download !== undefined) {
                        const url = URL.createObjectURL(blob);
                        link.setAttribute("href", url);
                        link.setAttribute("download", exportName);
                        link.style.visibility = "hidden";
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                        }
                      }
                        
                    },
                    error => {
                      console.log('Error Obtención de los datos globales');
                    }
                  );
    }
    else
    {
      alert("Debe seleccionar una fecha inicial y una fecha final para mostrar los datos de los usuarios");
    }
  }

  public exportCSVUSUARIOS() {

    var cabecera = ["CODIGO_PRINEX", "NOMBRE", "APELLIDO", "DNI", "FIJO", "PROFESION", "MOVIL", "EMAIL", "POBLACION", "PROVINCIA", "CODIGO_POSTAL", "TIPO_VIA", "NOMBRE_VIA", "NUMERO_VIA", "ESCALERA", "PISO", "PUERTA", "ESTADO_CIVIL", "AÑO", "MES", "DIA", "F_ALTA", "H_ALTA", "TIPO", "CODPAIS", "PAIS"];

    this.FechaInicial = (<HTMLInputElement>document.getElementById('initialcalendar')).value;
    this.FechaFinal = (<HTMLInputElement>document.getElementById('finishcalendar')).value;

    if(this.FechaInicial != "" && this.FechaFinal != "")
    {
      let selectVivienda = AppSettings.SELECT_USUARIOS(this.FechaInicial, this.FechaFinal);
                  this.wsService.getViviendasCompradores(selectVivienda)
                  .pipe(
                    finalize(() => {
                    })
                  )
                  .subscribe(
                    resultviviendas => {
                      const jsonData = JSON.parse(JSON.stringify(resultviviendas));

                      const jsonObject = JSON.stringify(jsonData);
                      const csv = this.convertToCSV(jsonObject, cabecera);

                      const exportName = "DatosUsuarios.csv" || "exportDatosUsuarios.csv";

                      const blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
                      if (navigator.msSaveBlob) {
                        navigator.msSaveBlob(blob, exportName);
                      } else {
                        const link = document.createElement("a");
                        if (link.download !== undefined) {
                        const url = URL.createObjectURL(blob);
                        link.setAttribute("href", url);
                        link.setAttribute("download", exportName);
                        link.style.visibility = "hidden";
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                        }
                      }
                        
                    },
                    error => {
                      console.log('Error Obtención de los datos usuarios');
                    }
                  );
    }
    else
    {
      alert("Debe seleccionar una fecha inicial y una fecha final para mostrar los datos de los usuarios");
    }
  }

  public exportCSVVIVIENDAS(codigoPrinex: string) {

    var cabecera = ["PRINEX", "NOMBRE_VIVIENDA", "PROMOCION", "NETO_VIVIENDA", "METROS_CONS", "METROS_UTILES", "EUROS_METRO_CONS", "EUROS_METRO_UTIL", "ACTO", "VIVIENDA", "CONO", "CESTA", "ESTADO_VIVIENDA"];
   
      let selectVivienda = AppSettings.SELECT_VIVIENDA_USUARIOS(codigoPrinex);
                  this.wsService.getViviendasCompradores(selectVivienda)
                  .pipe(
                    finalize(() => {
                    })
                  )
                  .subscribe(
                    resultviviendas => {
                      const jsonData = JSON.parse(JSON.stringify(resultviviendas));

                      const jsonObject = JSON.stringify(jsonData);
                      const csv = this.convertToCSV(jsonObject, cabecera);

                      const exportName = "DatosViviendas.csv" || "exportDatosViviendas.csv";

                      const blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
                      if (navigator.msSaveBlob) {
                        navigator.msSaveBlob(blob, exportName);
                      } else {
                        const link = document.createElement("a");
                        if (link.download !== undefined) {
                        const url = URL.createObjectURL(blob);
                        link.setAttribute("href", url);
                        link.setAttribute("download", exportName);
                        link.style.visibility = "hidden";
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                        }
                      }
                        
                    },
                    error => {
                      console.log('Error Obtención de los datos usuarios');
                    }
                  );

  }



  public convertToCSV(objArray, cabecera) {
  const array = typeof objArray != "object" ? JSON.parse(objArray) : objArray;
  let str = "";


  let line = "";
  for(let j = 0; j < cabecera.length; j++)
  {
     if (line != "") line += ","; 
    line += cabecera[j];
   }
   str += line + "\r\n";
  
 for (let i = 0; i < array.length; i++) {
   let line = "";
   for (let index in array[i]) {
    if (line != "") line += ",";

 line += array[i][index];
   }
 
 str += line + "\r\n";
  }
 
 return str;
  }



}

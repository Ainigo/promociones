import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MktcloudComponent } from './mktcloud.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('MktcloudComponent', () => {
  let component: MktcloudComponent;
  let fixture: ComponentFixture<MktcloudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgbModule],
      declarations: [ MktcloudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MktcloudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

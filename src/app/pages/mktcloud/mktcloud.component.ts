import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Categoria} from '../../classes/categoria';
import { ProductosService } from 'src/app/services/productos.service';
import { WebsocketService } from '../../services/websocket.service';
import { finalize } from 'rxjs/operators';
import { datosPropietario} from '../../classes/datosPropietario';
import { AuthenticationService } from '../../core';
import * as $ from 'jquery';
import { datosAnejos } from 'src/app/classes/datosAnejos';
import { datosVivienda } from 'src/app/classes/datosVivienda';
import { AppSettings } from '../../shared/app-settings';
import { datosViviendaSalesForce } from 'src/app/classes/datosViviendaSalesForce';
import { datosUsuarioSalesForce } from 'src/app/classes/datosUsuarioSalesForce';
import { datosUsuarioPrinex } from 'src/app/classes/datosUsuarioPrinex';
import { datosPromocionesSalesforce } from 'src/app/classes/datosPromocionesSalesforce';

declare var html2canvas: any;
declare var jsPDF: any;
declare var req: any;


@Component({
  selector: 'app-mktcloud',
  templateUrl: './mktcloud.component.html',
  styleUrls: ['./mktcloud.component.css']
})



export class MktcloudComponent implements OnInit {

  public promociones: string [] = [];
  public viviendas: string [] = [];
  public PromocionesID: string;
  public nombreUsuario: string;
  public arrayViviendas: string [] = [];
  public cambios: string [] = [];
  public usuario: datosPropietario;
  public anejos: datosAnejos;
  public datosViviendas: datosVivienda;
  public datosViendaArray: datosVivienda [] = [];
  public activadoAreaPrivada = '';
  public Viviendassalesforce: datosViviendaSalesForce [] = [];
  public DatosUsuariosalesforce: datosUsuarioSalesForce [] = [];
  public DatosUsuariosalesforce2: datosUsuarioSalesForce [] = [];
  public DatosUsuarioprinex: datosUsuarioPrinex [] = [];
  public DatosUsuarioprinex2: datosUsuarioPrinex [] = [];
  public DatoViviendaPrinex: datosVivienda [] = [];
  public IdInmueble = '';
  public estadoComercialSalesForce = '';
  public estadoViviendaEscriturada = '';
  public Inmuebles: string [] = [];
  public datosPromocionesArray: datosPromocionesSalesforce [] = [];
  public datosPromocionesMKT: Number [] = [];



  constructor(private router: Router, public wsService: WebsocketService, public authenticationService: AuthenticationService, private route: ActivatedRoute) { }
  public usuarioAdmin;
  ngOnInit() {
    
    const credentials = this.authenticationService.credentials;

    this.nombreUsuario = credentials.username;
    this.wsService.getpintarTrazasLogs(this.nombreUsuario,'El nombre de usuario es: ' + this.nombreUsuario)
    this.wsService.getpintarTrazasLogs(this.nombreUsuario,'vamos a mirar el perfil');


    this.wsService.getUserApp(this.nombreUsuario)
  .pipe(
    finalize(() => {
    })
  )
  .subscribe(
    result => {
      let valor = JSON.stringify(result);
      let valor2 = JSON.parse(valor);
      this.wsService.getpintarTrazasLogs(this.nombreUsuario,'Vamos a la obtención del perfil' +valor2["result"] );
      this.usuarioAdmin = valor2["result"];
      this.wsService.getpintarTrazasLogs(this.nombreUsuario,'El resultado es: '+this.usuarioAdmin);
      if(this.usuarioAdmin.toLowerCase() == 'admin' || this.usuarioAdmin.toLowerCase() == 'marketing' )
      {

        let promocion = new datosPromocionesSalesforce();
        promocion.IdPromocionSalesForce = ' ';
        promocion.NamePromocionSalesForce = 'Elija una opción';
        this.datosPromocionesArray.push(promocion);
    
        this.getPromotions();
    
      }
      if(this.usuarioAdmin.toLowerCase() == 'user')
      {
        this.router.navigateByUrl('/login');
      }
      if(this.usuarioAdmin.toLowerCase() == 'notario')
      {
        this.router.navigateByUrl('/login');
      }
      if(this.usuarioAdmin.toLowerCase() == 'portales')
      {
        this.router.navigateByUrl('/login');
      }

    },
    error => {
      this.wsService.getpintarTrazasLogs(this.nombreUsuario,'Error');
    }
  );
  }

  volver() {
    this.route.queryParams.subscribe(params => {
      this.router.navigate(['/promociones'], { replaceUrl: true });
    });
  }

  public getPromotions(): void {

    this.wsService.getListaPromociones().subscribe(
      data => {
        let datosinmuebles = JSON.stringify(data['result']);
        
        data['result'].forEach((element, index) => {
          let VivSalesForce = new datosPromocionesSalesforce();
          VivSalesForce.IdPromocionSalesForce = element['id'];
          VivSalesForce.NamePromocionSalesForce = element['name'];
          VivSalesForce.Id_Prinex = element['IdentificadorPrinex__c'];
          this.datosPromocionesArray.push(VivSalesForce);
        });
        
        
      },
      error => {
        console.log(error, { depth: null});
      }
    );

  }

  GetPromociones() {
    this.IdInmueble = '';
    this.Inmuebles = [];
    let promocionesSalesForce = (<HTMLInputElement>document.getElementById('promocionesUserSalesForce')).value;
    let promocionesOrigenSalesForce = (<HTMLInputElement>document.getElementById('promocionesOrigenSalesForce')).value;



  

    if(promocionesSalesForce != "" && promocionesOrigenSalesForce != "")
    {
      let botonSelect = document.getElementById('buttonToUploadSelect');
      botonSelect.style.display = 'none';
  
      let botonAtras = document.getElementById('buttonToUploadAtras');
      botonAtras.style.display = 'none';


      let botonUrl = document.getElementById('buttonToUploadUrl');
      botonUrl.style.display = 'inline';

      let botonReloaded = document.getElementById('buttonToUploadReloaded');
      botonReloaded.style.display = 'inline';

      document.getElementById("promocionesUserSalesForce").setAttribute("readonly", "true");
      document.getElementById("promocionesOrigenSalesForce").setAttribute("readonly", "true");

      let valuePromociones = Number(promocionesSalesForce);
      
      for(let x = 0; x < valuePromociones; x++)
      {
        this.datosPromocionesMKT.push(x);
      }
      
    }
    else
    {
       alert("Debe indicar el número de promociones de SalesForce y el campo de origen");
    }

  }

  GetUrl() {

    var arrayPromotion: string[]  = [];
    let promocionesSalesForce = (<HTMLInputElement>document.getElementById('promocionesUserSalesForce')).value;
    let promocionesOrigenSalesForce = (<HTMLInputElement>document.getElementById('promocionesOrigenSalesForce')).value;

    let valuePromociones = Number(promocionesSalesForce);
    let seleccionvalores = true;

    var data =  [     
    ]

    
    for(let x = 0; x < valuePromociones; x++)
    {
      let valuePromotion = (<HTMLSelectElement>document.getElementById('selectpromociones'+x)).value;
      let valueText;
      
      
      if(valuePromotion != 'Elija una opción/ ')
      {
        var arrayDeCadenas = valuePromotion.split('/');
        valueText = arrayDeCadenas[1];
        var arrayDeCadenas2 = arrayDeCadenas[0];
        let arrayDeCadenas3 = arrayDeCadenas2.split('- ');
        valuePromotion = arrayDeCadenas3[1];
        
          data.push( {
            "name": ""+valuePromotion+"",
            "value": ""+valueText+""
          },  )
        
      }
      else
      {
        seleccionvalores = false;
      }




    }

    if(seleccionvalores != false)
    {
      var urlParams = encodeURIComponent(JSON.stringify(data)) ;
     
      var urlPrincipio = 'https://cloud.e.aedashomes.net/PromosMultiples?promos=';
      var origen = promocionesOrigenSalesForce;
      var final = '&amp;origen='+origen;
      let urlFinalForce = document.getElementById('urlGenerada');
      let urlObtenida = document.getElementById('urlObtenida');
      console.log(urlPrincipio+urlParams + final);
      urlObtenida.setAttribute("href", urlPrincipio+urlParams+final);
      urlFinalForce.style.display = "inline";
    }
    else
    {
      alert("Debe rellenar todas las promociones");
    }
     


  }

  RecargarPagina() {
    window.location.reload();
  }


}

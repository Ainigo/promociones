import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Categoria} from '../../classes/categoria';
import { ProductosService } from 'src/app/services/productos.service';
import { WebsocketService } from '../../services/websocket.service';
import { finalize } from 'rxjs/operators';
import { datosPropietario} from '../../classes/datosPropietario';
import { AuthenticationService } from '../../core';
import * as $ from 'jquery';
import { datosAnejos } from 'src/app/classes/datosAnejos';
import { datosVivienda } from 'src/app/classes/datosVivienda';
import { AppSettings } from '../../shared/app-settings';
import { datosViviendaSalesForce } from 'src/app/classes/datosViviendaSalesForce';
import { datosUsuarioSalesForce } from 'src/app/classes/datosUsuarioSalesForce';
import { datosUsuarioPrinex } from 'src/app/classes/datosUsuarioPrinex';

declare var html2canvas: any;
declare var jsPDF: any;
declare var req: any;


@Component({
  selector: 'app-token',
  templateUrl: './token.component.html',
  styleUrls: ['./token.component.css']
})



export class TokenComponent implements OnInit {

  public promociones: string [] = [];
  public viviendas: string [] = [];
  public PromocionesID: string;
  public nombreUsuario: string;
  public tokenUsuario: string;
  public arrayViviendas: string [] = [];
  public cambios: string [] = [];
  public usuario: datosPropietario;
  public anejos: datosAnejos;
  public datosViviendas: datosVivienda;
  public datosViendaArray: datosVivienda [] = [];
  public activadoAreaPrivada = '';
  public Viviendassalesforce: datosViviendaSalesForce [] = [];
  public DatosUsuariosalesforce: datosUsuarioSalesForce [] = [];
  public DatosUsuariosalesforce2: datosUsuarioSalesForce [] = [];
  public DatosUsuarioprinex: datosUsuarioPrinex [] = [];
  public DatosUsuarioprinex2: datosUsuarioPrinex [] = [];
  public DatoViviendaPrinex: datosVivienda [] = [];
  public IdInmueble = '';
  public estadoComercialSalesForce = '';
  public estadoViviendaEscriturada = '';



  constructor(private router: Router, public wsService: WebsocketService, public authenticationService: AuthenticationService, private route: ActivatedRoute) { }
  public usuarioAdmin;
  ngOnInit() {
    
    const credentials = this.authenticationService.credentials;

    this.nombreUsuario = credentials.username;
    this.tokenUsuario = credentials.token;
    this.wsService.getpintarTrazasLogs(this.nombreUsuario,'El nombre de usuario es: ' + this.nombreUsuario)
    this.wsService.getpintarTrazasLogs(this.nombreUsuario,'vamos a mirar el perfil');
    document.getElementById('tokenUserSalesForce').innerHTML = this.tokenUsuario;

    

    this.wsService.getUserApp(this.nombreUsuario)
  .pipe(
    finalize(() => {
    })
  )
  .subscribe(
    result => {
      let valor = JSON.stringify(result);
      let valor2 = JSON.parse(valor);
      this.wsService.getpintarTrazasLogs(this.nombreUsuario,'Vamos a la obtención del perfil' +valor2["result"] );
      this.usuarioAdmin = valor2["result"];
      this.wsService.getpintarTrazasLogs(this.nombreUsuario,'El resultado es: '+this.usuarioAdmin);
      

    },
    error => {
      this.wsService.getpintarTrazasLogs(this.nombreUsuario,'Error');
    }
  );
  }

  getUserToken()
  {
    this.tokenUsuario = (<HTMLSelectElement>document.getElementById('tokenUserSalesForce')).value;
    this.wsService.getUserToken(this.tokenUsuario).subscribe(
      data => {
        // if(data == false)
        // {
        //   alert("El usuario "+this.emailsAreaPrivada[z].Id+" no se pudo dar de alta en Magnolia o en Active Directory")
        // }
        // else
        // {
        //   alert("Error con el token"); 
        // }

          
        
      },
      error => {
        alert('Error al obtener el usuario');
        console.log(error, { depth: null});
      }
    );
  }


}

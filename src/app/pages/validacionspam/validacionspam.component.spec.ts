import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidacionSpamComponent } from './validacionspam.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('ValidacionSpamComponent', () => {
  let component: ValidacionSpamComponent;
  let fixture: ComponentFixture<ValidacionSpamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgbModule],
      declarations: [ ValidacionSpamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidacionSpamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

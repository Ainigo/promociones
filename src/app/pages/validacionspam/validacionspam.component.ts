import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WebsocketService } from '../../services/websocket.service';
import { finalize } from 'rxjs/operators';
import { Promociones} from '../../classes/promociones';
import { datosPropietario} from '../../classes/datosPropietario';
import { AuthenticationService } from '../../core';import { AppSettings } from '../../shared/app-settings';
import { datosPromociones } from 'src/app/classes/datosPromociones';
import {datosUsuariosEmailSalesForce} from 'src/app/classes/datosUsuariosEmailSalesForce';
import {datosUsuariosSalesForceAreaPrivada} from 'src/app/classes/datosUsuariosSalesForceAreaPrivada';
import {UsuariosAltaPortales} from '../../classes/usuariosAltaPortales';
import {datosMensajes} from '../../classes/datosMensajes';
import { stringify } from 'querystring';


declare var html2canvas: any;
declare var jsPDF: any;
declare var req: any;


@Component({
  selector: 'app-validacionspam',
  templateUrl: './validacionspam.component.html',
  styleUrls: ['./validacionspam.component.css']
})



export class ValidacionSpamComponent implements OnInit {
  public usuarios: UsuariosAltaPortales [] = [];
  public mensajes : datosMensajes [] = [];
  public promociones: datosPromociones [] = [];
  public viviendas: string [] = [];
  public Viviendasanejos: Promociones [] = [];
  public Viviendaanejosdownload: Promociones [] = [];
  public PromocionesID: string;
  public nombreUsuario: string;
  public arrayViviendas: string [] = [];
  public cambios: string [] = [];
  public usuario: datosPropietario;
  public emailsUsers: datosUsuariosEmailSalesForce [] = [];
  public emailsAreaPrivada: datosUsuariosSalesForceAreaPrivada [] = [];
  



  constructor(private router: Router, public wsService: WebsocketService, public authenticationService: AuthenticationService,private route: ActivatedRoute) { }
  public usuarioAdmin;
  public urlpedido;
  public idNoExiste

  public user;
  public idPortal;
  public portal;
  public idSalesForce;
  public nombrePromocion;
  public fechaCreacion;
  public id;
  public email;
  public name;
  public idioma;
  public comentario;
  public ip;
  public navegador;
  public navigator;
  public currentIp;
  public estados: string[] = ['No Validado Por Maquina', 'Spam', 'Validado Por Usuario'];

  ngOnInit() {
    const credentials = this.authenticationService.credentials;
    const nombreUsuario = credentials.username;
    this.user = nombreUsuario;
    console.log('El nombre de usuario es: ' + nombreUsuario);
    console.log('vamos a mirar el perfil');
    this.navigator = this.getNavigator();
    
    



  this.wsService.getUserApp(nombreUsuario)
  .pipe(
    finalize(() => {
    })
  )
  .subscribe(
    result => {
      let valor = JSON.stringify(result);
      let valor2 = JSON.parse(valor);
      console.log('Vamos a la obtención del perfil' +valor2["result"] );
      this.usuarioAdmin = valor2["result"];
      console.log('El resultado es: '+this.usuarioAdmin);

      this.getFechaActual();

      this.wsService.getIp()
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(
          result => {
            this.currentIp = result['result'];
          },
          error => {
            console.log('Error en la obtención del IP');
          }
        );



      if(this.usuarioAdmin.toLowerCase() == 'admin')
      {
        
      }
      if(this.usuarioAdmin.toLowerCase() == 'user')
      {

      

      }
      if(this.usuarioAdmin.toLowerCase() == 'notario' || this.usuarioAdmin.toLowerCase() == 'portales' ||this.usuarioAdmin.toLowerCase() == 'marketing')
      {
        

        this.router.navigateByUrl('/login');
      }

    },
    error => {
      console.log('Error en Validacion Spam');
    }
  );

  }

  public getFechaActual(): void {
    var date = new Date();

    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    let valuemonth, valueday;

    
    if (month < 10)
    {
      valuemonth = "0" + month;
    }
    else
    {
      valuemonth = month;
    }
      
    if (day < 10)
    {
      valueday = "0" + day;
    }
    else
    {
      valueday = day;
    }      

    var today = valueday + "/" + valuemonth + "/" + year;

    (<HTMLInputElement>document.getElementById('initialcalendar')).value = today;
  }

  public cargarDatosSpam(fecha): void {
    let estado = '';

    this.mensajes = [];

    this.wsService.getMensajesSpam(fecha)
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      data => {
        console.log('Resultado de la obtención de los mensajes');
        console.log(data['result'].length);
        if(data['result'].length == 0)
        {
          alert("No existen datos para esa fecha");
        }
        data['result'].forEach(element => {
          let estado = '';
          let perfil = '';
          const datosMensajesSpam = new datosMensajes();
          datosMensajesSpam.id_mensaje = element['id'];
          datosMensajesSpam.email = element['email'];
          datosMensajesSpam.mensaje = element['mensaje'];
          estado = element['validacion_spam'];
          datosMensajesSpam.estado = estado;
          datosMensajesSpam.nombre_promocion = element['nombre_promocion'];
          

          console.log(datosMensajesSpam);
          this.mensajes.push(datosMensajesSpam);
        });
      },
      error => {
        console.log('Error');
      }
    );



  }
  
  public selectDatosSpam(): void {
    let fechaseleccionada = (<HTMLInputElement>document.getElementById('initialcalendar')).value;

    if(fechaseleccionada != "")
    {
      this.cargarDatosSpam(fechaseleccionada);
    }
    else
    {
      alert("Debe seleccionar una fecha");
    }
  }

  public getNavigator()
  {
     var sBrowser, sUsrAg = navigator.userAgent;

     

      if(sUsrAg.indexOf("Chrome") > -1) {
          sBrowser = "Google Chrome";
      } else if (sUsrAg.indexOf("Safari") > -1) {
          sBrowser = "Apple Safari";
      } else if (sUsrAg.indexOf("Opera") > -1) {
          sBrowser = "Opera";
      } else if (sUsrAg.indexOf("Firefox") > -1) {
          sBrowser = "Mozilla Firefox";
      } else if (sUsrAg.indexOf("MSIE") > -1) {
          sBrowser = "Microsoft Internet Explorer";
      }

      return navigator.userAgent;//sBrowser;

  }

  public getIp(){

    let valorIp = "0";

    this.wsService.getIp()
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      result => {
        alert(result['result']);
        valorIp = result['result'];
        alert(valorIp);
        return valorIp;
      },
      error => {
        console.log('Error en la obtención del IP');
      }
    );

    return valorIp;

  }

  public modificarRegistro(id){

    let valueSpam = (<HTMLSelectElement>document.getElementById('selectestado'+id)).value;
    this.currentIp;
    this.navigator;

    this.wsService.updateSpamBBDD(id,valueSpam, this.currentIp,this.user, this.navigator)
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(
          result => {
           alert('Spam modificado correctamente');
           window.location.reload();
          },
          error => {
            console.log('Error');
          }
        );

  }






}


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VincularInmuebleComponent } from './vincularinmueble.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('VincularInmuebleComponent', () => {
  let component: VincularInmuebleComponent;
  let fixture: ComponentFixture<VincularInmuebleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgbModule],
      declarations: [ VincularInmuebleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VincularInmuebleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Categoria} from '../../classes/categoria';
import { ProductosService } from 'src/app/services/productos.service';
import { WebsocketService } from '../../services/websocket.service';
import { finalize } from 'rxjs/operators';
import { Promociones} from '../../classes/promociones';
import { datosPropietario} from '../../classes/datosPropietario';
import { AuthenticationService } from '../../core';
import * as $ from 'jquery';
import { datosAnejos } from 'src/app/classes/datosAnejos';
import { datosVivienda } from 'src/app/classes/datosVivienda';
import { AppSettings } from '../../shared/app-settings';
import { datosPromociones } from 'src/app/classes/datosPromociones';
import { datosPromocionesSalesforce } from 'src/app/classes/datosPromocionesSalesforce';
import { datosUsuariosPromocionesSalesForce } from 'src/app/classes/datosUsuariosPromocionesSalesForce';
import {datosUsuariosEmailSalesForce} from 'src/app/classes/datosUsuariosEmailSalesForce';
import {datosUsuariosSalesForceAreaPrivada} from 'src/app/classes/datosUsuariosSalesForceAreaPrivada';
import {UsuariosAltaPortales} from '../../classes/usuariosAltaPortales';
import { stringify } from 'querystring';


declare var html2canvas: any;
declare var jsPDF: any;
declare var req: any;


@Component({
  selector: 'app-vincularinmueble',
  templateUrl: './vincularinmueble.component.html',
  styleUrls: ['./vincularinmueble.component.css']
})



export class VincularInmuebleComponent implements OnInit {
  public usuarios: UsuariosAltaPortales [] = [];
  public promociones: datosPromociones [] = [];
  public viviendas: string [] = [];
  public Viviendasanejos: Promociones [] = [];
  public Viviendaanejosdownload: Promociones [] = [];
  public PromocionesID: string;
  public nombreUsuario: string;
  public arrayViviendas: string [] = [];
  public cambios: string [] = [];
  public usuario: datosPropietario;
  public anejos: datosAnejos;
  public datosViviendas: datosVivienda;
  public datosViendaArray: datosVivienda [] = [];
  public datosPromocionesArray: datosPromocionesSalesforce [] = [];
  public datosUsuariosPromociones: datosUsuariosPromocionesSalesForce [] = [];
  public datosIdsPromociones: datosUsuariosPromocionesSalesForce [] = [];
  public emailsUsers: datosUsuariosEmailSalesForce [] = [];
  public emailsAreaPrivada: datosUsuariosSalesForceAreaPrivada [] = [];
  



  constructor(private router: Router, public wsService: WebsocketService, public authenticationService: AuthenticationService,private route: ActivatedRoute) { }
  public usuarioAdmin;
  public urlpedido;
  public idNoExiste

  public user;
  public idPortal;
  public portal;
  public idSalesForce;
  public nombrePromocion;
  public fechaCreacion;
  public id;
  public email;
  public name;
  public idioma;
  public comentario;
  public ip;
  public navegador;
  public navigator;
  public currentIp;
  public urlSite;

  ngOnInit() {
    this.idNoExiste = document.getElementById("Idnoexiste");
    const credentials = this.authenticationService.credentials;
    const nombreUsuario = credentials.username;
    this.user = nombreUsuario;
    console.log('El nombre de usuario es: ' + nombreUsuario);
    console.log('vamos a mirar el perfil');
    this.navigator = this.getNavigator();
    
  



  this.wsService.getUserApp(nombreUsuario)
  .pipe(
    finalize(() => {
    })
  )
  .subscribe(
    result => {
      let valor = JSON.stringify(result);
      let valor2 = JSON.parse(valor);
      console.log('Vamos a la obtención del perfil' +valor2["result"] );
      this.usuarioAdmin = valor2["result"];
      console.log('El resultado es: '+this.usuarioAdmin);
      if(this.usuarioAdmin.toLowerCase() == 'admin' || this.usuarioAdmin.toLowerCase() == 'portales' || this.usuarioAdmin.toLowerCase() == 'notario' || this.usuarioAdmin.toLowerCase() == 'user')
      {
        let promocion = new datosPromocionesSalesforce();
        promocion.IdPromocionSalesForce = ' ';
        promocion.NamePromocionSalesForce = 'Elija una opción';
        this.datosPromocionesArray.push(promocion);
        const queryString = window.location.search;
        var id = queryString.split('=');

        this.wsService.getIp()
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(
          result => {
            this.currentIp = result['result'];
          },
          error => {
            console.log('Error en la obtención del IP');
          }
        );



        console.log("id " +id[1])
        this.cargarDatoUsuario(id[1]);
        this.getPromotions();
      }
      if(this.usuarioAdmin.toLowerCase() == 'marketing')
      {

        this.router.navigateByUrl('/login');

      }
      // if(this.usuarioAdmin.toLowerCase() == 'notario')
      // {
        

      //   this.router.navigateByUrl('/login');
      // }

    },
    error => {
      console.log('Error en Vincular Inmueble');
    }
  );

  }

  public getPromotions(): void {

    this.wsService.getListaPromociones().subscribe(
      data => {
        let datosinmuebles = JSON.stringify(data['result']);
        
        data['result'].forEach((element, index) => {
          let VivSalesForce = new datosPromocionesSalesforce();
          VivSalesForce.IdPromocionSalesForce = element['id'];
          VivSalesForce.NamePromocionSalesForce = element['name'];
          VivSalesForce.Id_Prinex = element['IdentificadorPrinex__c'];
          this.datosPromocionesArray.push(VivSalesForce);
        });
        
        
      },
      error => {
        console.log(error, { depth: null});
      }
    );

  }

  public cargarDatoUsuario(idUsuario): void {
    this.wsService.getUsuariosAltaPortales(idUsuario)
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      data => {
        console.log('Resultado de la obtención de Usuarios');
        console.log(data['result'].length);
        if(data['result'].length == 0)
        {
          this.idNoExiste.style.display="inline";
          let urlEnlace = document.getElementById("urlEnlace");
          urlEnlace.style.display = "none";
          let promoEnlace = document.getElementById("promoEnlace");
          promoEnlace.style.display = "none";
          let botonEnlace = document.getElementById("botonEnlace");
          botonEnlace.style.display = "none";
        }
        data['result'].forEach(element => {
          let estado = '';
          let perfil = '';
          const usersAltaPortales = new UsuariosAltaPortales();
          usersAltaPortales.id = element['id'];
          usersAltaPortales.id_portal = element['id_portal'];
          this.urlpedido = element['url_portal'];
          usersAltaPortales.portal = element['portal'];
          usersAltaPortales.id_salesforce = element['id_salesforce'];
          usersAltaPortales.nombre_promocion = element['nombre_promocion'];
          usersAltaPortales.fecha_creacion = element['fecha_creacion'];
          usersAltaPortales.nombre = element['nombre'];
          usersAltaPortales.email = element['email'];
          usersAltaPortales.mensaje = element['mensaje'];
          usersAltaPortales.referencia = element['referencia'];
          usersAltaPortales.fecha_procesado = element['fecha_procesado'];
          usersAltaPortales.fecha_lead = element['fecha_lead'];
          usersAltaPortales.ip = element['ip'];
          usersAltaPortales.navegador = element['navegador'];
          usersAltaPortales.validacion_spam = element['validacion_spam'];
          usersAltaPortales.idioma = element['idioma'];


          this.idPortal = element['id_portal'];
          this.portal = element['portal'];
          this.idSalesForce = element['id_salesforce'];
          this.nombrePromocion = element['nombre_promocion'];
          this.fechaCreacion = Date.now();
          this.id = element['id'];
          this.name = element['nombre'];
          this.email = element['email'];
          this.idioma = element['idioma'];
          this.comentario = element['mensaje'];
          this.ip = element['ip'];
          this.navegador = element['navegador'];

          if(this.idSalesForce != null)
          {
            let promoEnlace = document.getElementById("promoEnlace");
            promoEnlace.style.display = "none";
            let botonEnlace = document.getElementById("botonEnlace");
            botonEnlace.style.display = "none";
            let TextoYaVinculado = document.getElementById("TextoYaVinculado");
            TextoYaVinculado.style.display = "inline";
          }

          console.log(usersAltaPortales);
          this.usuarios.push(usersAltaPortales);
        });
      },
      error => {
        console.log('Error');
      }
    );



  }

  public vincularPromocion(): void {
    this.PromocionesID = (<HTMLSelectElement>document.getElementById('selectpromociones')).value;
    if(this.PromocionesID != " ")
    {
      this.idSalesForce = this.PromocionesID;
      var combo = (<HTMLSelectElement>document.getElementById("selectpromociones"));
      var selected = combo.options[combo.selectedIndex].text;
      this.nombrePromocion = selected;

      var result = confirm("Comienza el proceso de vinculación para la promoción: \n ID PORTAL: "+this.idPortal+" \n El proceso comenzará con el usuario: "+this.name+" \n de la promoción: "+this.nombrePromocion+". \n ¿Desea que se ejecute el proceso?");
      if (result) {
      
      
      this.insertarVinculacion(); //INSERTAMOS EN LA TABLA PORTALES PROMOCIONES
      this.usuariosPromocion(this.idPortal); //ACTUALIZAMOS EN LA TABLA PORTALES LEADS
      // alert("El proceso de vinculación ha comenzado. En unos minutos finalizará este proceso.");
      // window.location.reload();
      //this.sendEmailVinculado();
      }

    }
    else
    {
      alert("Debe seleccionar una promoción con la que vincular");
    }
    
    
  }


  public sendEmailVinculado(): void {



    var sendTO = 'jrios@aedashomes.com';
    var sendfrom = 'notificaciones@aedashomes.com';
    var sendsubject = 'Información sobre el tratamiento de tus datos personales';
    var sendText = this.readTextFile("../../../assets/catalogo/email.html");

    //if(this.portal != 'Idealista')
    //{
      sendText = sendText.replace("<a href='https://www.idealista.com' target='_blank' style='color:#009ca6; font-weight:bold;'>idealista</a>", "<a href='"+this.urlpedido+"' target='_blank' style='color:#009ca6; font-weight:bold;'>idealista</a>");
    //}

    var jsonEmail = JSON.stringify({from:sendfrom, to: sendTO, subject: sendsubject, html: sendText});
    var jsonEmailFinal = JSON.parse(jsonEmail);

   
      this.wsService.getSendEmail(jsonEmailFinal).subscribe(
        data => {
          alert("El proceso de vinculación ha comenzado. En unos minutos finalizará este proceso.");
        },
        error => {
          console.log(error, { depth: null});
        }
      );

      


  }

  public insertarVinculacion(): void {


    this.wsService.addPortalesPromociones(this.idPortal, this.portal, this.idSalesForce, this.nombrePromocion, this.user, this.navigator, this.currentIp, this.urlpedido)
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(
          result => {
           console.log('Vinculacion realizada correctamente');
          },
          error => {
            console.log('Error');
          }
        );
  }

  public actualizarPortales(): void {
    this.wsService.updateAltasPortales(this.idSalesForce, this.nombrePromocion, this.user, this.id, this.currentIp, this.navigator, this.urlSite) .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      result => {
       console.log('Usuario modificado correctamente');
      },
      error => {
        console.log('Error');
      }
    );
  }

  public usuariosPromocion(id_portal : string): void {
    console.log(id_portal);
    this.wsService.getUsuariosPortalesLeads(id_portal)
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      data => {
        console.log('Resultado de la obtención de Usuarios');
        console.log(data['result']);
        data['result'].forEach(element => {
          this.id = element;
          this.idPortal = element['id_portal'];
          this.portal = element['portal'];
          //this.idSalesForce = element['id_salesforce'];
          console.log("EL IDsalesforce es:" +this.idSalesForce);
          console.log("EL nombrepromocion es:" +this.nombrePromocion);
          //this.nombrePromocion = element['nombre_promocion'];
          //console.log(this.nombrePromocion);
          this.fechaCreacion = Date.now();
          this.id = element['id'];
          this.name = element['nombre'];
          this.email = element['email'];
          this.idioma = element['idioma'];
          this.comentario = element['mensaje'];
          this.ip = this.currentIp;
          this.navegador = this.navigator;
          this.urlSite = this.urlpedido;

          this.actualizarPortales();
          
        });
        alert("El proceso de vinculación ha comenzado. En unos minutos finalizará este proceso.");
        window.location.reload();
      },
      error => {
        console.log('Error');
      }
    );



    
  }

  public readTextFile(file)
  {
    var allText = null;
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                allText = rawFile.responseText;
            }
        }
    }
    rawFile.send(null);

    return allText;

  }

  public createLeads(){

    this.wsService.getCreateLead(this.email,this.name, this.idioma, this.comentario,  this.ip, this.navegador, this.urlpedido,  this.idPortal).subscribe(
      data => {
        alert("DATA"+data);
      },
      error => {
        console.log(error, { depth: null});
      }
    );







    // this.wsService.getCreateLead(this.email,this.name, this.idioma, this.comentario,  this.ip, this.navegador, this.urlpedido,  this.idPortal)
    // .pipe(
    //   finalize(() => {
    //   })
    // )
    // .subscribe(
    //   data => {
    //     if(data == false)
    //     {
    //       alert("El Lead no pudo ser creado");
    //     }
    //     else
    //     {
    //       alert("El Lead creado correctamente");
    //     }
          
        
    //   },
    //   error => {
    //     alert('Error al gestionar la creación del Lead'+error)
    //     console.log(error, { depth: null});
    //   }
    // );
  }

  public getNavigator()
  {
     var sBrowser, sUsrAg = navigator.userAgent;

     

      if(sUsrAg.indexOf("Chrome") > -1) {
          sBrowser = "Google Chrome";
      } else if (sUsrAg.indexOf("Safari") > -1) {
          sBrowser = "Apple Safari";
      } else if (sUsrAg.indexOf("Opera") > -1) {
          sBrowser = "Opera";
      } else if (sUsrAg.indexOf("Firefox") > -1) {
          sBrowser = "Mozilla Firefox";
      } else if (sUsrAg.indexOf("MSIE") > -1) {
          sBrowser = "Microsoft Internet Explorer";
      }

      return navigator.userAgent;//sBrowser;

  }

  public getIp(){

    let valorIp = "0";

    this.wsService.getIp()
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      result => {
        alert(result['result']);
        valorIp = result['result'];
        alert(valorIp);
        return valorIp;
      },
      error => {
        console.log('Error en la obtención del IP');
      }
    );

    return valorIp;

  }






}


import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WebsocketService } from '../../services/websocket.service';
import { finalize } from 'rxjs/operators';
import { AuthenticationService } from '../../core';


@Component({
  selector: 'app-talkdesk',
  templateUrl: './talkdesk.component.html',
  styleUrls: ['./talkdesk.component.css']
})



export class TalkdeskComponent implements OnInit {  



  constructor(private router: Router, public wsService: WebsocketService, public authenticationService: AuthenticationService,private route: ActivatedRoute) { }
  public usuarioAdmin;
  public urlpedido;
  public idNoExiste

  public user;
  public idPortal;
  public portal;
  public idSalesForce;
  public nombrePromocion;
  public fechaCreacion;
  public id;
  public email;
  public name;
  public idioma;
  public comentario;
  public ip;
  public navegador;
  public navigator;
  public currentIp;
  public urlSite;
  public url;
  public fecha;
  public duracion;

  ngOnInit() {

    

    const queryString = window.location.search;
    var id = queryString.split('=');

    if(id[1] != undefined)
    {
      this.cargarDatosTalkdesk(id[1])
    }
    else 
    {
        document.getElementById('video').style.display = 'none'
        document.getElementById('datosllamada').style.display = 'none'
        document.getElementById('nollamada').style.display = 'inline'
        console.log("No incluyó un id correcto contacte con el Administrador");
      }



    /*

    const credentials = this.authenticationService.credentials;
    const nombreUsuario = credentials.username;
    this.user = nombreUsuario;
    console.log('El nombre de usuario es: ' + nombreUsuario);
    console.log('vamos a mirar el perfil');
    this.navigator = this.getNavigator();   



  this.wsService.getUserApp(nombreUsuario)
  .pipe(
    finalize(() => {
    })
  )
  .subscribe(
    result => {
      let valor = JSON.stringify(result);
      let valor2 = JSON.parse(valor);
      console.log('Vamos a la obtención del perfil' +valor2["result"] );
      this.usuarioAdmin = valor2["result"];
      console.log('El resultado es: '+this.usuarioAdmin);
      if(this.usuarioAdmin.toLowerCase() == 'admin')
      {
       
      
      }
      if(this.usuarioAdmin.toLowerCase() != 'admin')
      {

        this.router.navigateByUrl('/login');

      }
    },
    error => {
      console.log('Error en Talkdesk');
    }
  );

  */

  }
  
  public getNavigator()
  {
     var sBrowser, sUsrAg = navigator.userAgent;

     

      if(sUsrAg.indexOf("Chrome") > -1) {
          sBrowser = "Google Chrome";
      } else if (sUsrAg.indexOf("Safari") > -1) {
          sBrowser = "Apple Safari";
      } else if (sUsrAg.indexOf("Opera") > -1) {
          sBrowser = "Opera";
      } else if (sUsrAg.indexOf("Firefox") > -1) {
          sBrowser = "Mozilla Firefox";
      } else if (sUsrAg.indexOf("MSIE") > -1) {
          sBrowser = "Microsoft Internet Explorer";
      }

      return navigator.userAgent;//sBrowser;

  }

  public getIp(){

    let valorIp = "0";

    this.wsService.getIp()
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      result => {
        alert(result['result']);
        valorIp = result['result'];
        alert(valorIp);
        return valorIp;
      },
      error => {
        console.log('Error en la obtención del IP');
      }
    );

    return valorIp;

  }

  public base64ToArrayBuffer(base64) {
    var binaryString =  window.atob(decodeURIComponent(base64));
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++)        {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
}







  public cargarDatosTalkdesk(idTalkdesk): void {


    

    var rutaFichero = ""
    this.wsService.getTalkdeskFile(idTalkdesk)
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      data => {
        console.log('Resultado de la obtención de los datos de Talkdesk '+data['result']);
        if(data['result'].length == 0)
        {
          document.getElementById('video').style.display = 'none'
          document.getElementById('datosllamada').style.display = 'none'
          document.getElementById('nollamada').style.display = 'inline'
          console.log("No existe Ruta para ese Id, contacte con el Administrador");
        }
        else
        {
          data['result'].forEach(element => {
            rutaFichero = element['Destino_file__c']
            var a = new Date(element['CreatedDate']);
            var dd = a.getDate(); 
            var mm = a.getMonth()+1; 
            var yyyy = a.getFullYear(); 
            var today = dd+'/'+mm+'/'+yyyy;
            this.fecha = today;
            this.duracion = element['talkdesk__CallDuration__c']
          });
          
          
          this.wsService.downloadTalkdeskFile(rutaFichero)
          .pipe(
            finalize(() => {
            })
          )
          .subscribe(
            
            data => {             
              console.log('Resultado FINAL de la descarga de Talkdesk '+JSON.stringify(data));
              let result1 = JSON.stringify(data)
              let result2 = JSON.parse(result1)
              this.url = "/assets/mp3/"+result2[0]['result'];
              console.log('Resultado FINAL de la descarga de Talkdesk '+this.url);
              document.getElementById('video').style.display = 'inline'
              document.getElementById('datosllamada').style.display = 'inline'
              document.getElementById('nollamada').style.display = 'none'
            },
            error => {
              document.getElementById('video').style.display = 'none'
              document.getElementById('datosllamada').style.display = 'none'
              document.getElementById('nollamada').style.display = 'inline'
              console.log('No se pudo obtener los datos del video contacte con el administrador');
            }
          );

          
        }
        
      },
      error => {
        document.getElementById('video').style.display = 'none'
        document.getElementById('datosllamada').style.display = 'none'
        document.getElementById('nollamada').style.display = 'inline'
        console.log('El Id no existe en Base de datos contacte con el administrador');
      }
    );



  }





}


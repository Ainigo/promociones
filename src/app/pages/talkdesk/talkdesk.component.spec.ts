import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TalkdeskComponent } from './talkdesk.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('TalkdeskComponent', () => {
  let component: TalkdeskComponent;
  let fixture: ComponentFixture<TalkdeskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgbModule],
      declarations: [ TalkdeskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TalkdeskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

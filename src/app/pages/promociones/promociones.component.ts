import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Categoria} from '../../classes/categoria';
import { ProductosService } from 'src/app/services/productos.service';
import { WebsocketService } from '../../services/websocket.service';
import { finalize } from 'rxjs/operators';
import { Promociones} from '../../classes/promociones';
import { datosPropietario} from '../../classes/datosPropietario';
import { AuthenticationService } from '../../core';
import * as $ from 'jquery';
import { datosAnejos } from 'src/app/classes/datosAnejos';
import { datosVivienda } from 'src/app/classes/datosVivienda';
import { AppSettings } from '../../shared/app-settings';
import { datosPromociones } from 'src/app/classes/datosPromociones';

declare var html2canvas: any;
declare var jsPDF: any;
declare var req: any;


@Component({
  selector: 'app-promociones',
  templateUrl: './promociones.component.html',
  styleUrls: ['./promociones.component.css']
})



export class PromocionesComponent implements OnInit {

  public promociones: datosPromociones [] = [];
  public viviendas: string [] = [];
  public Viviendasanejos: Promociones [] = [];
  public Viviendaanejosdownload: Promociones [] = [];
  public PromocionesID: string;
  public nombreUsuario: string;
  public arrayViviendas: string [] = [];
  public cambios: string [] = [];
  public usuario: datosPropietario;
  public anejos: datosAnejos;
  public datosViviendas: datosVivienda;
  public datosViendaArray: datosVivienda [] = [];
  public estadocivil = new Map([['X', 'CASADO'], ['S', 'SOLTERO'], ['D', 'DIVORCIADO'], ['V', 'VIUDO / PAREJA DE HECHO']]);
  public tipovia = new Map([['AD', 'ALDEA'],['AL', 'ALAMEDA'],['AP', 'APARTAMENTO'],['AV', 'AVENIDA'],['BL', 'BLOQUE'],['BO', 'BARRIO'],['CH', 'CHALET'],['CL', 'CALLE'],['CM', 'CAMINO'], ['CO', 'COLONIA'],['CR', 'CARRETERA'],['CS', 'CASERIO'],['CT', 'CUESTA'],['ED', 'EDIFICIO'],['GL', 'GLORIETA'],['GR', 'GRUPO'],['LG', 'LUGAR'],['MC', 'MERCADO'],['MN', 'MUNICIPIO'],['MZ', 'MANZANA'],['PB', 'POBLADO'],['PG', 'POLIGONO'],['PJ', 'PASAJE'],['PQ', 'PARQUE'],['PR', 'PROLONGACION'],['PS', 'PASEO'],['PZ', 'PLAZA'],['RB', 'RAMBLA'],['RD', 'RONDA'],['TR', 'TRAVESIA'],['UR', 'URBANIZACION']]);
  public p: number = 1;



  constructor(private router: Router, public wsService: WebsocketService, public authenticationService: AuthenticationService,private route: ActivatedRoute) { }
  public usuarioAdmin;
  ngOnInit() {
    
    const credentials = this.authenticationService.credentials;

    const nombreUsuario = credentials.username;
    console.log('El nombre de usuario es: ' + nombreUsuario);
    console.log('vamos a mirar el perfil');


  this.wsService.getUserApp(nombreUsuario)
  .pipe(
    finalize(() => {
    })
  )
  .subscribe(
    result => {
      let valor = JSON.stringify(result);
      let valor2 = JSON.parse(valor);
      console.log('Vamos a la obtención del perfil' +valor2["result"] );
      this.usuarioAdmin = valor2["result"];
      console.log('El resultado es: '+this.usuarioAdmin);
      if(this.usuarioAdmin == 'admin')
      {
        this.viviendas.push('Elija una opción');

        let promocion = new datosPromociones();
        promocion.PMNOMP = ' ';
        promocion.PMPROM = 'Elija una opción';
        this.promociones.push(promocion);

        this.getPromotions();


          ($('#selectanejos') as any).select2();
          $('#checkbox').click(function(){
            if($('#checkbox').is(':checked') ) {
                $('#selectanejos > option').prop('selected','selected');
                $('#selectanejos').trigger('change');
            }else{
                $('#selectanejos').val(null).trigger('change');
            }
        });
      }
      if(this.usuarioAdmin.toLowerCase() == 'user')
      {
        this.viviendas.push('Elija una opción');

        let promocion = new datosPromociones();
        promocion.PMNOMP = ' ';
        promocion.PMPROM = 'Elija una opción';
        this.promociones.push(promocion);

        this.getPromotionsUsers(nombreUsuario);


          ($('#selectanejos') as any).select2();
          $('#checkbox').click(function(){
            if($('#checkbox').is(':checked') ) {
                $('#selectanejos > option').prop('selected','selected');
                $('#selectanejos').trigger('change');
            }else{
                $('#selectanejos').val(null).trigger('change');
            }
        });
      }
      if(this.usuarioAdmin== 'notario' || this.usuarioAdmin== 'portales' || this.usuarioAdmin== 'marketing')
      {
        this.router.navigateByUrl('/login');
      }

    },
    error => {
      console.log('Error');
    }
  );

    

  }

  public getPromotions(): void {

    this.wsService.getPromocionName()
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      result => {
        console.log(result.length);
        for(let i = 0; i < result.length; i++) {
          let promocionName = new datosPromociones();
          promocionName.PMNOMP = result[i]['PMPROM'];
          promocionName.PMPROM = result[i]['PMNOMP'];
          this.promociones.push(promocionName);
        }

      },
      error => {
        console.log('Error');
      }
    );

  }

  public getPromotionsUsers(useremail : string): void {

    this.wsService.getPromocionNameUser(useremail)
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      result => {
        console.log(result.length);
        for(let i = 0; i < result.length; i++) {
          let promocionName = new datosPromociones();
          promocionName.PMNOMP = result[i]['PMPROM'];
          promocionName.PMPROM = result[i]['PMNOMP'];
          this.promociones.push(promocionName);
        }

      },
      error => {
        console.log('Error');
      }
    );

  }

  public getViviendas(): void {

    this.PromocionesID = (<HTMLSelectElement>document.getElementById('selectpromociones')).value;
    this.viviendas = [];

    if(this.PromocionesID === 'Elija una opción'){
      this.viviendas.push('Elija una opción');
    }
    else
    {
      this.wsService.getViviendas(this.PromocionesID)
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      result => {
        console.log(result);
        console.log(result.length);
        this.viviendas.push('Elija una opción');

        for(let i = 0; i < result.length; i++) {
          this.viviendas.push(result[i]['UHEDIT']);
        }

      },
      error => {
        console.log('Error');
      }
    );
    }
  }

  public getViviendaSeleccionada(): void {
    this.Viviendasanejos = [];
    let viviendaSeleccionada = <HTMLSelectElement>document.getElementById('selectanejos');
    let generarPdf = <HTMLElement>document.getElementById('GenerarPdf');


    console.log(viviendaSeleccionada);
    this.arrayViviendas = [];
    let anejosArray: string [] = ['UHPROM','UHTIEX','UHPOEX','UHPLEX','UHLEEX','UHDAEX'];

    let collection = viviendaSeleccionada.selectedOptions;

     for (let i=0; i<collection.length; i++)
     {
        console.log(collection[i].value);
        if(collection[i].value !== 'Elija una opción')
        {
          this.arrayViviendas.push(collection[i].value);
        }
      }

      for(let j = 0 ; j < this.arrayViviendas.length; j++)
      {

        let viviendapartes: string [] = [];
        viviendapartes = (this.arrayViviendas[j]).split('-');
        const partes = viviendapartes.length;
        console.log('la vivienda ' + this.arrayViviendas[j] + ' tiene ' + partes);
        const viviendaconAnejo = new Promociones();
        let select;

        select = AppSettings.SELECT_PROMOCIONES(this.PromocionesID);

        for(let k = 1; k <= partes; k++)
        {
          select = select + "AND " + anejosArray[k] + "='" +viviendapartes[k-1].trim()+"'";
        }
        console.log('La select es: '+ select);

        let selectVivienda = AppSettings.SELECT_VIVIENDA(this.PromocionesID, this.arrayViviendas[j]);
        this.wsService.getDatosViviendas(selectVivienda)
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(
          result => {
          viviendaconAnejo.DATOSVIVIENDAS =  [];
          for(let i = 0; i < result.length; i++) {
            this.datosViviendas = new datosVivienda();
            this.datosViviendas.NOMBRE_VIVIENDA = result[i]['UHALIA'];
            this.datosViviendas.PRECIO_VIVIENDA = result[i]['UHNETO'];
            if(result[i]['UHMTUT'] !== null && result[i]['UHMTUT'] !== '')
            {
              this.datosViviendas.METROS_HABILES = result[i]['UHMTUT'].toFixed(2);
            }
            else
            {
              this.datosViviendas.METROS_HABILES = result[i]['UHMTUT'];
            }

            if(result[i]['UHMTCO'] !== null && result[i]['UHMTCO'] !== '')
            {
              this.datosViviendas.METROS_CONSTRUIDOS = result[i]['UHMTCO'].toFixed(2);
            }
            else
            {
              this.datosViviendas.METROS_CONSTRUIDOS = result[i]['UHMTCO'];
            }


            this.datosViviendas.ESTADO_VIVIENDA = result[i]['UHDEES'];
            viviendaconAnejo.DATOSVIVIENDAS.push(this.datosViviendas);
          }
          this.wsService.getAnejos(select)
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(
          result => {
            console.log(result);
            console.log(result.length);
            viviendaconAnejo.UHPROM = this.arrayViviendas[j];
            viviendaconAnejo.DATOSANEJOS =  [];
            for(let i = 0; i < result.length; i++) {
              this.anejos = new datosAnejos();
              this.anejos.NOMBRE_ANEJO = result[i]['UHNOCO'];
              if(result[i]['UHMTCO'] !== null && result[i]['UHMTCO'] !== '')
              {
                this.anejos.METROS_ANEJO = result[i]['UHMTCO'].toFixed(2);
              }
              else
              {
                this.anejos.METROS_ANEJO = result[i]['UHMTCO'];
              }
              this.anejos.PRECIO_ANEJO = result[i]['UHNETO'];

              viviendaconAnejo.DATOSANEJOS.push(this.anejos);
            }

            let selectPropietarios = AppSettings.SELECT_PROPIETARIOS(this.PromocionesID, this.arrayViviendas[j]);
            console.log("LA SELECT DE PROPIETARIOS ES: "+ selectPropietarios);

            this.wsService.getCompradores(selectPropietarios)
              .pipe(
                finalize(() => {
                })
              )
              .subscribe(
                result => {
                  console.log(result);
                  console.log(result.length);
                  let estado = '';

                  viviendaconAnejo.COMPRADORES =  [];

                    for(let i = 0; i < result.length; i++) {
                      this.usuario = new datosPropietario();
                      this.usuario.PROMOCION = result[i]['PROMOCION'];
                      this.usuario.VIVIENDA = result[i]['VIVIENDA'];
                      this.usuario.CODIGO_PRINEX = result[i]['CODIGO_PRINEX'];
                      this.usuario.NOMBRE = result[i]['NOMBRE'];
                      this.usuario.APELLIDOS = result[i]['APELLIDO'];
                      this.usuario.DNI = result[i]['DNI'].trim();
                      this.usuario.TELEFONOFIJO = result[i]['FIJO'];
                      this.usuario.MOVIL = result[i]['MOVIL'];
                      this.usuario.EMAIL = result[i]['EMAIL'];
                      this.usuario.PROFESION = result[i]['PROFESION'];
                      this.usuario.POBLACION = result[i]['POBLACION'];
                      this.usuario.PROVINCIA = result[i]['PROVINCIA'];
                      this.usuario.CODIGO_POSTAL = result[i]['CODIGO_POSTAL'];
                      this.usuario.TIPO_VIA = result[i]['TIPO_VIA'];
                      this.usuario.NOMBRE_VIA = result[i]['NOMBRE_VIA'];
                      this.usuario.NUMERO_VIA = result[i]['NUMERO_VIA'];
                      this.usuario.ESCALERA = result[i]['ESCALERA'];
                      this.usuario.PISO = result[i]['PISO'];
                      this.usuario.PUERTA = result[i]['PUERTA'];
                      this.usuario.ESTADO_CIVIL = result[i]['ESTADO_CIVIL'];
                      this.usuario.ANIO = result[i]['AÑO'];
                      this.usuario.MES = result[i]['MES'];
                      this.usuario.DIA = result[i]['DIA'];
                      this.usuario.IBAN = result[i]['IBAN'];

                      viviendaconAnejo.COMPRADORES.push(this.usuario);
                    }

                },
                error => {
                  console.log('Error Obtención de Compradores');
                }
              );

            this.Viviendasanejos.push(viviendaconAnejo);
          },
          error => {
            console.log('Error obtención de Anejos');
          }
        );


        },
        error => {
          console.log('Error obtención de los Datos de las viviendas');
        }

        );


      }


  }

  public setAllElements(): void {

  const selectBox = <HTMLSelectElement>document.getElementById('selectanejos');
  for (let aux_count = 0; aux_count < selectBox.options.length; aux_count++) {

    selectBox.options[aux_count].selected = true;
    selectBox.options[0].selected = false;
  }


  }


  public actualizarDatos(numeroprinex: string): void {

  const nombre = (<HTMLInputElement>document.getElementById('nombrePrinex' + numeroprinex)).value;
  const nombreOculto = (<HTMLInputElement>document.getElementById('nombrePrinexOculto' + numeroprinex)).value;

  const apellidos = (<HTMLInputElement>document.getElementById('apellidosPrinex' + numeroprinex)).value;
  const apellidosOculto = (<HTMLInputElement>document.getElementById('apellidosPrinexOculto' + numeroprinex)).value;

  const DNI = (<HTMLInputElement>document.getElementById('dniPrinex' + numeroprinex)).value;
  const DNIOculto = (<HTMLInputElement>document.getElementById('dniPrinexOculto' + numeroprinex)).value;

  const IBAN = (<HTMLInputElement>document.getElementById('IBANPrinex' + numeroprinex)).value;
  const IBANOculto = (<HTMLInputElement>document.getElementById('IBANPrinexOculto' + numeroprinex)).value;

  const estadocivil = (<HTMLSelectElement>document.getElementById('estadocivilPrinex' + numeroprinex)).value;
  const estadociviliOculto = (<HTMLSelectElement>document.getElementById('estadocivilPrinexOculto' + numeroprinex)).value;

  const fijo = (<HTMLInputElement>document.getElementById('fijoPrinex' + numeroprinex)).value;
  const fijoOculto = (<HTMLInputElement>document.getElementById('fijoPrinexOculto' + numeroprinex)).value;

  const movil = (<HTMLInputElement>document.getElementById('movilPrinex' + numeroprinex)).value;
  const movilOculto = (<HTMLInputElement>document.getElementById('movilPrinexOculto' + numeroprinex)).value;

   const diaNac = (<HTMLInputElement>document.getElementById('diaNacPrinex' + numeroprinex)).value;
   const diaNacOculto = (<HTMLInputElement>document.getElementById('diaNacPrinexOculto' + numeroprinex)).value;

  const mesNac = (<HTMLInputElement>document.getElementById('mesNacPrinex' + numeroprinex)).value;
  const mesNacOculto = (<HTMLInputElement>document.getElementById('mesNacPrinexOculto' + numeroprinex)).value;

  const anioNac = (<HTMLInputElement>document.getElementById('anioNacPrinex' + numeroprinex)).value;
  const anioNacOculto = (<HTMLInputElement>document.getElementById('anioNacPrinexOculto' + numeroprinex)).value;

  const profesion = (<HTMLInputElement>document.getElementById('profesionPrinex' + numeroprinex)).value;
  const profesionOculto = (<HTMLInputElement>document.getElementById('profesionPrinexOculto' + numeroprinex)).value;

  const poblacion = (<HTMLInputElement>document.getElementById('poblacionPrinex' + numeroprinex)).value;
  const poblacionOculto = (<HTMLInputElement>document.getElementById('poblacionPrinexOculto' + numeroprinex)).value;

  const provincia = (<HTMLInputElement>document.getElementById('provinciaPrinex' + numeroprinex)).value;
  const provinciaOculto = (<HTMLInputElement>document.getElementById('provinciaPrinexOculto' + numeroprinex)).value;

  const tipovia = (<HTMLSelectElement>document.getElementById('tipoViaPrinex' + numeroprinex)).value;
  const tipoviaOculto = (<HTMLSelectElement>document.getElementById('tipoViaPrinexOculto' + numeroprinex)).value;

  const nombreVia = (<HTMLInputElement>document.getElementById('nombreViaPrinex' + numeroprinex)).value;
  const nombreViaOculto = (<HTMLInputElement>document.getElementById('nombreViaPrinexOculto' + numeroprinex)).value;

  const numeroVia = (<HTMLInputElement>document.getElementById('numeroViaPrinex' + numeroprinex)).value;
  const numeroViaOculto = (<HTMLInputElement>document.getElementById('numeroViaPrinexOculto' + numeroprinex)).value;

  const cp = (<HTMLInputElement>document.getElementById('cpPrinex' + numeroprinex)).value;
  const cpOculto = (<HTMLInputElement>document.getElementById('cpPrinexOculto' + numeroprinex)).value;

  const piso = (<HTMLInputElement>document.getElementById('pisoPrinex' + numeroprinex)).value;
  const pisoOculto = (<HTMLInputElement>document.getElementById('pisoPrinexOculto' + numeroprinex)).value;

  const escalera = (<HTMLInputElement>document.getElementById('escaleraPrinex' + numeroprinex)).value;
  const escaleraOculto = (<HTMLInputElement>document.getElementById('escaleraPrinexOculto' + numeroprinex)).value;

  const puerta = (<HTMLInputElement>document.getElementById('puertaPrinex' + numeroprinex)).value;
  const puertaOculto = (<HTMLInputElement>document.getElementById('puertaPrinexOculto' + numeroprinex)).value;




  let campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion;

  if(nombre !== nombreOculto) {

    campoPrinex = 'CLNOMC';
    idPrinex = numeroprinex;
    valornuevo = nombre;
    valorantiguo = nombreOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Nombre : "+ nombre + "\n");
  }

  if(apellidos !== apellidosOculto) {

    campoPrinex = 'CLAPEC';
    idPrinex = numeroprinex;
    valornuevo = apellidos;
    valorantiguo = apellidosOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Apellidos : "+ apellidos + "\n");
  }

  if(DNI !== DNIOculto) {

    campoPrinex = 'CLCIF';
    idPrinex = numeroprinex;
    valornuevo = DNI;
    valorantiguo = DNIOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> DNI : "+ DNI + "\n");
  }

  if(IBAN !== IBANOculto) {

    campoPrinex = 'ACIBAN';
    idPrinex = numeroprinex;
    valornuevo = IBAN;
    valorantiguo = IBANOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> IBAN : "+ IBAN + "\n");
  }

  if(estadocivil !== estadociviliOculto) {

    campoPrinex = 'CLESTC';
    idPrinex = numeroprinex;
    valornuevo = estadocivil;
    valorantiguo = estadociviliOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Estado Civil : "+ estadocivil + "\n");
  }

  if(fijo !== fijoOculto) {

    campoPrinex = 'CLTELF';
    idPrinex = numeroprinex;
    valornuevo = fijo;
    valorantiguo = fijoOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Teléfono 1: "+ fijo + "\n");
  }

  if(movil !== movilOculto) {

    campoPrinex = 'CLMOVIL';
    idPrinex = numeroprinex;
    valornuevo = movil;
    valorantiguo = movilOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Teléfono 2: "+ movil + "\n");
  }

  if(diaNac !== diaNacOculto) {

    campoPrinex = 'CLDINA';
    idPrinex = numeroprinex;
    valornuevo = diaNac;
    valorantiguo = diaNacOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Día Nacimiento: "+ diaNac + "\n");
  }

  if(mesNac !== mesNacOculto) {

    campoPrinex = 'CLMENA';
    idPrinex = numeroprinex;
    valornuevo = mesNac;
    valorantiguo = mesNacOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Mes Nacimiento: "+ mesNac + "\n");
  }

  if(anioNac !== anioNacOculto) {

    campoPrinex = 'CLANNA';
    idPrinex = numeroprinex;
    valornuevo = anioNac;
    valorantiguo = anioNacOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Año Nacimiento: "+ anioNac + "\n");
  }

  if(profesion !== profesionOculto) {

    campoPrinex = 'COPROF';
    idPrinex = numeroprinex;
    valornuevo = profesion;
    valorantiguo = profesionOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Profesión: "+ profesion + "\n");
  }

  if(poblacion !== poblacionOculto) {

    campoPrinex = 'CLPOBL';
    idPrinex = numeroprinex;
    valornuevo = poblacion;
    valorantiguo = poblacionOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Población: "+ poblacion + "\n");
  }

  if(provincia !== provinciaOculto) {

    campoPrinex = 'CLPOVI';
    idPrinex = numeroprinex;
    valornuevo = provincia;
    valorantiguo = provinciaOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Provincia: "+ provincia + "\n");
  }

  if(tipovia !== tipoviaOculto) {

    campoPrinex = 'CLSGVP';
    idPrinex = numeroprinex;
    valornuevo = tipovia;
    valorantiguo = tipoviaOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Tipo Vía: "+ tipovia + "\n");
  }

  if(nombreVia !== nombreViaOculto) {

    campoPrinex = 'CLDOMI';
    idPrinex = numeroprinex;
    valornuevo = nombreVia;
    valorantiguo = nombreViaOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Nombre Vía: "+ nombreVia + "\n");
  }

  if(numeroVia !== numeroViaOculto) {

    campoPrinex = 'CLNUME';
    idPrinex = numeroprinex;
    valornuevo = numeroVia;
    valorantiguo = numeroViaOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Número Vía: "+ numeroVia + "\n");
  }

  if(cp !== cpOculto) {

    campoPrinex = 'CLCODP';
    idPrinex = numeroprinex;
    valornuevo = cp;
    valorantiguo = cpOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Código Postal: "+ cp + "\n");
  }

  if(piso !== pisoOculto) {

    campoPrinex = 'CLPISO';
    idPrinex = numeroprinex;
    valornuevo = piso;
    valorantiguo = pisoOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Piso: "+ piso + "\n");
  }

  if(escalera !== escaleraOculto) {

    campoPrinex = 'CLESCA';
    idPrinex = numeroprinex;
    valornuevo = escalera;
    valorantiguo = escaleraOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Escalera: "+ escalera + "\n");
  }

  if(puerta !== puertaOculto) {

    campoPrinex = 'CLPUER';
    idPrinex = numeroprinex;
    valornuevo = puerta;
    valorantiguo = puertaOculto;
    user = this.nombreUsuario;
    aplicacion = 'RPA_PRINEX';
    this.registrarCambios(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion);
    this.cambios.push("--> Puerta: "+ puerta + "\n");
  }

  if(this.cambios.length>0)
  {
    alert('Mañana se modificará el usuario con el código de Prinex: ' + numeroprinex +' con los siguientes cambios: \n' + this.cambios);
    window.location.reload();
  }
  else
  {
    alert('No se han producido cambios');
  }


  }

  public connectSalesForce(codigoprinex: string): void {

    const codigopromocion = (<HTMLInputElement>document.getElementById('codigoPromocion' + codigoprinex)).value;
    const codigovivienda = (<HTMLInputElement>document.getElementById('codigoVivienda' + codigoprinex)).value;
    const productcode = codigopromocion+'-'+codigovivienda;


    this.wsService.connectSaleForce(codigoprinex).subscribe(
        data => {
          console.log('el data es ' +data['result'].length);
          if(data['result'].length === 0){
            alert('El usuario con código prinex '+codigoprinex+' no existe en SalesForce, revíselo');
          }
          else
          {
            data['result'].forEach(element => {
              localStorage.setItem('userSalesForce', element['name']);
              localStorage.setItem('dniSalesForce', element['dni']);
              localStorage.setItem('emailuserSalesForce', element['email']);
              localStorage.setItem('areaprivadaSalesForce', element['fechaareaprivada']);
              localStorage.setItem('IdSalesForce', element['Id']);
              localStorage.setItem('productCode', productcode);
              localStorage.setItem('telefono1', element['telefono1']);
              localStorage.setItem('telefono2', element['telefono2']);
              this.route.queryParams.subscribe(params => {
                this.router.navigate(['/salesforce'], { replaceUrl: true });
              });
  
            });
          }        
        },
        error => {
          console.log(error, { depth: null});
        }
      );
  }

  public registrarCambios(campoPrinex : string, idPrinex : string, valornuevo : string, valorantiguo : string, user :string, aplicacion : string): void {

  this.wsService.addCambioBBDD(campoPrinex, idPrinex, valornuevo, valorantiguo, user, aplicacion)
      .pipe(
      finalize(() => {
      })
      )
      .subscribe(
      result => {
       console.log(campoPrinex +' modificado correctamente');
       },
          error => {
             console.log('Error');
             console.log('Se ha producido un error al subir un documento en BBDD');
            }
       );


  }

  public download(promotion: string) {

  const viviendaconAnejo = new Promociones();
  this.Viviendaanejosdownload = [];
  let anejosArray: string [] = ['UHPROM','UHTIEX','UHPOEX','UHPLEX','UHLEEX','UHDAEX'];
  let width = 70;

  var doc = new jsPDF();
  var img = new Image();
  img.src = 'assets/secciones/LOGOaedas.png';
  doc.addImage(img, 'png', 5, 5, 35, 20);
  doc.setFont('helvetica');
  doc.setFontType('bold');
  doc.setFontSize(18);
  doc.text(50, 20, 'RECIBÍ LLAVES Y DOCUMENTACIÓN');
  doc.setFontSize(10);

  let selectNombrePromo = AppSettings.SELECT_NOMBREPROMOS(this.PromocionesID);
  this.wsService.getDatosViviendas(selectNombrePromo)
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      result => {
        console.log(result);
        console.log(result.length);
        for(let j = 0; j < result.length; j++)
        {
          doc.text(10, 30, 'Promoción: ' + result[j]['PMNOMP']);
        }
        let selectVivienda = AppSettings.SELECT_VIVIENDA(this.PromocionesID, promotion);
        console.log('LA SELECT DE LA VIVIENDA ES: '+selectVivienda);

        this.wsService.getDatosViviendas(selectVivienda)
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(
          result => {
            console.log(result);
            console.log(result.length);
            doc.setFontSize(12);
            for(let j = 0; j < result.length; j++)
            {
              let tipovia = '';
              let nombrevia = '';
              let numerovia = '';
              let poblacion = '';
              if (result[j]['UHSGVP'] !== null && result[j]['UHSGVP'] !== '')
              {
                tipovia = result[j]['UHSGVP'];
              }
              if (result[j]['UHNOVP'] !== null && result[j]['UHNOVP'] !== '')
              {
                nombrevia = result[j]['UHNOVP'];
              }
              if (result[j]['UHNUME'] !== null && result[j]['UHNUME'] !== '')
              {
                numerovia = result[j]['UHNUME'];
              }
              if (result[j]['UHPOBLACION'] !== null && result[j]['UHPOBLACION'] !== '')
              {
                poblacion = result[j]['UHPOBLACION'];
              }

              doc.text(10, 40, '* DIRECCIÓN: ' + tipovia + ' ' + nombrevia + ' ' + numerovia + ' ' + poblacion);
              doc.text(10, 50, '* VIVIENDA: ' + result[j]['UHALIA']);
            }

          },
          error => {
            console.log('Error Obtención los datos de la vivienda');
          }
        );

        let viviendapartes: string [] = [];
        viviendapartes = (promotion.split('-'));
        const partes = viviendapartes.length;
        console.log('la vivienda ' + promotion + ' tiene ' + partes);
        const viviendaconAnejo = new Promociones();
        let select = AppSettings.SELECT_PROMOCIONES(this.PromocionesID);

        for(let k = 1; k <= partes; k++)
        {
          select = select + "AND " + anejosArray[k] + "='" +viviendapartes[k-1].trim()+"'";
        }
        console.log('La select es: '+ select);

        this.wsService.getDatosViviendas(select)
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(
          result => {
            console.log(result);
            console.log(result.length);
            doc.text(10, 60, '* ANEJOS');

            if(result.length === 0)
            {
              doc.text(10, width, '* Esta vivienda no tiene anejos');
              width = width + 10;
            }
            else
            {
              for(let j = 0; j < result.length; j++)
              {
                doc.text(10, width, '* '+result[j]['UHNOCO']);
                width = width + 10;
              }
            }

            doc.setFontSize(14);
            doc.setFontType('italic');
            doc.text(50, width, 'ACTUALIZACION DATOS PERSONALES');
            width = width + 10;
            doc.setFontSize(10);
            doc.setFontType('normal');
            doc.text(10, width, '- Como queremos seguir en contacto contigo te solicitamos actualices tus datos personales');

            let selectPropietarios = AppSettings.SELECT_PROPIETARIOS(this.PromocionesID,promotion);
            this.wsService.getCompradores(selectPropietarios)
              .pipe(
                finalize(() => {
                })
              )
              .subscribe(
                result => {
                  console.log(result);
                  console.log(result.length);
                  let estado = '';

                  viviendaconAnejo.COMPRADORES =  [];
                  for(let i = 0; i < result.length; i++) {
                    this.usuario = new datosPropietario();
                    this.usuario.CODIGO_PRINEX = result[i]['CODIGO_PRINEX'];
                    this.usuario.NOMBRE = result[i]['NOMBRE'];
                    this.usuario.APELLIDOS = result[i]['APELLIDO'];
                    this.usuario.DNI = result[i]['DNI'].trim();
                    this.usuario.TELEFONOFIJO = result[i]['FIJO'];
                    this.usuario.MOVIL = result[i]['MOVIL'];
                    this.usuario.EMAIL = result[i]['EMAIL'];
                    this.usuario.PROFESION = result[i]['PROFESION'];
                    this.usuario.POBLACION = result[i]['POBLACION'];
                    this.usuario.PROVINCIA = result[i]['PROVINCIA'];
                    this.usuario.CODIGO_POSTAL = result[i]['CODIGO_POSTAL'];
                    this.usuario.TIPO_VIA = result[i]['TIPO_VIA'];
                    this.usuario.NOMBRE_VIA = result[i]['NOMBRE_VIA'];
                    this.usuario.NUMERO_VIA = result[i]['NUMERO_VIA'];
                    this.usuario.ESCALERA = result[i]['ESCALERA'];
                    this.usuario.PISO = result[i]['PISO'];
                    this.usuario.PUERTA = result[i]['PUERTA'];
                    this.usuario.ESTADO_CIVIL = result[i]['ESTADO_CIVIL'];
                    this.usuario.ANIO = result[i]['AÑO'];
                    this.usuario.MES = result[i]['MES'];
                    this.usuario.DIA = result[i]['DIA'];
                    this.usuario.IBAN = result[i]['IBAN'];
                    viviendaconAnejo.COMPRADORES.push(this.usuario);
                  }

                  this.Viviendaanejosdownload.push(viviendaconAnejo);

                  for(let z = 0; z < this.Viviendaanejosdownload[0].COMPRADORES.length; z++)
                  {
                    width = width + 10;
                    doc.text(10, width, 'Nombre: '+this.Viviendaanejosdownload[0].COMPRADORES[z].NOMBRE);
                    width = width + 10;
                    doc.text(10, width, 'Apellidos: '+this.Viviendaanejosdownload[0].COMPRADORES[z].APELLIDOS);
                    width = width + 10;
                    doc.text(10, width, 'Teléfono 1: '+this.Viviendaanejosdownload[0].COMPRADORES[z].TELEFONOFIJO);
                    width = width + 10;
                    doc.text(10, width, 'Correo electrónico: '+this.Viviendaanejosdownload[0].COMPRADORES[z].EMAIL);

                  }

                  doc.setFontSize(8);
                  width = width + 40;
                  doc.text(5, width, 'AEDAS HOMES, S.A (CIF: A-87586483. Domicilio: Paseo de la Castellana, nº 42, Madrid), trata sus datos personales con el objeto de prestarle los servicios');
                  width = width + 5;
                  doc.text(5, width, 'objeto de la relación jurídica establecida entre las partes.');
                  width = width + 5;
                  doc.text(5, width, 'Vd. tiene derecho a acceder , rectificar y suprimir los datos, limitar su tratamiento, oponerse al tratamiento y ejercer su derecho a la portabilidad');
                  width = width + 5;
                  doc.text(5, width, 'de los datos de carácter personal, todo ello de forma gratuita, tal como se detalla en la información completa sobre petición de datos.');
                  width = width + 5;
                  doc.text(5, width, 'Puede ejercer tales derechos mediante escrito remitido a AEDAS HOMES, Paseo de la Castellana, nº, 28046 Madrid o bien a DPO@aedashomes.com');
                  doc.setFontSize(6);
                  width = width + 30;
                  doc.text(120, width, 'En                     a      de                  de  20');
                  doc.setFontSize(5);
                  width = width + 10;
                  doc.text(140, width, 'RECIBÍ Y CONFORME');


                  let valida = promotion.replace(/ /g, '');
                  doc.save(valida+'.pdf');

                },
                error => {
                  console.log('Error Obtención de Compradores');
                }
              );
          },
          error => {
            console.log('Error Obtención los datos de la vivienda');
          }
        );

      },
      error => {
        console.log('Error Obtención de la Dirección');
      }
    );
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuariosBBDD} from '../../classes/usuariosBBDD';
import { ProductosService } from 'src/app/services/productos.service';
import { WebsocketService } from '../../services/websocket.service';
import { AuthenticationService } from '../../core';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})



export class UsersComponent implements OnInit {

  public usuarios: UsuariosBBDD [] = [];
  public estados: string[] = ['activo', 'no activo'];
  public perfiles: string[] = ['admin', 'user', 'notario', 'marketing', 'portales'];

  selectedRow: Number;
  setClickedRow: Function;



  constructor(private router: Router, public wsService: WebsocketService, public authenticationService: AuthenticationService) {
    this.setClickedRow = function(index) {
      this.selectedRow = index;
    }
  }

  public usuarioAdmin;
  ngOnInit() {

    const credentials = this.authenticationService.credentials;

    const nombreUsuario = credentials.username;
    console.log('El nombre de usuario es: ' + nombreUsuario);
    console.log('vamos a mirar el perfil');

  // this.wsService.getUserAdmin(nombreUsuario)
  // .pipe(
  //   finalize(() => {
  //   })
  // )
  // .subscribe(
  //   result => {
  //     console.log('obtención del perfil ' );
  //     this.usuarioAdmin = result;
  //     console.log(this.usuarioAdmin);
  //     if (this.usuarioAdmin === false) {
  //       this.router.navigateByUrl('/login');
  //     } else {
  //       this.consultaUsuarios();
  //     }
  //   },
  //   error => {
  //     console.log('Error');
  //   }
  // );

  this.wsService.getUserApp(nombreUsuario)
  .pipe(
    finalize(() => {
    })
  )
  .subscribe(
    result => {
      let valor = JSON.stringify(result);
      let valor2 = JSON.parse(valor);
      console.log('Vamos a la obtención del perfil' +valor2["result"] );
      this.usuarioAdmin = valor2["result"];
      console.log('El resultado es: '+this.usuarioAdmin);
      if(this.usuarioAdmin.toLowerCase() == 'admin')
      {
        this.consultaUsuarios();
      }
      if(this.usuarioAdmin.toLowerCase() == 'user')
      {
        this.router.navigateByUrl('/login');
      }
      if(this.usuarioAdmin.toLowerCase() == 'notario')
      {
        this.router.navigateByUrl('/login');
      }
      if(this.usuarioAdmin.toLowerCase() == 'portales')
      {
        this.router.navigateByUrl('/login');
      }
      if(this.usuarioAdmin.toLowerCase() == 'marketing')
      {
        this.router.navigateByUrl('/login');
      }

    },
    error => {
      console.log('Error GORDO');
    }
  );





  }

  public consultaUsuarios(): void {

    this.wsService.getUsuariosBBDD()
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      data => {
        console.log('Resultado de la obtención de Usuarios');
        console.log(data);
        data['result'].forEach(element => {
          let estado = '';
          let perfil = '';
          const usersBBDD = new UsuariosBBDD();
          usersBBDD.id_usuario = element['id_user'];
          usersBBDD.email_user = element['user_mail'];
          estado = element['estado'];
          usersBBDD.estado = estado.toLocaleLowerCase();
          usersBBDD.fecha_creacion = element['fecha_creacion'];
          usersBBDD.nombre_user = element['user_name'];
          perfil = element['perfil'];
          usersBBDD.perfil = perfil.toLocaleLowerCase();
          console.log(usersBBDD);
          this.usuarios.push(usersBBDD);
        });
      },
      error => {
        console.log('Error');
      }
    );

  }

  public editarCampo(event): void {
    let idUser = event;
    let inputEmail = (<HTMLInputElement>document.getElementById('inputemailUser' + event)).value;
    let inputName = (<HTMLInputElement>document.getElementById('inputnombrelUser' + event)).value;
    let selectPerfil = (<HTMLSelectElement>document.getElementById('selectperfillUser' + event)).value;
    let selectEstado = (<HTMLSelectElement>document.getElementById('selectestadolUser' + event)).value;

    if (inputEmail === '') {
      alert('Debe incluir un email para modificar un usuario');
    } else {
      if (inputName === '') {
        alert('Debe incluir un nombre para modificar un usuario');
      } else {
        this.wsService.updateUserBBDD(idUser, inputEmail, selectEstado, inputName, selectPerfil)
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(
          result => {
           alert('Usuario modificado correctamente');
           window.location.reload();
          },
          error => {
            console.log('Error');
          }
        );

      }
    }

  }

  public guardarCampo(): void {
    let inputEmail = (<HTMLInputElement>document.getElementById('AddInputEmail')).value;
    let inputName = (<HTMLInputElement>document.getElementById('AddInputName')).value;
    let selectPerfil = (<HTMLSelectElement>document.getElementById('AddSelectPerfil')).value;
    let selectEstado = (<HTMLSelectElement>document.getElementById('AddSelectEstado')).value;

    if (inputEmail === '') {
      alert('Debe incluir un email para dar un usuario de Alta');
    } else {
      if (inputName === '') {
        alert('Debe incluir un nombre para dar un usuario de Alta');
      } else {

        this.wsService.addUserBBDD(inputEmail, selectEstado, inputName, selectPerfil)
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(
          result => {
           alert('Usuario dado de alta correctamente');
           window.location.reload();
          },
          error => {
            console.log('Error');
          }
        );

      }
    }
  }

}

import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';

/**
 * Prefixes all requests with `environment.serverUrl`.
 */
@Injectable()
export class ApiPrefixInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string = localStorage.getItem('token');
    if (token) {
      // request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
      // request = request.clone({ headers: request.headers.set('token', token) });
      request = request.clone({headers: request.headers.set('Authorization', 'Bearer ' + token)});
    }

    if (!/^(http|https):/i.test(request.url)) {
      request = request.clone({
        url: environment.serverUrl + request.url
      });
    }
    return next.handle(request);
  }
}

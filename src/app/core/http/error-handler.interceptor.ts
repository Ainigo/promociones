import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { Logger } from '../logger.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '..';

const log = new Logger('ErrorHandlerInterceptor');

/**
 * Adds a default error handler to all requests.
 */
@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    private toasterService: ToastrService,
    private authenticationService: AuthenticationService
  ) {}
  /*
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      return next.handle(request).pipe(catchError(error => this.errorHandler(error)));
    }
    */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError(error => {
        // if (this.router.url !== '/login' && error.status == 401) {
        if (error.status === 401) {
          // this.authenticationService.logout();
          this.printError(error);
          this.router.navigate(['/login']);
          /*
          this.authenticationService.logout().subscribe(out => {
            this.router.navigate(['/login']);
          });
          */
        } else if (error instanceof HttpErrorResponse) {
          if (error.status === 404) {
            this.router.navigate(['/notFound']);
          } else if (error.status === 403) {
            this.printError(error);
            this.router.navigate(['/notAllowed']);
          } else if (error.error != null && error.error.code != null && error.error.code != undefined) {
            this.toasterService.error(error.error.message, 'Error al procesar la petición', {
              positionClass: 'toast-bottom-center'
            });
          } else {
            this.printError(error.error);
          }
        } else {
          this.authenticationService.logout();
        }
        return this.errorHandler(error);
      })
    );
  }

  private printError(error: any) {
    try {
      log.error(error);
      if (error != null && error != undefined) {
        if (error.error != null && error.error.message != null){
          this.toasterService.error(error.error.message, error.error.title, {
            positionClass: 'toast-bottom-center'
          });
        } else if(error.error != null){
          this.toasterService.error(error.error, 'Error al procesar la petición', {
            positionClass: 'toast-bottom-center'
          });
        } else {
          this.toasterService.error(error, 'Error al procesar la petición', {
            positionClass: 'toast-bottom-center'
          });
        }
      } else {
        this.toasterService.error('Se ha producido un error. Consulte con el administrador.', 'Error', {
          positionClass: 'toast-bottom-center'
        });
      }
    } catch (e) {
      this.toasterService.error('An error occurred', '', { positionClass: 'toast-bottom-center' });
    }
  }

  // Customize the default error handler here if needed
  private errorHandler(response: HttpEvent<any>): Observable<HttpEvent<any>> {
    if (!environment.production) {
      // Do something with the error
      // log.error('Request error', response);
    }
    throw response;
  }
}

import { Injectable } from '@angular/core';
import {
    Router, CanActivate, ActivatedRouteSnapshot
} from '@angular/router';

@Injectable()
export class RoleGuardService implements CanActivate {
    constructor(public router: Router) { }
    canActivate(route: ActivatedRouteSnapshot): boolean {
        const expectedRole = route.data.expectedRole;
        const role = localStorage.getItem('role');
        const data = expectedRole.some((item: string) => item === role);
        if (!data) {
            this.router.navigate(['/notAllowed']);
            return false;
        }
        return true;
    }
}

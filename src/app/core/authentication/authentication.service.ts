import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ApiServiceService } from '../../services/api-service.service';
import { authParameters } from '../../../environments/environment';

export interface Credentials {
  // Customize received credentials here
  username: string;
  token: string;
  role_name: string;
}

export interface SalesForce {
  userSalesForce: string;
  emailuserSalesForce: string;
  areaprivadaSalesForce: string;
}

export interface LoginContext {
  username: string;
  password: string;
  remember?: boolean;
}

const credentialsKey = 'credentials';

/**
 * Provides a base for authentication workflow.
 * The Credentials interface as well as login/logout methods should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  /**
   * Gets the user credentials.
   * @return The user credentials or null if the user is not authenticated.
   */
  get credentials(): Credentials | null {
    return this._credentials;
  }

  get SalesForce(): SalesForce | null {
    return this._salesforce;
  }

  private _credentials: Credentials | null;
  private _salesforce: SalesForce | null;

  constructor(public apiService: ApiServiceService) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }


  /**
   * Authenticates the user.
   * @param context The login parameters.
   * @return The user credentials.
   */

  getAuthToken(code: string): Observable<any> {
    return this.apiService.getAuthReq(code);

  }
  login(context: LoginContext): Observable<any> {
    return this.apiService.getLogin(context.username, context.password);
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout() {
    // Customize credentials invalidation here
    // return this.apiService.getLogout();
    this.apiService.getLogout().subscribe((res) => {
      this.setCredentials(null);
    });
    const urlLogin = 'https://login.microsoftonline.com/1eac135e-ccdf-4d83-8b4f-efc9038309f0/oauth2/logout?post_logout_redirect_uri=';
    window.location.href = urlLogin + authParameters.postLogoutUrl;
  }

  /**
   * Checks is the user is authenticated.
   * @return True if the user is authenticated.
   */
  isAuthenticated(): boolean {
    return !!this.credentials;
    // return true;
  }

  /**
   * Sets the user credentials.
   * The credentials may be persisted across sessions by setting the `remember` parameter to true.
   * Otherwise, the credentials are only persisted for the current session.
   * @param credentials The user credentials.
   * @param remember True to remember credentials across sessions.
   */
  setCredentials(credentials?: Credentials, remember?: boolean) {
    this._credentials = credentials || null;

    if (credentials) {
      // const storage = remember ? localStorage : sessionStorage;
      const storage = localStorage;
      storage.setItem('token', credentials.token);
      storage.setItem(credentialsKey, JSON.stringify(credentials));
    } else {
      sessionStorage.removeItem(credentialsKey);
      localStorage.removeItem('token');
      localStorage.removeItem(credentialsKey);
      localStorage.removeItem('name');
      localStorage.removeItem(credentialsKey);
      localStorage.removeItem('urlActual');
    }
  }
}

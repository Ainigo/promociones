// src/app/auth/auth-guard.service.ts
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { WebsocketService } from '../../services/websocket.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
    constructor(public wsService: WebsocketService, public router: Router, public auth: AuthenticationService) { }
    canActivate(): boolean {
      const currenturl = window.location.href;
      const url: string = localStorage.getItem('urlActual');
      console.log('currenturl: ' + currenturl);
      console.log('url: ' + url);

      if (url == null && currenturl !== window.location.origin + '/') {
        localStorage.setItem('urlActual', window.location.href);
      }
      if (!this.auth.isAuthenticated()) {
          console.log('NO ESTA AUTENTICADO');
          this.router.navigate(['login']);
          return false;
      }
      return true;
    }
}
